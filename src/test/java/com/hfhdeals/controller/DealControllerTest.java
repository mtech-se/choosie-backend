package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.faker.CuisineFaker;
import com.hfhdeals.faker.DealFaker;
import com.hfhdeals.faker.PhotoFaker;
import com.hfhdeals.model.jpaentities.*;
import com.hfhdeals.service.baseservice.CategoryDealService;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DealControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private DealService dealService;

    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private CategoryFaker categoryFaker;

    @Autowired
    private CuisineFaker cuisineFaker;

    @Autowired
    private PhotoFaker photoFaker;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryDealService categoryDealService;

    private Deal deal;
    private Category category;
    private Cuisine cuisine;
    private Photo photo;
    private Category category2;

    @Before
    public void setUp() {
        deal = dealFaker.create();
        category = categoryFaker.create();
        category2 = categoryFaker.create();
        cuisine = cuisineFaker.create();
        photo = photoFaker.create();
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a deal that is already persisted in database
        // and a category for that deal
        // and a cuisine for that deal
        // and a photo for that deal
        category.addDeal(deal);
        cuisine.addDeal(deal);
        deal.addPhoto(photo);

        assert deal.getDealPhotos().size() == 1;

        assertNotNull(deal.getId());
        assertEquals(1, deal.getCategoryDeals().size());
        assertEquals(1, deal.getCuisineDeals().size());
        assertEquals(1, deal.getDealPhotos().size());

        // when we call the show endpoint for that deal

        // then we should not get any serialization errors
        // and the JSON has category_deals key
        // and the JSON does not have the order_items key
        MvcResult mvcResult = this.mvc.perform(get("/deals/" + deal.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.category_deals").exists())
                .andExpect(jsonPath("$.category_deals.length()").value(is(1)))
                .andExpect(jsonPath("$.cuisine_deals").exists())
                .andExpect(jsonPath("$.cuisine_deals.length()").value(is(1)))
                .andExpect(jsonPath("$.deal_photos").exists())
                .andExpect(jsonPath("$.deal_photos.length()").value(is(1)))
                .andExpect(jsonPath("$.order_items").doesNotExist())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void update_can_add_a_new_category_to_a_deal() throws Exception {
        // given a deal with no categories
        // and a category
        assertEquals(0, deal.getCategoryDeals().size());
        assertEquals(0, category.getCategoryDeals().size());
        assertNotNull(category.getId());

        // when we call the update endpoint for that deal
        // supplying a JSON request body with 1 categoryDeal containing the category's ID
        String jsonAsString = "{\n" +
                "    \"title\": \"xxx\",\n" +
                "    \"category_deals\": [\n" +
                "    \t{\n" +
                "    \t\t\"category\": {\n" +
                "    \t\t\t\"id\": " + category.getId() + "\n" +
                "    \t\t}\n" +
                "    \t}\n" +
                "    ]\n" +
                "}";

        // then we should get a JSON response with the new category as the sole categoryDeal
        MvcResult mvcResult = this.mvc.perform(
                put("/deals/" + deal.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.category_deals").exists())
                .andExpect(jsonPath("$.category_deals.length()").value(is(1)))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void update_can_remove_categories_from_a_deal_by_supplying_empty_array_as_category_deals() throws Exception {
        // given a deal with 2 categories
        assertEquals(0, deal.getCategoryDeals().size());
        assertEquals(0, category.getCategoryDeals().size());
        assertEquals(0, category2.getCategoryDeals().size());

        CategoryDeal savedCategoryDeal = new CategoryDeal();
        savedCategoryDeal.setDeal(deal);
        savedCategoryDeal.setCategory(category);

        categoryDealService.save(savedCategoryDeal);

        assertEquals(1, deal.getCategoryDeals().size());
        assertEquals(1, category.getCategoryDeals().size());
        assertEquals(0, category2.getCategoryDeals().size());

        for (CategoryDeal categoryDeal : deal.getCategoryDeals()) {
            assertNotNull(categoryDeal.getId());
        }
        for (CategoryDeal categoryDeal : category.getCategoryDeals()) {
            assertNotNull(categoryDeal.getId());
        }
        for (CategoryDeal categoryDeal : category2.getCategoryDeals()) {
            assertNotNull(categoryDeal.getId());
        }

        // when we call the update endpoint for that deal
        // supplying a JSON request body with 1 categoryDeal containing the category's ID
        String jsonAsString = "{\n" +
                "    \"title\": \"xxx\",\n" +
                "    \"category_deals\": []\n" +
                "}";

        // then we should get a JSON response with the new category as the sole categoryDeal
        MvcResult mvcResult = this.mvc.perform(
                put("/deals/" + deal.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.category_deals").exists())
                .andExpect(jsonPath("$.category_deals.length()").value(is(0)))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_deal() throws Exception {
        // given a deal in database that is not soft-deleted
        assertNotNull(deal.getId());
        assertNull(deal.getDeletedAt());

        // when we call the endpoint to delete that deal
        this.mvc.perform(delete("/deals/" + deal.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that deal record to be soft-deleted
        Deal fetchedDeal = dealService.getById(deal.getId());
    }
}