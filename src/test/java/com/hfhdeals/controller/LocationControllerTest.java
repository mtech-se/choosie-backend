package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.LocationFaker;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.service.baseservice.LocationService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.hamcrest.Matchers.empty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LocationControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private LocationService locationService;

    @Autowired
    private LocationFaker locationFaker;

    private Location location;
    private Location location2;

    @Before
    public void setUp() {
        location = locationFaker.create();
        location2 = locationFaker.create();
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 locations already in persistence but one is soft-deleted
        assertNotNull(location.getId());
        assertNotNull(location2.getId());

        assertNull(location.getDeletedAt());
        locationService.softDelete(location2.getId());
        assertNotNull(location2.getDeletedAt());

        // when we GET /locations

        // then we expect
        // - a 200 response
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        MvcResult mvcResult = this.mvc.perform(get("/locations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + location2.getId() + ")]", empty()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a location that is already persisted in database
        assertNotNull(location.getId());

        // when we call the show endpoint for that location

        // then we should not get any serialization errors
        // and the JSON has country key
        MvcResult mvcResult = this.mvc.perform(get("/locations/" + location.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.country").exists())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_location() throws Exception {
        // given a location in database that is not soft-deleted
        assertNotNull(location.getId());
        assertNull(location.getDeletedAt());

        // when we call the endpoint to delete that location
        this.mvc.perform(delete("/locations/" + location.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that location record to be soft-deleted
        Location fetchedLocation = locationService.getById(location.getId());
    }
}