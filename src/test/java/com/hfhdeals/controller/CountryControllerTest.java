package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CountryFaker;
import com.hfhdeals.faker.LocationFaker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.service.baseservice.CountryService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CountryControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private CountryService countryService;

    @Autowired
    private CountryFaker countryFaker;

    @Autowired
    private LocationFaker locationFaker;

    private Country country;
    private Country country2;

    @Before
    public void setUp() {
        country = countryFaker.create();
        country2 = countryFaker.create();

        Location location = locationFaker.create();
        country.addLocation(location);

        Location location2 = locationFaker.create();
        country.addLocation(location2);
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 countries already in persistence but one is soft-deleted
        assertNotNull(country.getId());
        assertNotNull(country2.getId());

        assertNull(country.getDeletedAt());
        countryService.softDelete(country2.getId());
        assertNotNull(country2.getDeletedAt());

        // when we GET /countries

        // then we expect
        // - a 200 response
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        MvcResult mvcResult = this.mvc.perform(get("/countries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + country2.getId() + ")]", empty()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a country that is already persisted in database
        // that has 2 locations to it
        assertNotNull(country.getId());

        assertEquals(2, country.getLocations().size());
        assertNotNull(country.getLocations().get(0).getId());
        assertNotNull(country.getLocations().get(1).getId());
        assertEquals(country, country.getLocations().get(0).getCountry());
        assertEquals(country, country.getLocations().get(1).getCountry());

        // when we call the show endpoint for that country

        // then we should not get any serialization errors
        // and the JSON has locations key with 2 location elements in it
        MvcResult mvcResult = this.mvc.perform(get("/countries/" + country.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.locations").exists())
                .andExpect(jsonPath("$.locations.length()", is(2)))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_country() throws Exception {
        // given a country in database that is not soft-deleted
        assertNotNull(country.getId());
        assertNull(country.getDeletedAt());

        // when we call the endpoint to delete that country
        this.mvc.perform(delete("/countries/" + country.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that country record to be soft-deleted
        Country fetchedCountry = countryService.getById(country.getId());
    }
}