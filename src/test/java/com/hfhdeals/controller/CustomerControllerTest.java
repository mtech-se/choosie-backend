package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CustomerFaker;
import com.hfhdeals.faker.OrderFaker;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.service.baseservice.CustomerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CustomerControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerFaker customerFaker;

    @Autowired
    private OrderFaker orderFaker;

    private Customer customer;
    private Customer customer2;

    @Before
    public void setUp() throws Exception {
        customer = customerFaker.create();
        customer2 = customerFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void index_should_401_when_call_is_unauthenticated() throws Exception {
        // given 2 customers in the database and no currently authenticated user
        assertNotNull(customer.getId());
        assertNotNull(customer2.getId());
        // assert that there's no currently authenticated user
        // TODO: For Priyesh: find out how to programmatically log in a user for use in tests

        // when we call the GET /customers endpoint

        // TODO: Please change this assertion to 401, it should be 401 not 403
        // then we expect a 403

        this.mvc.perform(get("/customers"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    @WithMockUser
    public void show_will_not_include_orders() throws Exception {
        // given a customer that has 2 orders in the database
        // where each of the orders contain 1 order item which in turn have their associated deals
        Order order = orderFaker.create();
        customer.addOrder(order);
        assertTrue(customer.getOrders().contains(order));
        assertEquals(customer, order.getCustomer());

        Order order2 = orderFaker.create();
        customer.addOrder(order2);
        assertTrue(customer.getOrders().contains(order2));
        assertEquals(customer, order2.getCustomer());

        // when we call the endpoint to show that customer

        // then we expect a JSON without the orders present
        MvcResult mvcResult = this.mvc.perform(get("/customers/" + customer.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").doesNotExist())
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_customer() throws Exception {
        // given a customer in database that is not soft-deleted
        assertNotNull(customer.getId());
        assertNull(customer.getDeletedAt());

        // when we call the endpoint to delete that customer
        this.mvc.perform(delete("/customers/" + customer.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that customer record to be soft-deleted
        Customer fetchedCustomer = customerService.getById(customer.getId());
    }
}