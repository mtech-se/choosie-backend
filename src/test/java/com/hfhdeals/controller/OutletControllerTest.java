package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.ContactPersonService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.service.baseservice.OutletService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OutletControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private OutletService outletService;

    private Merchant merchant;
    private Brand brand;
    private Outlet outlet;

    @Before
    public void setUp() {
        merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        merchantService.save(merchant);

        brand = new Brand();
        brand.setName("brand");
        merchant.addBrand(brand);
        brandService.save(brand);

        outlet = new Outlet();
        outlet.setName("outlet");
        outlet.setAddress("outlet address");
        brand.addOutlet(outlet);
        outletService.save(outlet);
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given an outlet that is already persisted in database
        assertNotNull(outlet.getId());

        // when we call the show endpoint for that outlet

        // then we should not get any serialization errors
        // and the JSON for brand relation must not have brand_photos key
        MvcResult mvcResult = this.mvc.perform(get("/outlets/" + outlet.getId()))
                .andExpect(status().isOk())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_outlet() throws Exception {
        // given x outlets in database
        // and a JSON request body that has the bare minimum expected from FE client for outlet creation
        int numOutletsBefore = outletService.getAll().size();
        String jsonAsString = "{\n" +
                "\t\"name\": \"name\",\n" +
                "\t\"address\": \"address\",\n" +
                "\t\"brand\": {\n" +
                "\t\t\"id\": " + brand.getId() + "\n" +
                "\t}\n" +
                "}";

        // when we call the endpoint with that JSON
        MvcResult mvcResult = this.mvc.perform(
                post("/outlets")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));

        // then we expect a new outlet record to be persisted
        assertEquals(numOutletsBefore + 1, outletService.getAll().size());
        assertTrue(outletService.getAll().contains(outlet));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_outlet() throws Exception {
        // given a outlet in database that is not soft-deleted
        assertNotNull(outlet.getId());
        assertNull(outlet.getDeletedAt());

        // when we call the endpoint to delete that outlet
        this.mvc.perform(delete("/outlets/" + outlet.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that outlet record to be soft-deleted
        Outlet fetchedOutlet = outletService.getById(outlet.getId());
    }
}