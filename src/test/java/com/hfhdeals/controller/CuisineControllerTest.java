package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CuisineFaker;
import com.hfhdeals.faker.DealFaker;
import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.service.baseservice.CuisineService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.empty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CuisineControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private CuisineService cuisineService;

    @Autowired
    private CuisineFaker cuisineFaker;

    @Autowired
    private DealFaker dealFaker;

    private Cuisine cuisine;
    private Deal deal;
    private Deal deal2;
    private Cuisine cuisine2;

    @Before
    public void setUp() {
        cuisine = cuisineFaker.create();
        cuisine2 = cuisineFaker.create();

        deal = dealFaker.create();
        deal2 = dealFaker.create();
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 cuisines already in persistence but one is soft-deleted
        assertNotNull(cuisine.getId());
        assertNotNull(cuisine2.getId());

        assertNull(cuisine.getDeletedAt());
        cuisineService.softDelete(cuisine2.getId());
        assertNotNull(cuisine2.getDeletedAt());

        // when we GET /cuisines

        // then we expect
        // - a 200 response
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        MvcResult mvcResult = this.mvc.perform(get("/cuisines"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + cuisine2.getId() + ")]", empty()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a cuisine that is already persisted in database
        // that has 2 deals with that cuisine
        cuisine.addDeal(deal);
        cuisine.addDeal(deal2);

        // when we call the show endpoint for that cuisine

        // then we should not get any serialization errors
        // and the JSON has no cuisine_deals key
        MvcResult mvcResult = this.mvc.perform(get("/cuisines/" + cuisine.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cuisine_deals").doesNotExist())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_cuisine() throws Exception {
        // given x cuisines in database
        // and a JSON request body that has the bare minimum expected from FE client for cuisine creation
        UUID uuid = UUID.randomUUID();

        int numCuisinesBefore = cuisineService.getAll().size();
        String jsonAsString = "{\n" +
                "    \"name\": \"" + uuid + "\",\n" +
                "    \"description\": \"xxx\"\n" +
                "}";

        // when we call the endpoint with that JSON
        MvcResult mvcResult = this.mvc.perform(
                post("/cuisines")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));

        // then we expect a new cuisine record to be persisted
        assertEquals(numCuisinesBefore + 1, cuisineService.getAll().size());
        assertTrue(cuisineService.getAll().contains(cuisine));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_cuisine() throws Exception {
        // given a cuisine in database that is not soft-deleted
        assertNotNull(cuisine.getId());
        assertNull(cuisine.getDeletedAt());

        // when we call the endpoint to delete that cuisine
        this.mvc.perform(delete("/cuisines/" + cuisine.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that cuisine record to be soft-deleted
        Cuisine fetchedCuisine = cuisineService.getById(cuisine.getId());
    }
}