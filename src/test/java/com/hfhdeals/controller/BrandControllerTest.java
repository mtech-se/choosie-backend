package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.BrandFaker;
import com.hfhdeals.faker.MerchantFaker;
import com.hfhdeals.faker.OutletFaker;
import com.hfhdeals.faker.PhotoFaker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BrandControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private BrandService brandService;

    @Autowired
    private BrandFaker brandFaker;

    @Autowired
    private OutletFaker outletFaker;

    @Autowired
    private PhotoFaker photoFaker;

    @Autowired
    private MerchantFaker merchantFaker;

    private Brand brand;
    private Merchant merchant;
    private Brand brand2;

    @Before
    public void setUp() {
        brand = brandFaker.create();
        brand2 = brandFaker.create();

        Outlet outlet = outletFaker.create();
        brand.addOutlet(outlet);

        Outlet outlet2 = outletFaker.create();
        brand.addOutlet(outlet2);

        Photo photo = photoFaker.create();
        brand.addPhoto(photo);

        Photo photo2 = photoFaker.create();
        brand.addPhoto(photo2);

        merchant = merchantFaker.create();
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 brands already in persistence but one is soft-deleted
        assertNotNull(brand.getId());
        assertNotNull(brand2.getId());

        assertNull(brand.getDeletedAt());
        brandService.softDelete(brand2.getId());
        assertNotNull(brand2.getDeletedAt());

        // when we GET /brands

        // then we expect
        // - a 200 response
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        MvcResult mvcResult = this.mvc.perform(get("/brands"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + brand2.getId() + ")]", empty()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a brand that is already persisted in database
        // which has 2 outlets
        // and has 2 photos
        assertNotNull(brand.getId());

        assertEquals(2, brand.getOutlets().size());
        assertNotNull(brand.getOutlets().get(0).getId());
        assertNotNull(brand.getOutlets().get(1).getId());
        assertEquals(brand, brand.getOutlets().get(0).getBrand());
        assertEquals(brand, brand.getOutlets().get(1).getBrand());

        assertEquals(2, brand.getBrandPhotos().size());
        assertEquals(brand, brand.getBrandPhotos().get(0).getBrand());
        assertEquals(brand, brand.getBrandPhotos().get(1).getBrand());

        // when we call the show endpoint for that outlet

        // then we should not get any serialization errors
        // and the JSON for merchant relation must exist and has a value for ID
        // and the JSON for outlets relation must exist and has 2 outlets in it
        // and the JSON for brand_photos relation must exist and has 2 photos in it
        MvcResult mvcResult = this.mvc.perform(get("/brands/" + brand.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchant").exists())
                .andExpect(jsonPath("$.merchant.id").isNumber())
                .andExpect(jsonPath("$.outlets").exists())
                .andExpect(jsonPath("$.outlets.length()", is(2)))
                .andExpect(jsonPath("$.brand_photos").exists())
                .andExpect(jsonPath("$.brand_photos.length()", is(2)))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_brand() throws Exception {
        // given x brands in database
        // and a JSON request body that has the bare minimum expected from FE client for brand creation
        int numBrandsBefore = brandService.getAll().size();
        String jsonAsString = "{\n" +
                "    \"name\": \"xxx\",\n" +
                "    \"merchant\": {\n" +
                "        \"id\": " + merchant.getId() + "\n" +
                "    }\n" +
                "}";

        // when we call the endpoint with that JSON
        MvcResult mvcResult = this.mvc.perform(
                post("/brands")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));

        // then we expect a new brand record to be persisted
        assertEquals(numBrandsBefore + 1, brandService.getAll().size());
        assertTrue(brandService.getAll().contains(brand));
    }

    @Test
    @WithMockUser
    public void update_can_update_a_brand_without_supplying_merchant() throws Exception {
        // given 1 existing brand
        // and a JSON request body that changes the name of the brand
        UUID uuid = UUID.randomUUID();
        String jsonAsString = "{\n" +
                "    \"name\": \"" + uuid + "\"\n" +
                "}";

        // when we call the endpoint with that JSON

        // then we expect that brand to have the updated name value
        MvcResult mvcResult = this.mvc.perform(
                put("/brands/" + brand.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(uuid.toString())))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));

        assertTrue(brandService.getAll().contains(brand));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_brand() throws Exception {
        // given a brand in database that is not soft-deleted
        assertNotNull(brand.getId());
        assertNull(brand.getDeletedAt());

        // when we call the endpoint to delete that brand
        this.mvc.perform(delete("/brands/" + brand.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that brand record to be soft-deleted
        Brand fetchedBrand = brandService.getById(brand.getId());
    }
}