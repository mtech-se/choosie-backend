package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.ContactPersonFaker;
import com.hfhdeals.faker.MerchantFaker;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.ContactPersonService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ContactPersonControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantFaker merchantFaker;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private ContactPersonFaker contactPersonFaker;

    private Merchant merchant;
    private ContactPerson contactPerson;
    private ContactPerson contactPerson2;

    @Before
    public void setUp() {
        merchant = merchantFaker.make();
        contactPerson = contactPersonFaker.make();
        contactPerson2 = contactPersonFaker.make();
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a contactPerson that is already persisted in database
        merchant.addContactPerson(contactPerson);
        merchantService.save(merchant);

        assertNotNull(contactPerson.getId());

        // when we call the show endpoint for that contactPerson

        // then we should not get any serialization errors
        // and the JSON has merchant key
        MvcResult mvcResult = this.mvc.perform(get("/contact-persons/" + contactPerson.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchant").exists())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void show_can_serialize_contact_person_when_merchant_relation_is_populated() throws Exception {
        // given a contact person record which is related to a merchant record (both already persisted)
        merchant.addContactPerson(contactPerson);
        merchant.addContactPerson(contactPerson2);
        merchantService.save(merchant);
        assertNotNull(merchant.getId());
        assertNotNull(contactPerson.getId());
        assertNotNull(contactPerson2.getId());

        // when we call the endpoint to show the contact-person record

        // then we expect that the call runs without exceptions
        // and the JSON response has the merchant
        // and the merchant JSON has no brands key
        // and the merchant JSON has no contact_persons key
        MvcResult mvcResult = this.mvc.perform(get("/contact-persons/" + contactPerson.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchant.brands").doesNotExist())
                .andExpect(jsonPath("$.merchant.contact_persons").doesNotExist())
                .andExpect(jsonPath("$.merchant.hibernateLazyInitializer").doesNotExist())
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_contact_person() throws Exception {
        // given a contact_person in database that is not soft-deleted
        ContactPerson contactPerson = contactPersonFaker.create();
        assertNotNull(contactPerson.getId());
        assertNull(contactPerson.getDeletedAt());

        // when we call the endpoint to delete that contactPerson
        this.mvc.perform(delete("/contact-persons/" + contactPerson.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that contactPerson record to be soft-deleted
        ContactPerson fetchedContactPerson = contactPersonService.getById(contactPerson.getId());
    }
}