package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.faker.DealFaker;
import com.hfhdeals.faker.PhotoFaker;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.utils.JsonUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CategoryControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryFaker categoryFaker;

    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private PhotoFaker photoFaker;

    private Category category;
    private Category category2;

    @Before
    public void setUp() {
        category = categoryFaker.create();
        category2 = categoryFaker.create();

        Deal deal = dealFaker.create();
        category.addDeal(deal);

        Deal deal2 = dealFaker.create();
        category.addDeal(deal2);

        Photo photo = photoFaker.create();
        category.addPhoto(photo);

        Photo photo2 = photoFaker.create();
        category.addPhoto(photo2);

        assertEquals(2, category.getCategoryPhotos().size());
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 categories already in persistence but one is soft-deleted
        assertNotNull(category.getId());
        assertNotNull(category2.getId());

        assertNull(category.getDeletedAt());
        categoryService.softDelete(category2.getId());
        assertNotNull(category2.getDeletedAt());

        // when we GET /categories

        // then we expect
        // - a 200 response
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        MvcResult mvcResult = this.mvc.perform(get("/categories"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + category2.getId() + ")]", empty()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        // given a category that is already persisted in database
        // that has 2 photos
        // and has 2 deals with that category
        assertNotNull(category.getId());

        assertEquals(2, category.getCategoryDeals().size());
        assertEquals(category, category.getCategoryDeals().get(0).getCategory());
        assertEquals(category, category.getCategoryDeals().get(1).getCategory());

        // when adding to a relation that's actually a many-to-many,
        // you have to call something on the collection (e.g. .size()) to
        // trigger JPA to persist the join-table records first,
        // or else JPA double-inserts (cos it runs the other side as well)
        assertEquals(2, category.getCategoryPhotos().size());
        assertEquals(category, category.getCategoryPhotos().get(0).getCategory());
        assertEquals(category, category.getCategoryPhotos().get(1).getCategory());

        // when we call the show endpoint for that category

        // then we should not get any serialization errors
        // and the JSON has no category_deals key
        // and the JSON has category_photos key with 2 elements
        MvcResult mvcResult = this.mvc.perform(get("/categories/" + category.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.category_deals").doesNotExist())
                .andExpect(jsonPath("$.category_photos").exists())
                .andExpect(jsonPath("$.category_photos.length()").value(is(2)))
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_category() throws Exception {
        // given x categories in database
        // and a JSON request body that has the bare minimum expected from FE client for category creation
        UUID uuid = UUID.randomUUID();

        int numCategoriesBefore = categoryService.getAll().size();
        String jsonAsString = "{\n" +
                "    \"name\": \"xxx" + uuid + "\",\n" +
                "    \"description\": \"yyy\"\n" +
                "}";

        // when we call the endpoint with that JSON
        MvcResult mvcResult = this.mvc.perform(
                post("/categories")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));

        // then we expect a new category record to be persisted
        assertEquals(numCategoriesBefore + 1, categoryService.getAll().size());
        assertTrue(categoryService.getAll().contains(category));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_category() throws Exception {
        // given a category in database that is not soft-deleted
        assertNotNull(category.getId());
        assertNull(category.getDeletedAt());

        // when we call the endpoint to delete that category
        this.mvc.perform(delete("/categories/" + category.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that category record to be soft-deleted
        Category fetchedCategory = categoryService.getById(category.getId());
    }
}