package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.*;
import com.hfhdeals.model.jpaentities.*;
import com.hfhdeals.service.baseservice.ContactPersonService;
import com.hfhdeals.service.baseservice.DealCustomerClickService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.utils.DateUtils;
import com.hfhdeals.utils.JsonUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Date;
import java.util.UUID;

import static junit.framework.TestCase.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MerchantControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantFaker merchantFaker;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private ContactPersonFaker contactPersonFaker;

    @Autowired
    private DealClickFaker dealClickFaker;

    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private CustomerFaker customerFaker;

    @Autowired
    private DealCustomerClickService dealCustomerClickService;

    @Autowired
    private OutletFaker outletFaker;

    @Autowired
    private BrandFaker brandFaker;

    private Merchant merchant;
    private Merchant merchant2;
    private Brand brand;
    private Brand brand2;
    private Outlet outlet;
    private Outlet outlet2;
    private ContactPerson contactPerson;
    private ContactPerson contactPerson2;
    private Deal deal;
    private Customer customer;

    @Before
    public void setUp() {
        merchant = merchantFaker.create();
        merchant2 = merchantFaker.create();

        contactPerson = contactPersonFaker.create();
        merchant.addContactPerson(contactPerson);

        contactPerson2 = contactPersonFaker.create();
        merchant.addContactPerson(contactPerson2);

        deal = dealFaker.create();
        outlet = outletFaker.create();
        brand = brandFaker.create();
        outlet.addDeal(deal);
        brand.addOutlet(outlet);
        merchant.addBrand(brand);

        outlet2 = outletFaker.create();
        brand2 = brandFaker.create();
        brand2.addOutlet(outlet2);
        merchant.addBrand(brand2);

        customer = customerFaker.create();
        DealCustomerClick dealCustomerClick = dealClickFaker.make(deal.getId(), customer.getId());
        dealCustomerClickService.save(dealCustomerClick);
    }

    @Test
    @WithMockUser
    public void index_can_return_merchant_records() throws Exception {
        // given 2 merchants already in persistence

        // when we GET /merchants

        // then we expect
        // - a 200 response
        // - response is paginated
        // - $.content being an array of 2 merchants

        this.mvc.perform(get("/merchants"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @WithMockUser
    public void index_does_not_return_soft_deleted_records() throws Exception {
        // given 2 merchants already in persistence but one is soft-deleted
        assertNull(merchant.getDeletedAt());
        merchantService.softDelete(merchant.getId());
//        testEntityManager.refresh(merchant);
        assertNotNull(merchant.getDeletedAt());

        // when we GET /merchants

        // then we expect
        // - a 200 response
        // - response is paginated
        // - $.content being an array of 1 merchant
        // - content doesn't have the soft-deleted merchant

        this.mvc.perform(get("/merchants"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[?(@.id == " + merchant.getId() + ")]", Matchers.empty()))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void show_should_return_json_with_audit_timestamps() throws Exception {
        // given a merchant record that has been persisted
        assertNotNull(merchant.getId());
        assertNotNull(merchant.getCreatedAt());
        assertNotNull(merchant.getUpdatedAt());
        assertNull(merchant.getDeletedAt());

        // when we call the endpoint to show that merchant

        // then we expect a JSON response containing the created_at, updated_at and deleted_at timestamps
        MvcResult mvcResult = this.mvc.perform(get("/merchants/" + merchant.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.created_at", notNullValue()))
                .andExpect(jsonPath("$.updated_at", notNullValue()))
                .andExpect(jsonPath("$.deleted_at", nullValue()))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_should_404_when_attempting_to_fetch_soft_deleted_record() throws Exception {
        // given a merchant record that has been soft-deleted
        assertNotNull(merchant.getId());
        assertNull(merchant.getDeletedAt());
        merchant.setDeletedAt(new Date());

        // when we call the endpoint to show that merchant

        // then we expect an exception to be thrown and a status error code of 404
        this.mvc.perform(get("/merchants/" + merchant.getId()))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors[0]", is("No record found with ID " + merchant.getId())))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_when_merchant_has_contact_person_relations() throws Exception {
        // given a merchant with 2 CP that are both already in database
        assertNotNull(merchant.getId());
        assertEquals(2, merchant.getContactPersons().size());
        assertNotNull(merchant.getContactPersons().get(0).getId());

        // when we call the show endpoint for that merchant ID

        // then we should not get any serialization errors
        MvcResult mvcResult = this.mvc.perform(get("/merchants/" + merchant.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.contact_persons[0].name", is(merchant.getContactPersons().get(0).getName())))
                .andReturn();

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void show_can_serialize_json_with_correct_properties() throws Exception {
        assertNotNull(merchant.getId());
        assertEquals(2, merchant.getContactPersons().size());
        assertEquals(2, merchant.getBrands().size());
        assertNotNull(merchant.getBrands().get(0).getId());
        assertNotNull(merchant.getBrands().get(1).getId());
        assertEquals(1, merchant.getBrands().get(0).getOutlets().size());
        assertEquals(1, merchant.getBrands().get(1).getOutlets().size());
        assertNotNull(merchant.getContactPersons().get(0).getId());
        assertNotNull(merchant.getContactPersons().get(1).getId());

        // when we call the show endpoint for that merchant

        // then we should not get any serialization errors
        // and the JSON for brands relation must be there
        // and the JSON for brands relation must not have brand_photos key
        // and the JSON for brands relation must have outlets key
        // and the JSON for contact_persons relation must not have merchant key
        MvcResult mvcResult = this.mvc.perform(get("/merchants/" + merchant.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brands").exists())
                .andExpect(jsonPath("$.brands[?(@.brand_photos)]").doesNotExist())
                .andExpect(jsonPath("$.brands[?(@.outlets)]").exists())
                .andExpect(jsonPath("$..contact_persons[?(@.merchant)]").doesNotExist())
                .andReturn();

        System.err.println(JsonUtils.asPrettyString(mvcResult.getResponse().getContentAsString()));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_merchant() throws Exception {
        // given x merchants in database
        // and a JSON request body that only has a merchant
        int numMerchantsBefore = merchantService.getAll().size();
        String jsonAsString = "{\n" +
                "\t\"name\": \"test\",\n" +
                "\t\"business_registration_number\": \"riw98355792i\",\n" +
                "\t\"mailing_address\": \"216 Bradtke Crescent, Langhaven, NE 31918-4864\"\n" +
                "}";

        // when we call the endpoint with that JSON
        this.mvc.perform(
                post("/merchants")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        // then we expect a new merchant record to be persisted
        assertEquals(numMerchantsBefore + 1, merchantService.getAll().size());
        assertTrue(merchantService.getAll().contains(merchant));
    }

    @Test
    @WithMockUser
    public void create_can_create_a_new_merchant_with_contact_person() throws Exception {
        // given x merchants in database
        // and a JSON request body that also has 1 contact person to be created
        int numMerchantsBefore = merchantService.getAll().size();
        int numContactPersonsBefore = contactPersonService.getAll().size();
        String jsonAsString = "{\n" +
                "    \"name\": \"qqq\",\n" +
                "    \"business_registration_number\": \"qqq\",\n" +
                "    \"mailing_address\": \"qqq@qqq.com\",\n" +
                "    \"contact_persons\": [\n" +
                "        {\n" +
                "            \"name\": \"qqq\",\n" +
                "            \"position\": \"qqq\",\n" +
                "            \"mobile_number\": \"123\",\n" +
                "            \"email\": \"qqq@qqq.com\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        // when we call the endpoint with that JSON
        this.mvc.perform(
                post("/merchants")
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isCreated())
                .andReturn();

        // then we expect a new merchant record to be persisted
        // and also a new contact person to be persisted
        assertEquals(numMerchantsBefore + 1, merchantService.getAll().size());
        assertEquals(numContactPersonsBefore + 1, contactPersonService.getAll().size());
        assertTrue(merchantService.getAll().contains(merchant));
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser
    public void delete_can_soft_delete_an_existing_merchant() throws Exception {
        // given a merchant in database that is not soft-deleted
        assertNotNull(merchant.getId());
        assertNull(merchant.getDeletedAt());

        // when we call the endpoint to delete that merchant
        this.mvc.perform(delete("/merchants/" + merchant.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that merchant record to be soft-deleted
        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
    }

    @Test
    @WithMockUser
    public void update_can_update_a_merchant() throws Exception {
        // given an existing merchant in database that is not soft-deleted
        // and a JSON request body to change some values of the merchant
        assertNotNull(merchant.getId());
        assertFalse(merchant.getName().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getBusinessRegistrationNumber().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getMailingAddress().equalsIgnoreCase("zzz"));
        String jsonAsString = "{\n" +
                "\t\"name\": \"zzz\",\n" +
                "\t\"business_registration_number\": \"zzz\",\n" +
                "\t\"mailing_address\": \"zzz\"\n" +
                "}";

        // when call the endpoint to update the merchant using that JSON

        // then we expect the fetched-merchant to have those updated values
        // and the response to return a JSON representation of that updated merchant
        MvcResult mvcResult = this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(merchant.getId())))
                .andExpect(jsonPath("$.name", is("zzz")))
                .andExpect(jsonPath("$.business_registration_number", is("zzz")))
                .andExpect(jsonPath("$.mailing_address", is("zzz")))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        assertEquals("zzz", fetchedMerchant.getName());
        assertEquals("zzz", fetchedMerchant.getBusinessRegistrationNumber());
        assertEquals("zzz", fetchedMerchant.getMailingAddress());

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void update_can_do_partial_update_of_a_merchant_record() throws Exception {
        // given an existing merchant in database that is not soft-deleted
        // and a JSON request body to change 1 value of the merchant
        assertNotNull(merchant.getId());
        String businessRegistrationNumber = merchant.getBusinessRegistrationNumber();
        String mailingAddress = merchant.getMailingAddress();

        assertFalse(merchant.getName().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getBusinessRegistrationNumber().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getMailingAddress().equalsIgnoreCase("zzz"));
        String jsonAsString = "{\n" +
                "\t\"name\": \"zzz\"\n" +
                "}";

        // when call the endpoint to update the merchant using that JSON

        // then we expect the fetched-merchant to have the updated value
        // and the rest of the values to be unchanged
        // and the response to return a JSON representation of that updated merchant
        MvcResult mvcResult = this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(merchant.getId())))
                .andExpect(jsonPath("$.name", is("zzz")))
                .andExpect(jsonPath("$.business_registration_number", is(businessRegistrationNumber)))
                .andExpect(jsonPath("$.mailing_address", is(mailingAddress)))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        assertEquals("zzz", fetchedMerchant.getName());
        assertEquals(businessRegistrationNumber, fetchedMerchant.getBusinessRegistrationNumber());
        assertEquals(mailingAddress, fetchedMerchant.getMailingAddress());

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void update_can_update_a_merchant_which_also_supplies_a_contact_person() throws Exception {
        // given an existing merchant in database that is not soft-deleted
        // and a JSON request body to change some values of the merchant
        assertNotNull(merchant.getId());
        assertFalse(merchant.getName().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getBusinessRegistrationNumber().equalsIgnoreCase("zzz"));
        assertFalse(merchant.getMailingAddress().equalsIgnoreCase("zzz"));
        UUID uuid = UUID.randomUUID();
        String jsonAsString = "{\n" +
                "    \"name\": \"" + uuid + "\",\n" +
                "    \"business_registration_number\": \"" + uuid + "\",\n" +
                "    \"mailing_address\": \"zzz\",\n" +
                "    \"contact_persons\": [\n" +
                "        {\n" +
                "            \"name\": \"xxx\",\n" +
                "            \"position\": \"xxx\",\n" +
                "            \"mobile_number\": \"123\",\n" +
                "            \"email\": \"" + uuid + "@" + uuid + "\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        // when call the endpoint to update the merchant using that JSON

        // then we expect the fetched-merchant to have those updated values
        // and the response to return a JSON representation of that updated merchant
        MvcResult mvcResult = this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(merchant.getId())))
                .andExpect(jsonPath("$.name", is(uuid.toString())))
                .andExpect(jsonPath("$.business_registration_number", is(uuid.toString())))
                .andExpect(jsonPath("$.mailing_address", is("zzz")))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        assertEquals(uuid.toString(), fetchedMerchant.getName());
        assertEquals(uuid.toString(), fetchedMerchant.getBusinessRegistrationNumber());
        assertEquals("zzz", fetchedMerchant.getMailingAddress());

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void update_should_preserve_created_at() throws Exception {
        // given an existing merchant in database that is not soft-deleted
        // and an empty JSON request body cos it doesn't matter
        assertNotNull(merchant.getId());
        assertNull(merchant.getDeletedAt());
        String jsonAsString = "{\n" +
                "\t\"name\": \"xxx\",\n" +
                "\t\"business_registration_number\": \"xxx\",\n" +
                "\t\"mailing_address\": \"xxx\"\n" +
                "}\n";
        Date createdAt = merchant.getCreatedAt();

        // when call the endpoint to update the merchant using that JSON

        // then we expect the fetched-merchant to have those updated values
        MvcResult mvcResult = this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.created_at", is(DateUtils.toIsoFormatString(createdAt))))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        assertEquals(createdAt, fetchedMerchant.getCreatedAt());

        System.err.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void update_should_ignore_timestamps_delivered_in_request_body() throws Exception {
        // given an existing merchant in database that is not soft-deleted
        // and a JSON request body that supplied values for created_at, updated_at and deleted_at
        assertNotNull(merchant.getId());
        assertNull(merchant.getDeletedAt());
        assertNotNull(merchant.getCreatedAt());
        assertNotNull(merchant.getUpdatedAt());
        String suppliedDateString = "2019-01-04T07:54:29.999Z";
        String jsonAsString = "{\n" +
                "\t\"name\": \"xxx\",\n" +
                "\t\"business_registration_number\": \"xxx\",\n" +
                "\t\"mailing_address\": \"xxx\",\n" +
                "\t\"created_at\": \"" + suppliedDateString + "\",\n" +
                "\t\"updated_at\": \"" + suppliedDateString + "\",\n" +
                "\t\"deleted_at\": \"" + suppliedDateString + "\"\n" +
                "}\n";

        // when call the endpoint to update the merchant using that JSON

        // then we expect the return JSON to not have the same timestamps as what's supplied
        // and the fetched-merchant to not have those supplied timestamp values
        MvcResult mvcResult = this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.created_at", not(suppliedDateString)))
                .andExpect(jsonPath("$.deleted_at", not(suppliedDateString)))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        assertNotSame(suppliedDateString, fetchedMerchant.getCreatedAt());
        assertNotSame(suppliedDateString, fetchedMerchant.getUpdatedAt());
        assertNotSame(suppliedDateString, fetchedMerchant.getDeletedAt());
    }

    @Test
    @WithMockUser
    public void update_should_set_updated_at_to_something_newer_than_its_previous_value() throws Exception {
        // given an existing merchant in database
        // that has a non-null updated_at date
        assertNotNull(merchant.getId());
        assertNotNull(merchant.getUpdatedAt());
        Date updatedAt = (Date) merchant.getUpdatedAt().clone();
        String jsonAsString = "{\n" +
                "\t\"name\": \"xxx\",\n" +
                "\t\"business_registration_number\": \"xxx\",\n" +
                "\t\"mailing_address\": \"xxx\"\n" +
                "}";

        // when we call the endpoint to update the merchant using that JSON

        // then we expect the updated_at date to be something newer than the one we got before calling the endpoint
        // or at the very least, less than 1 second difference (this affordance is due to flaky tests not consistently getting the time right
        this.mvc.perform(
                put("/merchants/" + merchant.getId())
                        .contentType("application/json")
                        .content(jsonAsString)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updated_at", not(DateUtils.toIsoFormatString(updatedAt))))
                .andReturn();

        Merchant fetchedMerchant = merchantService.getById(merchant.getId());
        Date newUpdatedAt = fetchedMerchant.getUpdatedAt();
        System.err.println("old date: " + DateUtils.toIsoFormatString(updatedAt));
        System.err.println("old date: " + updatedAt.getTime());
        System.err.println("new date: " + DateUtils.toIsoFormatString(newUpdatedAt));
        System.err.println("new date: " + newUpdatedAt.getTime());
        System.err.println("time difference: " + (newUpdatedAt.getTime() - updatedAt.getTime()));
        long millisecondsDifference = newUpdatedAt.getTime() - updatedAt.getTime();
        // allow millisecondsDifference to be a range of -1000
        assertTrue(millisecondsDifference > -1000);
    }
}