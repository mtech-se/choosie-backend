package com.hfhdeals.controller;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.AdminFaker;
import com.hfhdeals.model.jpaentities.Admin;
import com.hfhdeals.service.baseservice.AdminService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdminControllerTest extends TransactionalMockMvcControllerTest {

    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminFaker adminFaker;

    private Admin admin;
    private Admin admin2;

    @Before
    public void setUp() throws Exception {
        admin = adminFaker.create();
        admin2 = adminFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void index_should_401_when_call_is_unauthenticated() throws Exception {
        // given 2 admins in the database and no currently authenticated user
        assertNotNull(admin.getId());
        assertNotNull(admin2.getId());
        // assert that there's no currently authenticated user

        // when we call the GET /admins endpoint

        // then we expect a 403

        this.mvc.perform(get("/admins"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test(expected = RecordNotFound.class)
    @WithMockUser(authorities = {"ADMIN"})
    public void delete_can_soft_delete_an_existing_admin() throws Exception {
        // given a admin in database that is not soft-deleted
        assertNotNull(admin.getId());
        assertNull(admin.getDeletedAt());

        // when we call the endpoint to delete that admin
        this.mvc.perform(delete("/admins/" + admin.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        // then we expect that admin record to be soft-deleted
        Admin fetchedAdmin = adminService.getById(admin.getId());
    }
}