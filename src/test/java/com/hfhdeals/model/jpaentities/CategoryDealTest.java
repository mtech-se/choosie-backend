package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryDealTest {
    @Autowired
    private TestEntityManager testEntityManager;
    private Category category;
    private Deal deal;
    private Deal deal2;

    @Before
    public void setUp() throws Exception {
        category = new Category();
        category.setName("xxx");
        category.setDescription("xxx");

        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        merchant.setMailingAddress("xxx");
        testEntityManager.persist(merchant);

        Brand brand = new Brand();
        brand.setName("xxx");
        brand.setMerchant(merchant);
        testEntityManager.persist(brand);

        Outlet outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setBrand(brand);
        testEntityManager.persist(outlet);

        deal = new Deal();
        deal.setTitle("xxx");
        deal.setOutlet(outlet);
        deal.setOriginalPrice(80);
        deal.setDiscountedPrice(100);
        testEntityManager.persist(deal);

        deal2 = new Deal();
        deal2.setTitle("yyy");
        deal2.setOutlet(outlet);
        deal2.setOriginalPrice(80);
        deal2.setDiscountedPrice(100);
        testEntityManager.persist(deal2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_add_deal_to_existing_category_and_persist() {
        testEntityManager.persist(category);

        category.addDeal(deal);

        int id = category.getId();
        category = null;
        testEntityManager.flush();
        testEntityManager.clear();

        Category fetchedCategory = testEntityManager.find(Category.class, id);
        assertNotNull(fetchedCategory);
        assertEquals(1, fetchedCategory.getCategoryDeals().size());
        assertNotNull(fetchedCategory.getCategoryDeals().get(0).getDeal().getId());
    }

    @Test
    public void it_can_remove_deal_from_existing_category_and_persist() {
        category.addDeal(deal);
        category.addDeal(deal2);
        testEntityManager.persist(category);

        int id = category.getId();
        category = null;
        testEntityManager.flush();
        testEntityManager.clear();

        Category fetchedCategory = testEntityManager.find(Category.class, id);
        assertNotNull(fetchedCategory);
        assertEquals(2, fetchedCategory.getCategoryDeals().size());
        assertNotNull(fetchedCategory.getCategoryDeals().get(0).getDeal().getId());
        assertNotNull(fetchedCategory.getCategoryDeals().get(1).getDeal().getId());

        fetchedCategory.removeDeal(deal);

        id = fetchedCategory.getId();
        testEntityManager.flush();
        testEntityManager.clear();

        fetchedCategory = testEntityManager.find(Category.class, id);
        assertNotNull(fetchedCategory);
        assertEquals(1, fetchedCategory.getCategoryDeals().size());
        assertNotNull(fetchedCategory.getCategoryDeals().get(0).getDeal().getId());
        assertEquals(deal2.getId(), fetchedCategory.getCategoryDeals().get(0).getDeal().getId());
    }
}