package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CuisineDealTest {
    @Autowired
    private TestEntityManager testEntityManager;
    private Cuisine cuisine;
    private Deal deal;
    private Deal deal2;

    @Before
    public void setUp() throws Exception {
        cuisine = new Cuisine();
        cuisine.setName("xxx");
        cuisine.setDescription("xxx");

        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        merchant.setMailingAddress("xxx");
        testEntityManager.persist(merchant);

        Brand brand = new Brand();
        brand.setName("xxx");
        brand.setMerchant(merchant);
        testEntityManager.persist(brand);

        Outlet outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setBrand(brand);
        testEntityManager.persist(outlet);

        deal = new Deal();
        deal.setTitle("xxx");
        deal.setOutlet(outlet);
        deal.setOriginalPrice(80);
        deal.setDiscountedPrice(100);
        testEntityManager.persist(deal);

        deal2 = new Deal();
        deal2.setTitle("yyy");
        deal2.setOutlet(outlet);
        deal2.setOriginalPrice(80);
        deal2.setDiscountedPrice(100);
        testEntityManager.persist(deal2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_add_deal_to_existing_cuisine_and_persist() {
        testEntityManager.persist(cuisine);

        cuisine.addDeal(deal);

        int id = cuisine.getId();
        cuisine = null;
        testEntityManager.flush();
        testEntityManager.clear();

        Cuisine fetchedCuisine = testEntityManager.find(Cuisine.class, id);
        assertNotNull(fetchedCuisine);
        assertEquals(1, fetchedCuisine.getCuisineDeals().size());
        assertNotNull(fetchedCuisine.getCuisineDeals().get(0).getDeal().getId());
    }

    @Test
    public void it_can_remove_deal_from_existing_cuisine_and_persist() {
        cuisine.addDeal(deal);
        cuisine.addDeal(deal2);
        testEntityManager.persist(cuisine);

        int id = cuisine.getId();
        cuisine = null;
        testEntityManager.flush();
        testEntityManager.clear();

        Cuisine fetchedCuisine = testEntityManager.find(Cuisine.class, id);
        assertNotNull(fetchedCuisine);
        assertEquals(2, fetchedCuisine.getCuisineDeals().size());
        assertNotNull(fetchedCuisine.getCuisineDeals().get(0).getDeal().getId());
        assertNotNull(fetchedCuisine.getCuisineDeals().get(1).getDeal().getId());

        fetchedCuisine.removeDeal(deal);

        id = fetchedCuisine.getId();
        testEntityManager.flush();
        testEntityManager.clear();

        fetchedCuisine = testEntityManager.find(Cuisine.class, id);
        assertNotNull(fetchedCuisine);
        assertEquals(1, fetchedCuisine.getCuisineDeals().size());
        assertNotNull(fetchedCuisine.getCuisineDeals().get(0).getDeal().getId());
        assertEquals(deal2.getId(), fetchedCuisine.getCuisineDeals().get(0).getDeal().getId());
    }
}