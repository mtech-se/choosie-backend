package com.hfhdeals.model.jpaentities;

import com.hfhdeals.utils.JsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ContactPersonTest {
    @Autowired
    TestEntityManager testEntityManager;

    private ContactPerson contactPerson;
    private Merchant merchant;

    @Before
    public void setUp() throws Exception {
        contactPerson = new ContactPerson();
        contactPerson.setName("xxx");
        contactPerson.setPosition("xxx");
        contactPerson.setEmail("xxx@xxx.com");
        contactPerson.setMobileNumber("xxx");

        merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");

        testEntityManager.persist(merchant);
    }

    @After
    public void tearDown() throws Exception {
        contactPerson = null;
    }

    @Test
    public void it_can_add_a_contact_person_to_persisted_merchant_and_persist_that_contact_person() {
        merchant.addContactPerson(contactPerson);

        assertNull(contactPerson.getId());
        testEntityManager.persist(contactPerson);
        assertNotNull(contactPerson.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_null() {
        contactPerson.setName(null);
        testEntityManager.persist(contactPerson);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_blank() {
        contactPerson.setName("");
        testEntityManager.persist(contactPerson);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_email_is_maformed() {
        contactPerson.setEmail("xxx");
        testEntityManager.persist(contactPerson);
    }

    @Test
    public void it_can_serialize_a_contact_person() throws IOException {
        merchant.addContactPerson(contactPerson);

        testEntityManager.persist(contactPerson);

        assertEquals(1, merchant.getContactPersons().size());
        assertNotNull(JsonUtils.asString(contactPerson));
    }
}