package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.hfhdeals.utils.JsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CountryTest {
    @Autowired
    TestEntityManager testEntityManager;

    private JacksonTester<Country> json;
    private Country country;
    private Faker faker = new Faker();

    @Before
    public void setUp() throws Exception {
        country = new Country();
        country.setName(faker.address().country());

        ObjectMapper objectMapper = new ObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }

    @After
    public void tearDown() throws Exception {
        country = null;
    }

    @Test
    public void it_can_persist_a_new_country() {
        assertNull(country.getId());
        testEntityManager.persist(country);
        assertNotNull(country.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_null() {
        country.setName(null);
        testEntityManager.persist(country);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_blank() {
        country.setName("");
        testEntityManager.persist(country);
    }

    @Test
    public void it_can_add_new_location_and_then_persist_that_location_when_persisting_country() {
        assertEquals(0, country.getLocations().size());
        Location location = new Location();
        location.setName("xxx");
        location.setCountry(country);
        country.addLocation(location);
        assertEquals(1, country.getLocations().size());
        testEntityManager.persist(country);

        assertNotNull(country.getId());
        assertNotNull(country.getLocations().get(0));
    }

    @Test
    public void it_can_remove_existing_location_and_then_persist_that_change_when_persisting_country() {
        Location location1 = new Location();
        location1.setName("1");
        location1.setCountry(country);

        Location location2 = new Location();
        location2.setName("2");
        location2.setCountry(country);

        country.addLocation(location1);
        country.addLocation(location2);

        testEntityManager.persist(country);
        assertEquals(2, country.getLocations().size());
        assertNotNull(country.getLocations().get(0).getId());
        assertNotNull(country.getLocations().get(1).getId());

        country.removeLocation(location1);
        assertEquals(1, country.getLocations().size());

        int id = country.getId();
        testEntityManager.flush();
        testEntityManager.clear();
        country = null;

        Country fetchedCountry = testEntityManager.find(Country.class, id);
        assertEquals(1, fetchedCountry.getLocations().size());
    }

    @Test
    public void it_can_serialize_a_country_that_has_a_location() throws IOException {
        Location location = new Location();
        location.setName("xxx");
        location.setCountry(country);
        country.addLocation(location);

        testEntityManager.persist(country);

        assertEquals(1, country.getLocations().size());
        assertNotNull(JsonUtils.asString(country));
    }
}