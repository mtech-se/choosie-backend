package com.hfhdeals.model.jpaentities;

import com.hfhdeals.faker.*;
import com.hfhdeals.service.baseservice.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        CustomerService.class,
        CustomerFaker.class,
        OrderService.class,
        OrderFaker.class,
        OutletService.class,
        OutletFaker.class,
        DealService.class,
        DealFaker.class,
        MerchantService.class,
        MerchantFaker.class,
        BrandService.class,
        BrandFaker.class,
})
public class OrderItemTest {
    @Autowired
    TestEntityManager testEntityManager;

    @Autowired
    OrderFaker orderFaker;

    @Autowired
    DealFaker dealFaker;

    private Order persistedOrder;
    private Deal persistedDeal;

    @Before
    public void setUp() throws Exception {
        persistedOrder = orderFaker.create();
        persistedDeal = dealFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_persist_order_item() {
        // given a persisted order and a persisted deal
        // and an orderItem comprising of that order and deal
        assertNotNull(persistedOrder.getId());
        assertNotNull(persistedDeal.getId());
        OrderItem orderItem = new OrderItem(persistedDeal, persistedOrder);

        // when we persist the order item
        testEntityManager.persist(orderItem);

        // then we expect the id to be set
        assertNotNull(orderItem.getId());
    }
}