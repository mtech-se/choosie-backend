package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OutletTest {
    @Autowired
    private TestEntityManager testEntityManager;
    private Brand brand;
    private Outlet outlet;
    private Deal deal;

    @Before
    public void setUp() throws Exception {
        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        testEntityManager.persist(merchant);

        brand = new Brand();
        brand.setName("xxx");
        merchant.addBrand(brand);
        testEntityManager.flush();

        outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setAddress("xxx");

        deal = initDeal();
    }

    @After
    public void tearDown() throws Exception {
    }

    private Deal initDeal() {
        UUID uuid = UUID.randomUUID();

        Merchant newMerchant = new Merchant();
        newMerchant.setName("yyy");
        newMerchant.setBusinessRegistrationNumber("yyy" + uuid);
        newMerchant.setMailingAddress("yyy");
        testEntityManager.persist(newMerchant);

        Brand newBrand = new Brand();
        newBrand.setName("yyy");
        newBrand.setMerchant(newMerchant);
        testEntityManager.persist(newBrand);

        Outlet newOutlet = new Outlet();
        newOutlet.setName("yyy");
        newOutlet.setBrand(newBrand);
        testEntityManager.persist(newOutlet);

        Deal newDeal = new Deal();
        newDeal.setTitle("yyy");
        newDeal.setDescription("yyy");
        newDeal.setOriginalPrice(100);
        newDeal.setDiscountedPrice(80);
        newDeal.setTermsOfUse("yyy");
        newDeal.setRedemptionInstructions("yyy");
        newDeal.setMaxQuantity(100);
        newDeal.setOutlet(newOutlet);

        return newDeal;
    }

    @Test
    public void it_can_get_back_same_values_as_set() {
        assertEquals("xxx", outlet.getName());
        assertEquals("xxx", outlet.getAddress());
    }

    @Test
    public void it_can_persist_a_new_outlet() {
        outlet.setBrand(brand);

        assertNull(outlet.getId());
        testEntityManager.persist(outlet);
        assertNotNull(outlet.getId());
    }

    @Test
    public void it_can_add_new_deal() {
        // given an existing outlet and a new deal
        brand.addOutlet(outlet);
        testEntityManager.persist(outlet);

        // when we add that new deal to the existing outlet and save
        outlet.addDeal(deal);
        assertNull(deal.getId());
        testEntityManager.flush();

        // then we expect that new deal to be persisted as well
        // and the outlet will have a new deal to its collection
        assertNotNull(deal.getId());
        assertEquals(1, outlet.getDeals().size());
        assertEquals(deal, outlet.getDeals().get(0));

    }

    @Test
    public void it_can_remove_existing_deals_from_outlet() {
        // given an existing outlet with 3 existing deals
        brand.addOutlet(outlet);
        testEntityManager.persist(outlet);
        Deal newDeal1 = initDeal();
        Deal newDeal2 = initDeal();
        Deal newDeal3 = initDeal();
        outlet.addDeal(newDeal1);
        outlet.addDeal(newDeal2);
        outlet.addDeal(newDeal3);
        testEntityManager.flush();
        assertNotNull(newDeal1.getId());
        assertNotNull(newDeal2.getId());
        assertNotNull(newDeal3.getId());

        // when we remove 2 of the 3 deals and save
        outlet.removeDeal(newDeal1);
        outlet.removeDeal(newDeal2);
        testEntityManager.flush();

        // then we expect the outlet to have only 1 deal left in its collection
        assertEquals(1, outlet.getDeals().size());
        assertEquals(newDeal3, outlet.getDeals().get(0));
    }
}