package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryPhotoTest {
    @Autowired
    TestEntityManager testEntityManager;
    private Photo photo;
    private Photo photo2;
    private Photo photo3;
    private Category category;

    @Before
    public void setUp() throws Exception {
        category = new Category();
        category.setName("xxx");
        category.setDescription("xxx");

        photo = new Photo();
        photo.setPath("xxx");

        photo2 = new Photo();
        photo2.setPath("yyy");

        photo3 = new Photo();
        photo3.setPath("zzz");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_add_new_photo_to_existing_category() {
        testEntityManager.persist(category);
        assertNotNull(category.getId());

        testEntityManager.persist(photo);
        assertNotNull(photo.getId());

        category.addPhoto(photo);

        Photo fetchedPhoto = testEntityManager.find(Photo.class, photo.getId());
        assertNotNull(fetchedPhoto.getId());
    }

    @Test
    public void it_can_remove_photo_from_existing_category() {
        // given an existing category with 3 existing photos
        testEntityManager.persist(photo);
        testEntityManager.persist(photo2);
        testEntityManager.persist(photo3);
        category.addPhoto(photo);
        category.addPhoto(photo2);
        category.addPhoto(photo3);

        assertNull(category.getId());
        testEntityManager.persist(category);
        assertNotNull(category.getId());

        CategoryPhoto categoryPhoto = testEntityManager.find(CategoryPhoto.class, category.getCategoryPhotos().get(0).getId());
        CategoryPhoto categoryPhoto2 = testEntityManager.find(CategoryPhoto.class, category.getCategoryPhotos().get(1).getId());
        CategoryPhoto categoryPhoto3 = testEntityManager.find(CategoryPhoto.class, category.getCategoryPhotos().get(2).getId());
        assertNotNull(categoryPhoto);
        assertNotNull(categoryPhoto2);
        assertNotNull(categoryPhoto3);

        // when we remove 2 photos from that category and save
        category.removePhoto(photo);
        category.removePhoto(photo2);
        testEntityManager.flush();

        // then we expect that category, when re-fetched from persistence, to have only 1 photo left
        Category fetchedCategory = testEntityManager.find(Category.class, category.getId());
        assertNotNull(fetchedCategory);
        assertEquals(1, fetchedCategory.getCategoryPhotos().size());
        assertEquals(photo3, fetchedCategory.getCategoryPhotos().get(0).getPhoto());
    }

    @Test
    public void it_should_only_add_1_photo() {
        // given a category that is already persisted
        // and a photo that is already persisted
        testEntityManager.persist(category);
        assertNotNull(category.getId());

        testEntityManager.persist(photo);
        assertNotNull(photo.getId());

        // when we add that photo to the category
        category.addPhoto(photo);

        // then that category should now have only 1 photo
        assertEquals(1, category.getCategoryPhotos().size());
    }

    @Test
    public void it_should_only_add_2_photos() {
        // given a category that is already persisted
        // and 2 photos that are already persisted
        testEntityManager.persist(category);
        assertNotNull(category.getId());

        testEntityManager.persist(photo);
        assertNotNull(photo.getId());
        testEntityManager.persist(photo2);
        assertNotNull(photo2.getId());

        // when we add those photos to the category
        category.addPhoto(photo);
        category.addPhoto(photo2);

        testEntityManager.persist(category);
        testEntityManager.persist(photo);
        testEntityManager.persist(photo2);

        // then that category should now have only 2 photos
        // and the first photo should have only 1 element in its categoryPhotos
        // and the second photo should have only 1 element in its categoryPhotos
        assertEquals(2, category.getCategoryPhotos().size());
        assertEquals(1, photo.getCategoryPhotos().size());
        assertEquals(1, photo2.getCategoryPhotos().size());
    }
}