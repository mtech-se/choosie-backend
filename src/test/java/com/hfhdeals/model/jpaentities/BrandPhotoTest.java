package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BrandPhotoTest {
    @Autowired
    TestEntityManager testEntityManager;
    private Photo photo;
    private Photo photo2;
    private Photo photo3;
    private Brand brand;

    @Before
    public void setUp() throws Exception {
        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        testEntityManager.persist(merchant);

        brand = new Brand();
        brand.setName("xxx");
        brand.setMerchant(merchant);

        photo = new Photo();
        photo.setPath("xxx");

        photo2 = new Photo();
        photo2.setPath("yyy");

        photo3 = new Photo();
        photo3.setPath("zzz");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_add_new_photo_to_existing_brand() {
        testEntityManager.persist(brand);
        assertNotNull(brand.getId());

        testEntityManager.persist(photo);
        assertNotNull(photo.getId());

        brand.addPhoto(photo);

        Photo fetchedPhoto = testEntityManager.find(Photo.class, photo.getId());
        assertNotNull(fetchedPhoto.getId());
    }

    @Test
    public void it_can_remove_photo_from_existing_brand() {
        // given an existing brand with 3 existing photos
        testEntityManager.persist(photo);
        testEntityManager.persist(photo2);
        testEntityManager.persist(photo3);
        brand.addPhoto(photo);
        brand.addPhoto(photo2);
        brand.addPhoto(photo3);

        assertNull(brand.getId());
        testEntityManager.persist(brand);
        assertNotNull(brand.getId());

        BrandPhoto brandPhoto = testEntityManager.find(BrandPhoto.class, brand.getBrandPhotos().get(0).getId());
        BrandPhoto brandPhoto2 = testEntityManager.find(BrandPhoto.class, brand.getBrandPhotos().get(1).getId());
        BrandPhoto brandPhoto3 = testEntityManager.find(BrandPhoto.class, brand.getBrandPhotos().get(2).getId());
        assertNotNull(brandPhoto);
        assertNotNull(brandPhoto2);
        assertNotNull(brandPhoto3);

        // when we remove 2 photos from that brand and save
        brand.removePhoto(photo);
        brand.removePhoto(photo2);
        testEntityManager.flush();

        // then we expect that brand, when re-fetched from persistence, to have only 1 photo left
        Brand fetchedBrand = testEntityManager.find(Brand.class, brand.getId());
        assertNotNull(fetchedBrand);
        assertEquals(1, fetchedBrand.getBrandPhotos().size());
        assertEquals(photo3, fetchedBrand.getBrandPhotos().get(0).getPhoto());
    }
}