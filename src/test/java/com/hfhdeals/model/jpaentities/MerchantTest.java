package com.hfhdeals.model.jpaentities;

import com.hfhdeals.utils.JsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MerchantTest {
    @Autowired
    TestEntityManager testEntityManager;

    private ContactPerson contactPerson;
    private ContactPerson contactPerson2;
    private ContactPerson contactPerson3;
    private Merchant merchant;
    private Brand brand;
    private Brand brand2;
    private Brand brand3;

    @Before
    public void setUp() throws Exception {
        merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");

        contactPerson = new ContactPerson();
        contactPerson.setName("xxx");
        contactPerson.setPosition("xxx");
        contactPerson.setEmail("xxx@xxx.com");
        contactPerson.setMobileNumber("xxx");

        contactPerson2 = new ContactPerson();
        contactPerson2.setName("yyy");
        contactPerson2.setPosition("yyy");
        contactPerson2.setEmail("yyy@yyy.com");
        contactPerson2.setMobileNumber("yyy");

        contactPerson3 = new ContactPerson();
        contactPerson3.setName("zzz");
        contactPerson3.setPosition("zzz");
        contactPerson3.setEmail("zzz@zzz.com");
        contactPerson3.setMobileNumber("zzz");

        brand = new Brand();
        brand.setName("xxx");

        brand2 = new Brand();
        brand2.setName("yyy");

        brand3 = new Brand();
        brand3.setName("zzz");
    }

    @After
    public void tearDown() throws Exception {
        contactPerson = null;
    }

    @Test
    public void it_can_persist_merchant() {
        assertNull(merchant.getId());
        testEntityManager.persist(merchant);
        assertNotNull(merchant.getId());
    }

    @Test
    public void it_can_persist_merchant_along_with_contact_persons() {
        merchant.addContactPerson(contactPerson);

        assertNull(merchant.getId());
        testEntityManager.persist(merchant);
        assertNotNull(merchant.getId());

        ContactPerson fetchedContactPerson = testEntityManager.find(ContactPerson.class, contactPerson.getId());
        assertNotNull(fetchedContactPerson.getId());
    }

    @Test
    public void it_can_serialize_a_merchant_that_has_a_contact_person() throws IOException {
        merchant.addContactPerson(contactPerson);
        testEntityManager.persist(merchant);

        assertEquals(1, merchant.getContactPersons().size());
        assertNotNull(JsonUtils.asString(merchant));
    }

    @Test
    public void it_can_add_2_brands_to_new_merchant_then_persist_merchant_to_persist_all_3() {
        // given a new merchant and 2 new brands
        assertNull(merchant.getId());
        assertNull(brand.getId());
        assertNull(brand2.getId());

        // when we add both brands to that merchant and persist the merchant
        merchant.addBrand(brand);
        merchant.addBrand(brand2);
        testEntityManager.persist(merchant);
        testEntityManager.flush();
        testEntityManager.clear();

        // then we expect to be able to re-fetch the merchant to find that it has those 2 brands
        assertNotNull(merchant.getId());
        Merchant fetchedMerchant = testEntityManager.find(Merchant.class, merchant.getId());
        assertEquals(2, fetchedMerchant.getBrands().size());
        assertEquals(brand.getId(), merchant.getBrands().get(0).getId());
        assertEquals(brand2.getId(), merchant.getBrands().get(1).getId());
    }

    @Test
    public void it_can_remove_brands_from_merchant() {
        // given a pre-existing merchant with 3 pre-existing brands
        merchant.addBrand(brand);
        merchant.addBrand(brand2);
        merchant.addBrand(brand3);

        assertNull(merchant.getId());
        assertNull(brand.getId());
        assertNull(brand2.getId());
        assertNull(brand3.getId());
        testEntityManager.persist(merchant);
        assertNotNull(merchant.getId());
        assertNotNull(brand.getId());
        assertNotNull(brand2.getId());
        assertNotNull(brand3.getId());

        // when we remove 2 brands from that merchant and save
        merchant.removeBrand(brand);
        merchant.removeBrand(brand3);
        testEntityManager.flush();

        // then we expect that merchant, when re-fetched from persistence, to have only 1 brand left
        Merchant fetchedMerchant = testEntityManager.find(Merchant.class, merchant.getId());
        assertEquals(1, fetchedMerchant.getBrands().size());
        assertEquals(brand2.getId(), fetchedMerchant.getBrands().get(0).getId());
        assertEquals(brand2, fetchedMerchant.getBrands().get(0));
    }

    @Test
    public void it_can_remove_contact_persons_from_merchant() {
        // given a pre-existing merchant with 3 pre-existing contact persons attached to it
        merchant.addContactPerson(contactPerson);
        merchant.addContactPerson(contactPerson2);
        merchant.addContactPerson(contactPerson3);

        assertNull(merchant.getId());
        assertNull(contactPerson.getId());
        assertNull(contactPerson2.getId());
        assertNull(contactPerson3.getId());
        testEntityManager.persist(merchant);
        assertNotNull(merchant.getId());
        assertNotNull(contactPerson.getId());
        assertNotNull(contactPerson2.getId());
        assertNotNull(contactPerson3.getId());

        // when we remove 2 of the 3 contact persons
        merchant.removeContactPerson(contactPerson);
        merchant.removeContactPerson(contactPerson2);
        testEntityManager.flush();

        // then we expect the same merchant re-fetched from persistence to have only 1 contact person remaining
        assertEquals(1, merchant.getContactPersons().size());
        assertEquals(contactPerson3.getId(), merchant.getContactPersons().get(0).getId());
        testEntityManager.flush();
    }
}