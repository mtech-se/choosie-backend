package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CuisineTest {
    @Autowired
    TestEntityManager testEntityManager;

    private Cuisine cuisine;

    @Before
    public void setUp() throws Exception {
        cuisine = new Cuisine();
        cuisine.setName("xxx");
        cuisine.setDescription("xxx");
    }

    @After
    public void tearDown() throws Exception {
        cuisine = null;
    }

    @Test
    public void it_can_get_back_same_values_as_set() {
        assertEquals("xxx", cuisine.getName());
        assertEquals("xxx", cuisine.getDescription());
    }

    @Test
    public void it_can_persist_a_new_cuisine() {
        assertNull(cuisine.getId());
        testEntityManager.persist(cuisine);
        assertNotNull(cuisine.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_null() {
        cuisine.setName(null);
        testEntityManager.persist(cuisine);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_blank() {
        cuisine.setName("");
        testEntityManager.persist(cuisine);
    }
}