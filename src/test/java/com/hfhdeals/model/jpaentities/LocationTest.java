package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LocationTest {
    @Autowired
    TestEntityManager testEntityManager;

    private Location location;

    @Before
    public void setUp() throws Exception {
        Country country = new Country();
        country.setName("xxx");
        testEntityManager.persist(country);

        location = new Location();
        location.setName("xxx");
        location.setCountry(country);
    }

    @After
    public void tearDown() throws Exception {
        location = null;
    }

    @Test
    public void it_can_get_back_same_values_as_set() {
        assertEquals("xxx", location.getName());
        assertEquals("xxx", location.getCountry().getName());
    }

    @Test
    public void it_can_persist_a_new_location() {
        assertNull(location.getId());
        testEntityManager.persist(location);
        assertNotNull(location.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_null() {
        location.setName(null);
        testEntityManager.persist(location);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_name_is_blank() {
        location.setName("");
        testEntityManager.persist(location);
    }
}