package com.hfhdeals.model.jpaentities;

import com.hfhdeals.faker.*;
import com.hfhdeals.service.baseservice.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        CategoryService.class,
        CategoryFaker.class,

        DealService.class,
        DealFaker.class,

        OutletService.class,
        OutletFaker.class,

        BrandService.class,
        BrandFaker.class,

        MerchantService.class,
        MerchantFaker.class,
})
public class CategoryTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CategoryFaker categoryFaker;

    @Autowired
    private DealFaker dealFaker;

    private Category category;
    private Deal deal;
    private Deal deal2;

    @Before
    public void setUp() throws Exception {
        category = categoryFaker.create();
        deal = dealFaker.create();
        deal2 = dealFaker.create();
    }

    @Test
    public void it_can_add_deals() {
        // given a category and 2 deals
        assertEquals(0, category.getCategoryDeals().size());

        // when we add the 2 deals to the category
        category.addDeal(deal);
        category.addDeal(deal2);
        testEntityManager.flush();

        // then we expect the category to have 2 categoryDeals
        // and each have an ID set to prove each has been persisted
        assertEquals(2, category.getCategoryDeals().size());
        assertNotNull(category.getCategoryDeals().get(0).getId());
        assertNotNull(category.getCategoryDeals().get(1).getId());
    }

    @Test
    public void it_can_remove_deals() {
        // given a category and 2 deals already added to the category
        category.addDeal(deal);
        category.addDeal(deal2);
        testEntityManager.flush();
        assertEquals(2, category.getCategoryDeals().size());
        assertNotNull(category.getCategoryDeals().get(0).getId());
        assertNotNull(category.getCategoryDeals().get(1).getId());

        // when we remove 1 deal from the category
        category.removeDeal(deal);

        // then we expect the category to have 1 categoryDeals
        assertEquals(1, category.getCategoryDeals().size());
        assertNotNull(category.getCategoryDeals().get(0).getId());
    }

    @Test
    public void it_can_remove_deal_and_update_deal_collection() {
        // given a category and a deal
        // and that the deal has been added to the category
        assertNotNull(category.getId());
        assertNotNull(deal.getId());
        assertEquals(0, category.getCategoryDeals().size());
        assertEquals(0, deal.getCategoryDeals().size());

        category.addDeal(deal);
        assertEquals(1, category.getCategoryDeals().size());
        assertEquals(1, deal.getCategoryDeals().size());

        // when we remove the deal from that category
        category.removeDeal(deal);

        // then we expect the deal's categoryDeals collection to be 0
        assertEquals(0, category.getCategoryDeals().size());
        assertEquals(0, deal.getCategoryDeals().size());
    }
}