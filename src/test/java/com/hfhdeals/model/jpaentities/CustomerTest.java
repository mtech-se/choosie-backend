package com.hfhdeals.model.jpaentities;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.CustomerFaker;
import com.hfhdeals.service.baseservice.CustomerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        CustomerFaker.class,
        CustomerService.class,
        BCryptPasswordEncoder.class,
})
public class CustomerTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CustomerFaker customerFaker;

    private Customer savedCustomer;
    private Faker faker = new Faker();
    private Customer unsavedCustomer;

    @Before
    public void setUp() throws Exception {
        savedCustomer = customerFaker.create();
        unsavedCustomer = customerFaker.make();
    }

    @After
    public void tearDown() throws Exception {
        savedCustomer = null;
    }

    @Test
    public void it_can_persist_a_new_customer() {
        assertNull(unsavedCustomer.getId());
        testEntityManager.persist(unsavedCustomer);
        assertNotNull(unsavedCustomer.getId());
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_email_is_null() {
        assertNull(unsavedCustomer.getId());
        unsavedCustomer.setEmail(null);
        testEntityManager.persist(unsavedCustomer);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_first_name_is_null() {
        assertNull(unsavedCustomer.getId());
        unsavedCustomer.setFirstName(null);
        testEntityManager.persist(unsavedCustomer);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_last_name_is_null() {
        assertNull(unsavedCustomer.getId());
        unsavedCustomer.setLastName(null);
        testEntityManager.persist(unsavedCustomer);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_cannot_persist_when_gender_is_null() {
        assertNull(unsavedCustomer.getId());
        unsavedCustomer.setGender(null);
        testEntityManager.persist(unsavedCustomer);
    }

}