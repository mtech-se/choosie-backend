package com.hfhdeals.model.jpaentities;

import com.hfhdeals.utils.JsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BrandTest {
    @Autowired
    TestEntityManager testEntityManager;

    private Brand brand;
    private Merchant merchant;
    private Outlet outlet;
    private Outlet outlet2;

    @Before
    public void setUp() throws Exception {
        brand = new Brand();
        brand.setName("xxx");

        merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");

        outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setAddress("xxx");

        outlet2 = new Outlet();
        outlet2.setName("yyy");
        outlet2.setAddress("yyy");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_persist_brand() {
        testEntityManager.persist(merchant);
        merchant.addBrand(brand);

        assertNull(brand.getId());
        testEntityManager.persist(brand);
        assertNotNull(brand.getId());
    }

    @Test
    public void it_can_serialize_a_brand() throws IOException {
        testEntityManager.persist(merchant);
        merchant.addBrand(brand);

        testEntityManager.flush();

        assertEquals(1, merchant.getBrands().size());
        assertNotNull(JsonUtils.asString(brand));
        System.err.println(JsonUtils.asString(brand));
    }

    @Test
    public void it_can_add_outlets_to_a_brand() {
        // given 2 new outlets and an existing brand
        testEntityManager.persist(merchant);
        merchant.addBrand(brand);
        testEntityManager.persist(brand);
        assertNotNull(brand.getId());

        // when we add those 2 outlets to the brand
        brand.addOutlet(outlet);
        brand.addOutlet(outlet2);
        testEntityManager.flush();

        // then we expect those 2 outlets to have that brand when we fetch them from persistence
        assertNotNull(outlet.getId());
        assertNotNull(outlet2.getId());
        Outlet fetchedOutlet = testEntityManager.find(Outlet.class, outlet.getId());
        assertEquals(brand.getId(), fetchedOutlet.getBrand().getId());
        Outlet fetchedOutlet2 = testEntityManager.find(Outlet.class, outlet2.getId());
        assertEquals(brand.getId(), fetchedOutlet2.getBrand().getId());
    }

    @Test
    public void it_can_remove_outlets_from_a_brand() {
        // given an existing brand with 2 outlets already saved to it
        merchant.addBrand(brand);
        brand.addOutlet(outlet);
        brand.addOutlet(outlet2);
        testEntityManager.persist(merchant);

        assertNotNull(merchant.getId());
        assertNotNull(brand.getId());
        assertNotNull(outlet.getId());
        assertNotNull(outlet2.getId());
        assertEquals(2, brand.getOutlets().size());

        // when we remove one outlet from that brand
        brand.removeOutlet(outlet);
        testEntityManager.flush();

        // then we expect that
        assertNull(outlet.getBrand());
        assertEquals(1, brand.getOutlets().size());
        assertEquals(outlet2.getId(), brand.getOutlets().get(0).getId());
    }
}