package com.hfhdeals.model.jpaentities;

import com.hfhdeals.faker.*;
import com.hfhdeals.service.baseservice.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        CustomerService.class,
        CustomerFaker.class,
        OrderService.class,
        OrderFaker.class,
        OutletService.class,
        OutletFaker.class,
        BrandService.class,
        BrandFaker.class,
        MerchantService.class,
        MerchantFaker.class,
        DealService.class,
        DealFaker.class,
        MerchantService.class,
        MerchantFaker.class,
        OrderItemService.class,
        OrderItemFaker.class,
})
public class OrderTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private OrderFaker orderFaker;

    @Autowired
    private OrderItemFaker orderItemFaker;

    @Autowired
    private DealFaker dealFaker;

    private Order unsavedOrder;
    private Order savedOrder;

    @Before
    public void setUp() throws Exception {
        unsavedOrder = orderFaker.make();
        savedOrder = orderFaker.create();
    }

    @Test
    public void it_can_persist_order() {
        // given an unsavedOrder that is not persisted
        assertNull(unsavedOrder.getId());
        assertNull(unsavedOrder.getCustomer().getId());

        // when we persist the unsavedOrder
        testEntityManager.persist(unsavedOrder.getCustomer());
        testEntityManager.persist(unsavedOrder);

        // then we expect the id to be set
        assertNotNull(unsavedOrder.getId());
        assertNotNull(unsavedOrder.getCustomer().getId());
    }

    @Test
    public void it_can_add_order_items_and_have_it_saved() {
        // given an order and 2 new orderItems
        Deal deal = dealFaker.create();
        assertNotNull(savedOrder.getId());

        OrderItem orderItem = new OrderItem();
        orderItem.setOrder(savedOrder);
        orderItem.setDeal(deal);
        assertEquals(savedOrder, orderItem.getOrder());
        assertNull(orderItem.getId());

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setOrder(savedOrder);
        orderItem2.setDeal(deal);
        assertEquals(savedOrder, orderItem2.getOrder());
        assertNull(orderItem2.getId());

        // when we add both orderItems to that order
        // and re-fetch that order from database
        savedOrder.addOrderItem(orderItem);
        savedOrder.addOrderItem(orderItem2);
        Order fetchedOrder = testEntityManager.find(Order.class, savedOrder.getId());

        // then we expect the re-fetched order to have those 2 orderItems
        assertEquals(2, fetchedOrder.getOrderItems().size());
        assertEquals(orderItem.getId(), fetchedOrder.getOrderItems().get(0).getId());
        assertEquals(orderItem2.getId(), fetchedOrder.getOrderItems().get(1).getId());
    }

    @Test
    public void it_can_remove_order_items_and_have_it_saved() {
        // given an order that has 2 order items
        assertNotNull(savedOrder.getId());

        OrderItem orderItem = orderItemFaker.create();
        savedOrder.addOrderItem(orderItem);
        assertEquals(savedOrder, orderItem.getOrder());
        assertTrue(savedOrder.getOrderItems().contains(orderItem));

        OrderItem orderItem2 = orderItemFaker.create();
        savedOrder.addOrderItem(orderItem2);
        assertEquals(savedOrder, orderItem2.getOrder());
        assertTrue(savedOrder.getOrderItems().contains(orderItem2));

        // when we remove 1 orderItem from that order
        // and re-fetch that order from database
        savedOrder.removeOrderItem(orderItem);
        Order fetchedOrder = testEntityManager.find(Order.class, savedOrder.getId());

        // then we expect the re-fetched order to have not have the removed orderItem
        // but still have the other orderItem
        assertEquals(1, fetchedOrder.getOrderItems().size());
        assertEquals(orderItem2.getId(), fetchedOrder.getOrderItems().get(0).getId());
    }
}