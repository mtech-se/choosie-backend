package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DealPhotoTest {
    @Autowired
    TestEntityManager testEntityManager;
    private Photo photo;
    private Photo photo2;
    private Photo photo3;
    private Deal deal;

    @Before
    public void setUp() throws Exception {
        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        merchant.setMailingAddress("xxx");
        testEntityManager.persist(merchant);

        Brand brand = new Brand();
        brand.setName("xxx");
        brand.setMerchant(merchant);
        testEntityManager.persist(brand);

        Outlet outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setBrand(brand);
        testEntityManager.persist(outlet);

        deal = new Deal();
        deal.setTitle("xxx");
        deal.setOriginalPrice(80);
        deal.setDiscountedPrice(100);
        deal.setOutlet(outlet);

        photo = new Photo();
        photo.setPath("xxx");

        photo2 = new Photo();
        photo2.setPath("yyy");

        photo3 = new Photo();
        photo3.setPath("zzz");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_add_new_photo_to_existing_deal() {
        testEntityManager.persist(deal);
        assertNotNull(deal.getId());

        testEntityManager.persist(photo);
        assertNotNull(photo.getId());

        deal.addPhoto(photo);

        Photo fetchedPhoto = testEntityManager.find(Photo.class, photo.getId());
        assertNotNull(fetchedPhoto.getId());
    }

    @Test
    public void it_can_remove_photo_from_existing_deal() {
        // given an existing deal with 3 existing photos
        testEntityManager.persist(photo);
        testEntityManager.persist(photo2);
        testEntityManager.persist(photo3);
        deal.addPhoto(photo);
        deal.addPhoto(photo2);
        deal.addPhoto(photo3);

        assertNull(deal.getId());
        testEntityManager.persist(deal);
        assertNotNull(deal.getId());

        DealPhoto dealPhoto = testEntityManager.find(DealPhoto.class, deal.getDealPhotos().get(0).getId());
        DealPhoto dealPhoto2 = testEntityManager.find(DealPhoto.class, deal.getDealPhotos().get(1).getId());
        DealPhoto dealPhoto3 = testEntityManager.find(DealPhoto.class, deal.getDealPhotos().get(2).getId());
        assertNotNull(dealPhoto);
        assertNotNull(dealPhoto2);
        assertNotNull(dealPhoto3);

        // when we remove 2 photos from that deal and save
        deal.removePhoto(photo);
        deal.removePhoto(photo2);
        testEntityManager.flush();

        // then we expect that deal, when re-fetched from persistence, to have only 1 photo left
        Deal fetchedDeal = testEntityManager.find(Deal.class, deal.getId());
        assertNotNull(fetchedDeal);
        assertEquals(1, fetchedDeal.getDealPhotos().size());
        assertEquals(photo3, fetchedDeal.getDealPhotos().get(0).getPhoto());
    }
}