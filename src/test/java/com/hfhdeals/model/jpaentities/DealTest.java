package com.hfhdeals.model.jpaentities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DealTest {
    @Autowired
    TestEntityManager testEntityManager;

    private Deal deal;

    @Before
    public void setUp() throws Exception {
        Merchant merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setBusinessRegistrationNumber("xxx");
        merchant.setMailingAddress("xxx");
        testEntityManager.persist(merchant);

        Brand brand = new Brand();
        brand.setName("xxx");
        brand.setMerchant(merchant);
        testEntityManager.persist(brand);

        Outlet outlet = new Outlet();
        outlet.setName("xxx");
        outlet.setBrand(brand);
        testEntityManager.persist(outlet);

        deal = new Deal();
        deal.setTitle("xxx");
        deal.setDescription("xxx");
        deal.setOriginalPrice(100);
        deal.setDiscountedPrice(80);
        deal.setTermsOfUse("xxx");
        deal.setRedemptionInstructions("xxx");
        deal.setMaxQuantity(100);
        deal.setOutlet(outlet);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_get_back_same_values_as_set() {
        assertEquals("xxx", deal.getTitle());
        assertEquals("xxx", deal.getDescription());
        assertEquals(Integer.valueOf(100), deal.getOriginalPrice());
        assertEquals(Integer.valueOf(80), deal.getDiscountedPrice());
        assertEquals("xxx", deal.getTermsOfUse());
        assertEquals("xxx", deal.getRedemptionInstructions());
        assertEquals(Integer.valueOf(100), deal.getMaxQuantity());
    }

    @Test
    public void it_can_persist_deal() {
        assertNull(deal.getId());
        testEntityManager.persist(deal);
        assertNotNull(deal.getId());
    }
}