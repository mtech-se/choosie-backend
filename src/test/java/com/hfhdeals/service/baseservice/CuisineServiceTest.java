package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.CuisineFaker;
import com.hfhdeals.model.jpaentities.Cuisine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CuisineServiceTest extends BaseServiceTest {

    @Autowired
    private CuisineService cuisineService;

    @Autowired
    private CuisineFaker cuisineFaker;

    private Cuisine cuisine;
    private Cuisine cuisine2;

    @Before
    public void setUp() throws Exception {
        cuisine = cuisineFaker.create();
        cuisine2 = cuisineFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 cuisines each with name containing abc123 already in persistence but one is soft-deleted
        cuisine.setName("abc123 xxx");
        cuisine2.setName("abc123 yyy");
        assertNotNull(cuisine.getId());
        assertNotNull(cuisine2.getId());
        assertTrue(cuisine.getName().contains("abc123"));
        assertTrue(cuisine2.getName().contains("abc123"));

        assertNull(cuisine.getDeletedAt());
        cuisineService.softDelete(cuisine2.getId());
        assertNotNull(cuisine2.getDeletedAt());

        Page<Cuisine> page = cuisineService.getAll("abc123", null);
        List<Cuisine> cuisines = page.stream().collect(Collectors.toList());

        assertTrue(cuisines.contains(cuisine));
        assertFalse(cuisines.contains(cuisine2));
    }

}