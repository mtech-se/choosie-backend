package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.BrandFaker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Merchant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class BrandServiceTest extends BaseServiceTest {

    @Autowired
    private BrandService brandService;

    @Autowired
    private BrandFaker brandFaker;

    private Brand brand;
    private Brand brand2;

    @Before
    public void setUp() throws Exception {
        brand = brandFaker.create();
        brand2 = brandFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 brands each with name containing abc123 already in persistence but one is soft-deleted
        brand.setName("abc123 xxx");
        brand2.setName("abc123 yyy");
        assertNotNull(brand.getId());
        assertNotNull(brand2.getId());
        assertTrue(brand.getName().contains("abc123"));
        assertTrue(brand2.getName().contains("abc123"));

        assertNull(brand.getDeletedAt());
        brandService.softDelete(brand2.getId());
        assertNotNull(brand2.getDeletedAt());

        Page<Brand> page = brandService.getAll("abc123", null);
        List<Brand> brands = page.stream().collect(Collectors.toList());

        assertTrue(brands.contains(brand));
        assertFalse(brands.contains(brand2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_merchant_id_and_pageable() {
        // given a merchant and 2 brands
        // and both brands belonging to that merchant
        // and all already in persistence
        // but one of the brands is soft-deleted

        Merchant merchant = brand.getMerchant();
        merchant.addBrand(brand2);
        assertEquals(merchant, brand.getMerchant());
        assertEquals(merchant, brand2.getMerchant());

        assertNotNull(brand.getId());
        assertNotNull(brand2.getId());

        assertNull(brand.getDeletedAt());
        brandService.softDelete(brand2.getId());
        assertNotNull(brand2.getDeletedAt());

        Page<Brand> page = brandService.getAll(merchant.getId(), null);
        List<Brand> brands = page.stream().collect(Collectors.toList());

        assertTrue(brands.contains(brand));
        assertFalse(brands.contains(brand2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_merchant_id_and_pageable() {
        // given a merchant and 2 brands with name containing abc123
        // and both brands belonging to that merchant
        // and all already in persistence
        // but one of the brands is soft-deleted
        brand.setName("abc123 xxx");
        brand2.setName("abc123 yyy");
        assertTrue(brand.getName().contains("abc123"));
        assertTrue(brand2.getName().contains("abc123"));

        Merchant merchant = brand.getMerchant();
        merchant.addBrand(brand2);
        assertEquals(merchant, brand.getMerchant());
        assertEquals(merchant, brand2.getMerchant());

        assertNotNull(brand.getId());
        assertNotNull(brand2.getId());

        assertNull(brand.getDeletedAt());
        brandService.softDelete(brand2.getId());
        assertNotNull(brand2.getDeletedAt());

        Page<Brand> page = brandService.getAll("abc123", merchant.getId(), null);
        List<Brand> brands = page.stream().collect(Collectors.toList());

        assertTrue(brands.contains(brand));
        assertFalse(brands.contains(brand2));
    }
}