package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.LocationFaker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.model.jpaentities.Location;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class LocationServiceTest extends BaseServiceTest {

    @Autowired
    private LocationService locationService;

    @Autowired
    private LocationFaker locationFaker;

    private Location location;
    private Location location2;

    @Before
    public void setUp() throws Exception {
        location = locationFaker.create();
        location2 = locationFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 locations each with name containing abc123 already in persistence but one is soft-deleted
        location.setName("abc123 xxx");
        location2.setName("abc123 yyy");
        assertNotNull(location.getId());
        assertNotNull(location2.getId());
        assertTrue(location.getName().contains("abc123"));
        assertTrue(location2.getName().contains("abc123"));

        assertNull(location.getDeletedAt());
        locationService.softDelete(location2.getId());
        assertNotNull(location2.getDeletedAt());

        Page<Location> page = locationService.getAll("abc123", null);
        List<Location> locations = page.stream().collect(Collectors.toList());

        assertTrue(locations.contains(location));
        assertFalse(locations.contains(location2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_country_id_and_pageable() {
        // given a country and 2 locations
        // and both locations belonging to that country
        // and all already in persistence
        // but one of the locations is soft-deleted

        Country country = location.getCountry();
        country.addLocation(location2);
        assertEquals(country, location.getCountry());
        assertEquals(country, location2.getCountry());

        assertNotNull(location.getId());
        assertNotNull(location2.getId());

        assertNull(location.getDeletedAt());
        locationService.softDelete(location2.getId());
        assertNotNull(location2.getDeletedAt());

        Page<Location> page = locationService.getAll(country.getId(), null);
        List<Location> locations = page.stream().collect(Collectors.toList());

        assertTrue(locations.contains(location));
        assertFalse(locations.contains(location2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_country_id_and_pageable() {
        // given a country and 2 locations with name containing abc123
        // and both locations belonging to that country
        // and all already in persistence
        // but one of the locations is soft-deleted
        location.setName("abc123 xxx");
        location2.setName("abc123 yyy");
        assertTrue(location.getName().contains("abc123"));
        assertTrue(location2.getName().contains("abc123"));

        Country country = location.getCountry();
        country.addLocation(location2);
        assertEquals(country, location.getCountry());
        assertEquals(country, location2.getCountry());

        assertNotNull(location.getId());
        assertNotNull(location2.getId());

        assertNull(location.getDeletedAt());
        locationService.softDelete(location2.getId());
        assertNotNull(location2.getDeletedAt());

        Page<Location> page = locationService.getAll("abc123", country.getId(), null);
        List<Location> locations = page.stream().collect(Collectors.toList());

        assertTrue(locations.contains(location));
        assertFalse(locations.contains(location2));
    }
}