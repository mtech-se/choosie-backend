package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.OutletFaker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Outlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class OutletServiceTest extends BaseServiceTest {

    @Autowired
    private OutletService outletService;

    @Autowired
    private OutletFaker outletFaker;

    private Outlet outlet;
    private Outlet outlet2;

    @Before
    public void setUp() throws Exception {
        outlet = outletFaker.create();
        outlet2 = outletFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 outlets each with name containing abc123 already in persistence but one is soft-deleted
        outlet.setName("abc123 xxx");
        outlet2.setName("abc123 yyy");
        assertNotNull(outlet.getId());
        assertNotNull(outlet2.getId());
        assertTrue(outlet.getName().contains("abc123"));
        assertTrue(outlet2.getName().contains("abc123"));

        assertNull(outlet.getDeletedAt());
        outletService.softDelete(outlet2.getId());
        assertNotNull(outlet2.getDeletedAt());

        Page<Outlet> page = outletService.getAll("abc123", null);
        List<Outlet> outlets = page.stream().collect(Collectors.toList());

        assertTrue(outlets.contains(outlet));
        assertFalse(outlets.contains(outlet2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_brand_id_and_pageable() {
        // given a brand and 2 outlets
        // and both outlets belonging to that brand
        // and all already in persistence
        // but one of the outlets is soft-deleted

        Brand brand = outlet.getBrand();
        brand.addOutlet(outlet2);
        assertEquals(brand, outlet.getBrand());
        assertEquals(brand, outlet2.getBrand());

        assertNotNull(outlet.getId());
        assertNotNull(outlet2.getId());

        assertNull(outlet.getDeletedAt());
        outletService.softDelete(outlet2.getId());
        assertNotNull(outlet2.getDeletedAt());

        Page<Outlet> page = outletService.getAll(brand.getId(), null);
        List<Outlet> outlets = page.stream().collect(Collectors.toList());

        assertTrue(outlets.contains(outlet));
        assertFalse(outlets.contains(outlet2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_brand_id_and_pageable() {
        // given a brand and 2 outlets with name containing abc123
        // and both outlets belonging to that brand
        // and all already in persistence
        // but one of the outlets is soft-deleted
        outlet.setName("abc123 xxx");
        outlet2.setName("abc123 yyy");
        assertTrue(outlet.getName().contains("abc123"));
        assertTrue(outlet2.getName().contains("abc123"));

        Brand brand = outlet.getBrand();
        brand.addOutlet(outlet2);
        assertEquals(brand, outlet.getBrand());
        assertEquals(brand, outlet2.getBrand());

        assertNotNull(outlet.getId());
        assertNotNull(outlet2.getId());

        assertNull(outlet.getDeletedAt());
        outletService.softDelete(outlet2.getId());
        assertNotNull(outlet2.getDeletedAt());

        Page<Outlet> page = outletService.getAll("abc123", brand.getId(), null);
        List<Outlet> outlets = page.stream().collect(Collectors.toList());

        assertTrue(outlets.contains(outlet));
        assertFalse(outlets.contains(outlet2));
    }
}