package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.ContactPersonFaker;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class ContactPersonServiceTest extends BaseServiceTest {

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private ContactPersonFaker contactPersonFaker;

    private ContactPerson contactPerson;
    private ContactPerson contactPerson2;

    @Before
    public void setUp() throws Exception {
        contactPerson = contactPersonFaker.create();
        contactPerson2 = contactPersonFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 contactPersons each with name containing abc123 already in persistence but one is soft-deleted
        contactPerson.setName("abc123 xxx");
        contactPerson2.setName("abc123 yyy");
        assertNotNull(contactPerson.getId());
        assertNotNull(contactPerson2.getId());
        assertTrue(contactPerson.getName().contains("abc123"));
        assertTrue(contactPerson2.getName().contains("abc123"));

        assertNull(contactPerson.getDeletedAt());
        contactPersonService.softDelete(contactPerson2.getId());
        assertNotNull(contactPerson2.getDeletedAt());

        Page<ContactPerson> page = contactPersonService.getAll("abc123", null);
        List<ContactPerson> contactPersons = page.stream().collect(Collectors.toList());

        assertTrue(contactPersons.contains(contactPerson));
        assertFalse(contactPersons.contains(contactPerson2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_merchant_id_and_pageable() {
        // given a merchant and 2 contactPersons
        // and both contactPersons belonging to that merchant
        // and all already in persistence
        // but one of the contactPersons is soft-deleted

        Merchant merchant = contactPerson.getMerchant();
        merchant.addContactPerson(contactPerson2);
        assertEquals(merchant, contactPerson.getMerchant());
        assertEquals(merchant, contactPerson2.getMerchant());

        assertNotNull(contactPerson.getId());
        assertNotNull(contactPerson2.getId());

        assertNull(contactPerson.getDeletedAt());
        contactPersonService.softDelete(contactPerson2.getId());
        assertNotNull(contactPerson2.getDeletedAt());

        Page<ContactPerson> page = contactPersonService.getAll(merchant.getId(), null);
        List<ContactPerson> contactPersons = page.stream().collect(Collectors.toList());

        assertTrue(contactPersons.contains(contactPerson));
        assertFalse(contactPersons.contains(contactPerson2));
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_merchant_id_and_pageable() {
        // given a merchant and 2 contactPersons with name containing abc123
        // and both contactPersons belonging to that merchant
        // and all already in persistence
        // but one of the contactPersons is soft-deleted
        contactPerson.setName("abc123 xxx");
        contactPerson2.setName("abc123 yyy");
        assertTrue(contactPerson.getName().contains("abc123"));
        assertTrue(contactPerson2.getName().contains("abc123"));

        Merchant merchant = contactPerson.getMerchant();
        merchant.addContactPerson(contactPerson2);
        assertEquals(merchant, contactPerson.getMerchant());
        assertEquals(merchant, contactPerson2.getMerchant());

        assertNotNull(contactPerson.getId());
        assertNotNull(contactPerson2.getId());

        assertNull(contactPerson.getDeletedAt());
        contactPersonService.softDelete(contactPerson2.getId());
        assertNotNull(contactPerson2.getDeletedAt());

        Page<ContactPerson> page = contactPersonService.getAll("abc123", merchant.getId(), null);
        List<ContactPerson> contactPersons = page.stream().collect(Collectors.toList());

        assertTrue(contactPersons.contains(contactPerson));
        assertFalse(contactPersons.contains(contactPerson2));
    }
}