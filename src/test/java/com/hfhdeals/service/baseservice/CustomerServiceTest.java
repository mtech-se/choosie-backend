package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.CustomerFaker;
import com.hfhdeals.model.jpaentities.Customer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class CustomerServiceTest extends BaseServiceTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerFaker customerFaker;

    private Customer customer;
    private Customer customer2;

    @Before
    public void setUp() throws Exception {
        customer = customerFaker.create();
        customer2 = customerFaker.create();
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 customers each with name containing abc123 already in persistence but one is soft-deleted
        customer.setFirstName("abc123 xxx");    // will be soft-deleted
        customer2.setFirstName("abc123 yyy");

        assertNotNull(customer.getId());
        assertNotNull(customer2.getId());

        assertTrue(customer.getFirstName().contains("abc123"));
        assertTrue(customer2.getFirstName().contains("abc123"));

        assertNull(customer.getDeletedAt());
        customerService.softDelete(customer.getId());
        assertNotNull(customer.getDeletedAt());

        Page<Customer> page = customerService.getAll("abc123", null);
        List<Customer> customers = page.stream().collect(Collectors.toList());

        assertTrue(customers.contains(customer2));
        assertFalse(customers.contains(customer));
    }
}