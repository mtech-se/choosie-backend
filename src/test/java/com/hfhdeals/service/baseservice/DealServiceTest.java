package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.faker.DealFaker;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.utils.DateUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class DealServiceTest extends BaseServiceTest {

    @Autowired
    private DealService dealService;

    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private CategoryFaker categoryFaker;

    private Deal deal;
    private Deal deal2;
    private Deal deal3;
    private Deal deal4;

    @Before
    public void setUp() throws Exception {
        deal = dealFaker.create();
        deal2 = dealFaker.create();
        deal3 = dealFaker.create();
        deal4 = dealFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_should_not_return_soft_deleted_records_when_called_with_title_and_pageable() {
        // given 2 deals both with abc123 in the title
        // and both already in persistence
        // but one of the deals is soft-deleted
        deal.setTitle("abc123 xxx");
        deal2.setTitle("abc123 yyy");
        assertTrue(deal.getTitle().contains("abc123"));
        assertTrue(deal2.getTitle().contains("abc123"));

        assertNotNull(deal.getId());
        assertNotNull(deal2.getId());

        assertNull(deal.getDeletedAt());
        dealService.softDelete(deal2.getId());
        assertNotNull(deal2.getDeletedAt());

        Page<Deal> page = dealService.getByTitle("abc123", null);
        List<Deal> deals = page.stream().collect(Collectors.toList());

        assertTrue(deals.contains(deal));
        assertFalse(deals.contains(deal2));
    }

    @Test
    public void it_should_not_return_soft_deleted_records_when_called_with_start_date_and_end_date_and_pageable() throws ParseException {
        // given a brand and 2 deals
        // each with created_at on 2-Jan-1990
        // and both already in persistence
        // but one of the deals is soft-deleted

        Date createdAt = DateUtils.fromString("1990-01-02 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
        Date startDate = DateUtils.fromString("1990-01-01 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
        Date endDate = DateUtils.fromString("1990-01-03 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);

        deal.setCreatedAt(createdAt);
        deal2.setCreatedAt(createdAt);
        assertSame(createdAt, deal.getCreatedAt());
        assertSame(createdAt, deal2.getCreatedAt());

        assertNotNull(deal.getId());
        assertNotNull(deal2.getId());

        assertNull(deal.getDeletedAt());
        dealService.softDelete(deal2.getId());
        assertNotNull(deal2.getDeletedAt());

        Page<Deal> page = dealService.getByDateRange(startDate, endDate, null);
        List<Deal> deals = page.stream().collect(Collectors.toList());

        assertTrue(deals.contains(deal));
        assertFalse(deals.contains(deal2));
    }

    @Test
    public void it_should_not_return_soft_deleted_records_when_called_with_start_date_and_end_date_and_name_and_pageable() throws ParseException {
        // given a brand and 2 deals both with abc123 in the title
        // each with created_at on 2-Jan-1990
        // and both already in persistence
        // but one of the deals is soft-deleted
        deal.setTitle("abc123 xxx");
        deal2.setTitle("abc123 yyy");
        assertTrue(deal.getTitle().contains("abc123"));
        assertTrue(deal2.getTitle().contains("abc123"));

        Date createdAt = DateUtils.fromString("1990-01-02 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
        Date startDate = DateUtils.fromString("1990-01-01 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
        Date endDate = DateUtils.fromString("1990-01-03 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);

        deal.setCreatedAt(createdAt);
        deal2.setCreatedAt(createdAt);
        assertSame(createdAt, deal.getCreatedAt());
        assertSame(createdAt, deal2.getCreatedAt());

        assertNotNull(deal.getId());
        assertNotNull(deal2.getId());

        assertNull(deal.getDeletedAt());
        dealService.softDelete(deal2.getId());
        assertNotNull(deal2.getDeletedAt());

        Page<Deal> page = dealService.getByDateRangeAndTitle(startDate, endDate, "abc123", null);
        List<Deal> deals = page.stream().collect(Collectors.toList());

        assertTrue(deals.contains(deal));
        assertFalse(deals.contains(deal2));
    }

    @Test
    public void it_should_get_deals_by_title_and_category_id_and_price_min_and_price_max() {
        // given 3 deals all with abc123 in the title, and 1 deal without
        // and all 4 already in persistence
        // and of the 3, 2 have discounted price between 1000 and 2000
        // and of the 2, both have a given category (the rest don't have any categories)
        deal.setTitle("abc123 xxx");
        deal2.setTitle("abc123 yyy");
        deal3.setTitle("abc123 zzz");
        deal4.setTitle("xxx");

        assertNotNull(deal.getId());
        assertNotNull(deal2.getId());
        assertNotNull(deal3.getId());
        assertNotNull(deal4.getId());

        deal.setDiscountedPrice(1000);
        deal2.setDiscountedPrice(2000);
        deal3.setDiscountedPrice(3000);
        deal4.setDiscountedPrice(3000);

        Category category = categoryFaker.create();
        category.addDeal(deal);
        category.addDeal(deal2);

        assertNotNull(category.getId());

        // when we fetch the page of deals given title, categoryId and price range
        Page<Deal> page = dealService.getDeals("abc123", category.getId(), 1000, 2000, null);
        List<Deal> deals = page.stream().collect(Collectors.toList());

        // then we should get a collection of 2 deal
        assertEquals(2, deals.size());
        assertTrue(deals.contains(deal));
        assertTrue(deals.contains(deal2));

        // when we try again with range set to 1000 strictly
        page = dealService.getDeals("abc123", category.getId(), 1000, 1000, null);
        deals = page.stream().collect(Collectors.toList());

        // then we should get a collection of 1 deal
        assertEquals(1, deals.size());
        assertTrue(deals.contains(deal));
    }
}