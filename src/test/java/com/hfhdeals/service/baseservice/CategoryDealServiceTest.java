package com.hfhdeals.service.baseservice;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.faker.DealFaker;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryDeal;
import com.hfhdeals.model.jpaentities.Deal;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

public class CategoryDealServiceTest extends BaseServiceTest {
    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryFaker categoryFaker;

    @Autowired
    private CategoryDealService categoryDealService;

    private Category category;
    private Deal deal;
    private CategoryDeal categoryDeal;

    @Before
    public void setUp() throws Exception {
        deal = dealFaker.create();
        category = categoryFaker.create();

        categoryDeal = new CategoryDeal();
        categoryDeal.setCategory(category);
        categoryDeal.setDeal(deal);
    }

    @Test
    @Transactional
    public void it_can_create_a_new_category_deal_whilst_also_updating_respective_collections() {
        // given existing Category and Deal
        // who don't have any categoryDeals
        assertNotNull(category.getId());
        assertNotNull(deal.getId());
        assertNull(categoryDeal.getId());
        assertEquals(0, category.getCategoryDeals().size());
        assertEquals(0, deal.getCategoryDeals().size());

        // when call the DAO's method to save a new categoryDeal for that Category and Deal
        categoryDealService.save(categoryDeal);

        // then the categoryDeal should have a non-null ID proving it has been saved
        // and the category has a categoryDeals size of 1
        // and the deal has a categoryDeals size of 1
        assertNotNull(categoryDeal.getId());
        assertEquals(1, category.getCategoryDeals().size());
        assertEquals(1, deal.getCategoryDeals().size());
    }

    @Test(expected = RecordNotFound.class)
    @Transactional
    public void it_can_delete_a_category_deal_when_given_category_and_deal_and_update_respective_collections() {
        // given an existing CategoryDeal which points to a Category and a Deal
        categoryDealService.save(categoryDeal);
        assertEquals(deal.getId(), categoryDeal.getDeal().getId());
        assertEquals(category.getId(), categoryDeal.getCategory().getId());
        assertNotNull(categoryDeal.getId());

        // when call the DAO's method to delete the categoryDeal
        // by giving it the categoryDeal's category and deal
        categoryDealService.delete(categoryDeal);

        // then the categoryDeal's category should have 0 items in its categoryDeals collection
        // and the categoryDeal's deal should have 0 items in its categoryDeals collection
        // and the categoryDeal should no longer exist when fetched
        assertEquals(0, categoryDeal.getCategory().getCategoryDeals().size());
        assertEquals(0, categoryDeal.getDeal().getCategoryDeals().size());
        assertNull(categoryDealService.getById(categoryDeal.getId()));
    }

}