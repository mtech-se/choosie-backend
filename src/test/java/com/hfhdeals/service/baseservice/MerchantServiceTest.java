package com.hfhdeals.service.baseservice;

import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.faker.BrandFaker;
import com.hfhdeals.faker.ContactPersonFaker;
import com.hfhdeals.faker.MerchantFaker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@AutoConfigureTestEntityManager
public class MerchantServiceTest extends BaseServiceTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantFaker merchantFaker;

    @Autowired
    private BrandFaker brandFaker;

    @Autowired
    private ContactPersonFaker contactPersonFaker;

    private Merchant merchant;
    private Merchant merchant2;

    @Before
    public void setUp() throws Exception {
        merchant = merchantFaker.create();

        merchant2 = merchantFaker.create();

        Brand brand = brandFaker.create();
        merchant.addBrand(brand);

        ContactPerson contactPerson = contactPersonFaker.create();
        merchant.addContactPerson(contactPerson);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_get_all_merchants_by_name_containing_ignore_case() {
        // given 2 merchants persisted
        merchant.setName("Black Hole Foods");
        testEntityManager.persist(merchant);
        merchant2.setName("Orion Foods");
        testEntityManager.persist(merchant2);

        // when we find by name containing something they both have
        Page<Merchant> page = merchantService.getAll("Foods", null);
        List<Merchant> merchants = page.stream().collect(Collectors.toList());

        // then we expect a collection containing both merchants
        assertEquals(2, merchants.size());
    }

    @Test
    public void it_can_get_all_merchants_by_name_containing_ignore_case_without_soft_deleted() {
        // given 2 merchants persisted
        merchant.setName("Black Hole Foods");
        testEntityManager.persist(merchant);
        merchant2.setName("Orion Foods");
        merchant2.setDeletedAt(new Date());
        testEntityManager.persist(merchant2);

        // when we find by name containing something they both have
        Page<Merchant> page = merchantService.getAll("Foods", null);
        List<Merchant> merchants = page.stream().collect(Collectors.toList());

        // then we expect a collection containing both merchants
        assertEquals(1, merchants.size());
        assertEquals(merchant, merchants.get(0));
    }

    @Test
    public void it_can_update_a_merchant() {
        // given an already-persisted merchant
        testEntityManager.persist(merchant);

        // when we edit details about that merchant, save it and re-fetch it from persistence
        assertNotEquals("blah", merchant.getName());
        merchant.setName("blah");
        assertEquals("blah", merchant.getName());
        merchantService.update(merchant);

        Merchant fetchedMerchant = testEntityManager.find(Merchant.class, merchant.getId());

        // then we should get the merchant record with the new fields
        assertEquals("blah", fetchedMerchant.getName());
    }

    @Test(expected = RecordNotFound.class)
    public void it_should_throw_when_updating_a_soft_deleted_merchant() {
        // given an already-persisted merchant that then gets soft-deleted
        testEntityManager.persist(merchant);
        merchant.setDeletedAt(new Date());
        testEntityManager.persist(merchant);

        // when we try to update that merchant record
        merchantService.update(merchant);

        // then it should throw
    }

    @Test
    public void it_should_keep_relation_records_when_doing_partial_update() {
        // given a persisted merchant with 1 brand and 1 contact person already persisted
        testEntityManager.persist(merchant);
        assertNotNull(merchant.getId());
        assertEquals(1, merchant.getBrands().size());
        assertEquals(1, merchant.getContactPersons().size());
        assertNotNull(merchant.getBrands().get(0).getId());
        assertNotNull(merchant.getContactPersons().get(0).getId());

        // when we do a partial update (e.g. change the name only), then run update on the record
        assertNotEquals("blah", merchant.getName());
        merchant.setName("blah");
        merchantService.update(merchant);
        Merchant fetchedMerchant = testEntityManager.find(Merchant.class, merchant.getId());

        testEntityManager.flush();
        testEntityManager.clear();

        // then we expect the re-fetched merchant to still have the brand and contact person
        assertEquals(1, fetchedMerchant.getBrands().size());
        assertEquals(1, fetchedMerchant.getContactPersons().size());
        assertNotNull(fetchedMerchant.getBrands().get(0).getId());
        assertNotNull(fetchedMerchant.getContactPersons().get(0).getId());
    }
}