package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.model.jpaentities.Category;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CategoryServiceTest extends BaseServiceTest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryFaker categoryFaker;

    private Category category;
    private Category category2;

    @Before
    public void setUp() throws Exception {
        category = categoryFaker.create();
        category2 = categoryFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 categorys each with name containing abc123 already in persistence but one is soft-deleted
        category.setName("abc123 xxx");
        category2.setName("abc123 yyy");
        assertNotNull(category.getId());
        assertNotNull(category2.getId());
        assertTrue(category.getName().contains("abc123"));
        assertTrue(category2.getName().contains("abc123"));

        assertNull(category.getDeletedAt());
        categoryService.softDelete(category2.getId());
        assertNotNull(category2.getDeletedAt());

        Page<Category> page = categoryService.getAll("abc123", null);
        List<Category> categorys = page.stream().collect(Collectors.toList());

        assertTrue(categorys.contains(category));
        assertFalse(categorys.contains(category2));
    }

}