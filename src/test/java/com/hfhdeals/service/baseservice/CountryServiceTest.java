package com.hfhdeals.service.baseservice;

import com.hfhdeals.faker.CountryFaker;
import com.hfhdeals.model.jpaentities.Country;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryServiceTest extends BaseServiceTest {

    @Autowired
    private CountryService countryService;

    @Autowired
    private CountryFaker countryFaker;

    private Country country;
    private Country country2;

    @Before
    public void setUp() throws Exception {
        country = countryFaker.create();
        country2 = countryFaker.create();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void it_should_not_return_soft_deleted_records_when_called_with_name_and_pageable() {
        // given 2 countrys each with name containing abc123 already in persistence but one is soft-deleted
        country.setName("abc123 xxx");
        country2.setName("abc123 yyy");
        assertNotNull(country.getId());
        assertNotNull(country2.getId());
        assertTrue(country.getName().contains("abc123"));
        assertTrue(country2.getName().contains("abc123"));

        assertNull(country.getDeletedAt());
        countryService.softDelete(country2.getId());
        assertNotNull(country2.getDeletedAt());

        Page<Country> page = countryService.getAll("abc123", null);
        List<Country> countries = page.stream().collect(Collectors.toList());

        assertTrue(countries.contains(country));
        assertFalse(countries.contains(country2));
    }

}