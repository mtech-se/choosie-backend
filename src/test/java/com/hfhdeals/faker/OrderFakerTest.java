package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderFakerTest {

    @Autowired
    private OrderFaker orderFaker;

    @Test
    public void it_can_make_a_new_instance_without_it_or_its_relations_being_persisted() {
        Order order = orderFaker.make();

        assertNull(order.getId());
        assertNull(order.getCustomer().getId());
    }

    @Test
    @Transactional
    public void it_can_make_and_persist_a_new_instance_with_its_relations_also_persisted() {
        Order order = orderFaker.create();

        assertNotNull(order.getId());
        assertNotNull(order.getCustomer().getId());
    }
}