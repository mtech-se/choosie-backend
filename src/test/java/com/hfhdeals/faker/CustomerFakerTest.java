package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerFakerTest {

    @Autowired
    private CustomerFaker customerFaker;

    @Test
    public void it_can_make_a_new_instance_without_it_or_its_relations_being_persisted() {
        Customer customer = customerFaker.make();

        assertNull(customer.getId());
    }

    @Test
    @Transactional
    public void it_can_make_and_persist_a_new_instance_with_its_relations_also_persisted() {
        Customer customer = customerFaker.create();

        assertNotNull(customer.getId());
    }
}