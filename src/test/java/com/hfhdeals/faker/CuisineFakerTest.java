package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Cuisine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CuisineFakerTest {

    @Autowired
    private CuisineFaker cuisineFaker;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_make_a_new_instance_without_it_or_its_relations_being_persisted() {
        Cuisine cuisine = cuisineFaker.make();

        assertNull(cuisine.getId());
    }

    @Test
    @Transactional
    public void it_can_make_and_persist_a_new_instance_with_its_relations_also_persisted() {
        Cuisine cuisine = cuisineFaker.create();

        assertNotNull(cuisine.getId());
    }
}