package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Outlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OutletFakerTest {

    @Autowired
    private OutletFaker outletFaker;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_make_a_new_instance_without_it_or_its_relations_being_persisted() {
        Outlet outlet = outletFaker.make();

        assertNull(outlet.getId());
        assertNull(outlet.getBrand().getId());
        assertNull(outlet.getBrand().getMerchant().getId());
    }

    @Test
    @Transactional
    public void it_can_make_and_persist_a_new_instance_with_its_relations_also_persisted() {
        Outlet outlet = outletFaker.create();

        assertNotNull(outlet.getId());
        assertNotNull(outlet.getBrand().getId());
        assertNotNull(outlet.getBrand().getMerchant().getId());
    }
}