package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Deal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DealFakerTest {

    @Autowired
    private DealFaker dealFaker;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_make_a_new_instance_without_it_or_its_relations_being_persisted() {
        Deal deal = dealFaker.make();

        assertNull(deal.getId());
        assertNull(deal.getOutlet().getId());
    }

    @Test
    @Transactional
    public void it_can_make_and_persist_a_new_instance_with_its_relations_also_persisted() {
        Deal deal = dealFaker.create();

        assertNotNull(deal.getId());
        assertNotNull(deal.getOutlet().getId());
        assertNotNull(deal.getOutlet().getBrand().getId());
        assertNotNull(deal.getOutlet().getBrand().getMerchant().getId());
    }

    @Test
    @Transactional
    public void it_should_have_null_published_at_if_auto_publish_date_is_null() {
        for (int i = 0; i < 10; i++) {
            Deal deal = dealFaker.createForSeeder();

            if (deal.getAutoPublishDate() == null) assertNull(deal.getPublishedAt());
        }
    }
}