package com.hfhdeals.database.dao.baserepository;//package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.faker.*;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryDeal;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.service.baseservice.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        CategoryDealService.class,

        CategoryService.class,
        CategoryFaker.class,

        DealService.class,
        DealFaker.class,

        OutletService.class,
        OutletFaker.class,

        BrandService.class,
        BrandFaker.class,

        MerchantService.class,
        MerchantFaker.class,
})
public class CategoryDealDaoTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private DealFaker dealFaker;

    @Autowired
    private CategoryFaker categoryFaker;

    @Autowired
    private CategoryDealService categoryDealService;

    @Autowired
    private CategoryDealDao categoryDealDao;

    private Category category;
    private Deal deal;
    private CategoryDeal categoryDeal;

    @Before
    public void setUp() throws Exception {
        category = categoryFaker.create();
        deal = dealFaker.create();

        categoryDeal = new CategoryDeal();
        categoryDeal.setCategory(category);
        categoryDeal.setDeal(deal);

        categoryDealService.save(categoryDeal);
    }

    @Test
    public void it_can_delete_a_category_deal_when_given_category_and_deal() {
//        // given an existing CategoryDeal which points to a Category and a Deal
//        assertNotNull(category.getId());
//        assertNotNull(deal.getId());
//        assertNotNull(categoryDeal.getId());
//
//        assertEquals(1, deal.getCategoryDeals().size());
//        assertEquals(1, category.getCategoryDeals().size());
//
//        assertEquals(deal.getId(), categoryDeal.getDeal().getId());
//        assertEquals(category.getId(), categoryDeal.getCategory().getId());
//
//        // when call the DAO's method to delete the categoryDeal
//        // by giving it the category and the deal
//        categoryDealDao.deleteCategoryDealByCategoryAndDeal(category, deal);
//
//        // then the categoryDeal should no longer exist when fetched
//        assertNull(categoryDealDao.getOne(categoryDeal.getId()));
    }
}