package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.faker.*;
import com.hfhdeals.model.jpaentities.OrderItem;
import com.hfhdeals.service.baseservice.*;
import com.hfhdeals.utils.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({
        OrderItemService.class,
        OrderItemFaker.class,

        DealService.class,
        DealFaker.class,

        OrderService.class,
        OrderFaker.class,

        MerchantService.class,
        MerchantFaker.class,

        BrandService.class,
        BrandFaker.class,

        OutletService.class,
        OutletFaker.class,

        CustomerService.class,
        CustomerFaker.class,
})
public class OrderItemDaoTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private OrderItemFaker orderItemFaker;
    private OrderItem orderItem;
    private OrderItem orderItem2;
    private OrderItem orderItem3;

    @Before
    public void setUp() throws Exception {
        orderItem = orderItemFaker.create();
        orderItem2 = orderItemFaker.create();
        orderItem3 = orderItemFaker.create();
    }

    @Test
    public void it_can_count_num_order_items_created_between_inclusive_two_dates() throws ParseException {
        // given 3 order items
        assertNotNull(orderItem.getId());
        assertNotNull(orderItem2.getId());
        assertNotNull(orderItem3.getId());

        // where 2 are created between 1-Jan-2018 and 31-Dec-2018
        // and the 3rd created on 31-Dec-2017
        Date lowerBoundary = DateUtils.fromString("2018-01-01 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
        Date upperBoundary = DateUtils.fromString("2018-12-31 23:59:59", DateUtils.DateFormat.MYSQL_FORMAT);

        orderItem.setCreatedAt(DateUtils.fromString("2018-01-01 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT));
        orderItem2.setCreatedAt(DateUtils.fromString("2018-12-31 23:59:59", DateUtils.DateFormat.MYSQL_FORMAT));
        orderItem3.setCreatedAt(DateUtils.fromString("2017-12-31 23:59:59", DateUtils.DateFormat.MYSQL_FORMAT));
        assertTrue(DateUtils.isDateBetweenInclusive(orderItem.getCreatedAt(), lowerBoundary, upperBoundary));
        assertTrue(DateUtils.isDateBetweenInclusive(orderItem2.getCreatedAt(), lowerBoundary, upperBoundary));
        assertFalse(DateUtils.isDateBetweenInclusive(orderItem3.getCreatedAt(), lowerBoundary, upperBoundary));

        // when we fetch the count of deals created between 2 dates
        int count = orderItemDao.countAllByDeletedAtIsNullAndCreatedAtBetween(lowerBoundary, upperBoundary);

        // then we should get 2 not 3
        assertEquals(Math.toIntExact(2), count);
    }
}