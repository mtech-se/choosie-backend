package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Merchant;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MerchantDaoTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private MerchantDao merchantDao;

    private Merchant merchant;
    private Merchant merchant2;

    @Before
    public void setUp() throws Exception {
        merchant = new Merchant();
        merchant.setName("xxx");
        merchant.setMailingAddress("xxx");
        merchant.setBusinessRegistrationNumber("xxx");

        merchant2 = new Merchant();
        merchant2.setName("yyy");
        merchant2.setMailingAddress("yyy");
        merchant2.setBusinessRegistrationNumber("yyy");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_fetch_all_merchants() {
        // given 2 merchants in persistence
        testEntityManager.persist(merchant);
        testEntityManager.persist(merchant2);

        // when we fetch all
        List<Merchant> merchants = merchantDao.findAll();

        // then we should get a collection of 2 merchants
        assertEquals(2, merchants.size());
    }

    @Test
    public void it_can_fetch_all_merchants_without_soft_deleted() {
        // given 2 merchants in persistence where 1 is soft-deleted
        testEntityManager.persist(merchant);
        merchant2.setDeletedAt(new Date());
        testEntityManager.persist(merchant2);

        // when we fetch all
        List<Merchant> merchants = merchantDao.findAll();

        // then we should get a collection of 1 merchant
        assertEquals(1, merchants.size());
        assertEquals(merchant, merchants.get(0));
    }

    @Test
    public void it_can_fetch_all_merchants_by_name_containing_ignore_case() {
        // given 2 merchants in persistence where 1 is soft-deleted
        merchant.setName("Black Hole Foods");
        testEntityManager.persist(merchant);
        merchant2.setName("Orion Foods");
        testEntityManager.persist(merchant2);

        // when we fetch all by name containing
        Page<Merchant> page = merchantDao.findAllByNameIgnoreCaseContainingAndDeletedAtIsNull("foods", null);
        List<Merchant> merchants = page.get().collect(Collectors.toList());

        // then we should get a collection of 2 merchants
        assertEquals(2, merchants.size());
    }

    @Test
    public void it_can_fetch_all_merchants_by_name_containing_ignore_case_without_soft_deleted() {
        // given 2 merchants in persistence where 1 is soft-deleted
        merchant.setName("Black Hole Foods");
        testEntityManager.persist(merchant);
        merchant2.setName("Orion Foods");
        merchant2.setDeletedAt(new Date());
        testEntityManager.persist(merchant2);

        // when we fetch all by name containing
        Page<Merchant> page = merchantDao.findAllByNameIgnoreCaseContainingAndDeletedAtIsNull("foods", null);
        List<Merchant> merchants = page.get().collect(Collectors.toList());

        // then we should get a collection of 1 merchant
        assertEquals(1, merchants.size());
        assertEquals(merchant, merchants.get(0));
    }
}