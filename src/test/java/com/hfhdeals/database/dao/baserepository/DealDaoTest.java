//package com.hfhdeals.database.dao.baserepository;
//
//import com.hfhdeals.faker.*;
//import com.hfhdeals.model.jpaentities.Deal;
//import com.hfhdeals.model.jpaentities.OrderItem;
//import com.hfhdeals.service.baseservice.*;
//import com.hfhdeals.utils.DateUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.context.annotation.Import;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.text.ParseException;
//import java.util.Date;
//import java.util.List;
//
//import static org.junit.Assert.assertEquals;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//@Import({
//        OrderItemService.class,
//        OrderItemFaker.class,
//
//        DealService.class,
//        DealFaker.class,
//
//        OrderService.class,
//        OrderFaker.class,
//
//        MerchantService.class,
//        MerchantFaker.class,
//
//        BrandService.class,
//        BrandFaker.class,
//
//        OutletService.class,
//        OutletFaker.class,
//
//        CustomerService.class,
//        CustomerFaker.class,
//})
//public class DealDaoTest {
//    @Autowired
//    private TestEntityManager testEntityManager;
//
//    @Autowired
//    private DealFaker dealFaker;
//
//    @Autowired
//    private OrderItemFaker orderItemFaker;
//
//    @Autowired
//    private DealDao dealDao;
//
//    private Deal deal;
//    private Deal deal2;
//    private Deal deal3;
//    private Deal deal4;
//    private Deal deal5;
//    private Deal deal6;
//
//    @Before
//    public void setUp() throws Exception {
//        deal = dealFaker.create();
//        testEntityManager.flush();
//        deal2 = dealFaker.create();
//        testEntityManager.flush();
//        deal3 = dealFaker.create();
//        deal4 = dealFaker.create();
//        deal5 = dealFaker.create();
//        deal6 = dealFaker.create();
//
//
//        Date createdAt = DateUtils.fromString("2018-01-02 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
//        deal.setCreatedAt(createdAt);
//        deal2.setCreatedAt(createdAt);
//        deal3.setCreatedAt(createdAt);
//        deal4.setCreatedAt(createdAt);
//        deal5.setCreatedAt(createdAt);
//        deal6.setCreatedAt(createdAt);
//
//        addOrderItems(deal, 1);
//        addOrderItems(deal2, 2);
//        addOrderItems(deal3, 3);
//        addOrderItems(deal4, 4);
//        addOrderItems(deal5, 5);
//        addOrderItems(deal6, 6);
//    }
//
//    private void addOrderItems(Deal deal, int numOrderItemsToAdd) {
//        for (int i = 0; i < numOrderItemsToAdd; i++) {
//            OrderItem orderItem = orderItemFaker.create();
//            orderItem.setDeal(deal);
//        }
//
//        testEntityManager.flush();
//    }
//
//    @Test
//    public void it_returns_top_deals_ordered_by_purchase_count_with_their_purchase_counts() throws ParseException {
//        // given 6 deals with 6, 5, ... , 1 order_items to each deal
//        assertEquals(1, deal.getOrderItems().size());
//        assertEquals(2, deal2.getOrderItems().size());
//        assertEquals(3, deal3.getOrderItems().size());
//        assertEquals(4, deal4.getOrderItems().size());
//        assertEquals(5, deal5.getOrderItems().size());
//        assertEquals(6, deal6.getOrderItems().size());
//
//        // when we get the top deals by purchase count
//        Date startDate = DateUtils.fromString("2018-01-01 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
//        Date endDate = DateUtils.fromString("2018-12-31 00:00:00", DateUtils.DateFormat.MYSQL_FORMAT);
//
//        List<Object> deals = dealDao.getDealsOrderedByPurchaseCount(startDate, endDate, new PageRequest(0, 1));
//        System.err.println(deals);
//        assertEquals(6, deals.size());
////        assertEquals(deal6.getId(), deals.get(0).getId());
////        assertEquals(deal5.getId(), deals.get(1).getId());
////        assertEquals(deal4.getId(), deals.get(2).getId());
////        assertEquals(deal3.getId(), deals.get(3).getId());
////        assertEquals(deal2.getId(), deals.get(4).getId());
////        assertEquals(deal.getId(), deals.get(5).getId());
//
//        // then we should get a collection of deals in decreasing order of num order items
//    }
//}