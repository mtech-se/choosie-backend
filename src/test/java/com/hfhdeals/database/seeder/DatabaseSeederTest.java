package com.hfhdeals.database.seeder;

import com.hfhdeals.service.baseservice.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class DatabaseSeederTest {
    @Autowired
    private DatabaseSeeder databaseSeeder;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CuisineService cuisineService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private OutletService outletService;

    @Autowired
    private DealService dealService;

    @Autowired
    private DealCustomerClickService dealCustomerClickService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private OrderItemService orderItemService;

//    @Autowired
//    private CategoryDealService categoryDealService;

//    @Autowired
//    private CuisineDealService cuisineDealService;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @Transactional
    public void run() {
        databaseSeeder.run();

        assertTrue("cuisines table has no records", cuisineService.count() > 0);
        assertTrue("countries table has no records", countryService.count() > 0);
        assertTrue("categories table has no records", categoryService.count() > 0);
        assertTrue("merchants table has no records", merchantService.count() > 0);
        assertTrue("customers table has no records", customerService.count() > 0);
        assertTrue("admins table has no records", adminService.count() > 0);

        assertTrue("locations table has no records", locationService.count() > 0);
        assertTrue("contact_persons table has no records", contactPersonService.count() > 0);
        assertTrue("brands table has no records", brandService.count() > 0);
        assertTrue("outlets table has no records", outletService.count() > 0);
        assertTrue("orders table has no records", orderService.count() > 0);

        assertTrue("deals table has no records", dealService.count() > 0);
        assertTrue("deal_customer_clicks table has no records", dealCustomerClickService.count() > 0);
        assertTrue("photos table has no records", photoService.count() > 0);
//        assertTrue("orders table has no records", categoryDealService.count() > 0);
//        assertTrue("orders table has no records", cuisineDealService.count() > 0);
        assertTrue("order_items table has no records", orderItemService.count() > 0);
    }
}