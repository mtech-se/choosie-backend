package com.hfhdeals.database.seeder.tableseeder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderItemsTableSeederTest {

    @Autowired
    private OrderItemsTableSeeder orderItemsTableSeeder;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @Transactional
    public void run() {
        orderItemsTableSeeder.run();
    }
}