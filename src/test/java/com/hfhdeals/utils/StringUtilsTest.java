package com.hfhdeals.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringUtilsTest {

    @Test
    public void removeNonAlphanumeric() {
        String inputString = "John Doe Jr.";
        String resultString = StringUtils.removeNonAlphanumeric(inputString);
        String expectedString = "John Doe Jr";

        assertEquals(expectedString, resultString);
    }

    @Test
    public void replaceSpacesWithUnderscores() {
        String inputString = "John Doe";
        String resultString = StringUtils.replaceSpacesWithUnderscores(inputString);
        String expectedString = "John_Doe";

        assertEquals(expectedString, resultString);
    }
}