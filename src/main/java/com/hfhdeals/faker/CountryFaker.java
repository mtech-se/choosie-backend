package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.service.baseservice.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CountryFaker {
    private static final Faker faker = new Faker();

    private final CountryService countryService;

    @Autowired
    public CountryFaker(CountryService countryService) {
        this.countryService = countryService;
    }

    public Country make() {
        return make(faker.address().country());
    }

    public Country make(String name) {
        UUID uuid = UUID.randomUUID();

        Country country = new Country();
        country.setName(name + uuid);
        return country;
    }

    public Country create() {
        return countryService.save(make());
    }
}
