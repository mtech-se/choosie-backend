package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PhotoFaker {
    private final PhotoService photoService;

    @Autowired
    public PhotoFaker(PhotoService photoService) {
        this.photoService = photoService;
    }

    public Photo make() {
        Photo photo = new Photo();
        photo.setPath("xxx");

        return photo;
    }

    public Photo create() {
        Photo photo = make();

        photoService.save(photo);

        return photo;
    }
}
