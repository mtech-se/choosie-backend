package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.model.jpaentities.OrderItem;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.service.baseservice.OrderItemService;
import com.hfhdeals.service.baseservice.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderItemFaker {
    private final DealService dealService;
    private final DealFaker dealFaker;
    private final OrderItemService orderItemService;
    private final OrderService orderService;
    private final OrderFaker orderFaker;

    @Autowired
    public OrderItemFaker(DealService dealService, DealFaker dealFaker, OrderItemService orderItemService, OrderService orderService, OrderFaker orderFaker) {
        this.dealService = dealService;
        this.dealFaker = dealFaker;
        this.orderItemService = orderItemService;
        this.orderService = orderService;
        this.orderFaker = orderFaker;
    }

    public OrderItem make() {
        Deal deal = dealFaker.make();
        Order order = orderFaker.make();

        return new OrderItem(deal, order);
    }

    // instead of walking the relations and persisting them one by one,
    // we will go with the approach of simply swopping out the relation
    // with a created version from the respective fakers
    public OrderItem create() {
        OrderItem orderItem = make();

        orderItem.setDeal(dealFaker.create());
        orderItem.setOrder(orderFaker.create());

        return orderItemService.save(orderItem);
    }

}
