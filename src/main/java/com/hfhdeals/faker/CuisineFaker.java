package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.service.baseservice.CuisineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CuisineFaker {
    private final CuisineService cuisineService;

    @Autowired
    public CuisineFaker(CuisineService cuisineService) {
        this.cuisineService = cuisineService;
    }

    public Cuisine make() {
        Faker faker = new Faker();

        Cuisine cuisine = new Cuisine();
        cuisine.setName(faker.food().spice());
        cuisine.setDescription(faker.lorem().sentence(10));

        return cuisine;
    }

    public Cuisine create() {
        Cuisine cuisine = make();

        cuisineService.save(cuisine);

        return cuisine;
    }
}
