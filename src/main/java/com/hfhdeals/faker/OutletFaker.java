package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.service.baseservice.OutletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OutletFaker {
    private final MerchantFaker merchantFaker;
    private final BrandFaker brandFaker;
    private final MerchantService merchantService;
    private final BrandService brandService;
    private final OutletService outletService;

    private Faker faker = new Faker();

    @Autowired
    public OutletFaker(MerchantFaker merchantFaker, BrandFaker brandFaker, MerchantService merchantService, BrandService brandService, OutletService outletService) {
        this.merchantFaker = merchantFaker;
        this.brandFaker = brandFaker;
        this.merchantService = merchantService;
        this.brandService = brandService;
        this.outletService = outletService;
    }

    public Outlet make() {
        Brand brand = brandFaker.make();

        Outlet outlet = new Outlet();
        outlet.setName("xxx");

        brand.addOutlet(outlet);

        return outlet;
    }

    public Outlet create() {
        Outlet outlet = make();
        merchantService.save(outlet.getBrand().getMerchant());
        brandService.save(outlet.getBrand());
        outletService.save(outlet);

        return outlet;
    }

    public Outlet make(Location location, Brand brand) {
        Outlet outlet = new Outlet();
        //Outlet Name would be Pastamania or Pastamania 39 or Pastamania @ Kallang
        outlet.setName(brand.getName() + (faker.random().nextBoolean() ? " " + faker.random().nextInt(10, 99) : faker.random().nextBoolean() ? "" : " @ " + location.getName()));
        outlet.setBrand(brand);
        outlet.setAddress(faker.address().streetAddress() + ", " + location.getName());
        return outlet;
    }
}
