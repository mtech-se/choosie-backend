package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContactPersonFaker {
    private final MerchantService merchantService;
    private final MerchantFaker merchantFaker;

    @Autowired
    public ContactPersonFaker(MerchantService merchantService, MerchantFaker merchantFaker) {
        this.merchantService = merchantService;
        this.merchantFaker = merchantFaker;
    }

    public ContactPerson make() {
        Merchant merchant = merchantFaker.make();

        ContactPerson contactPerson = new ContactPerson();

        Faker faker = new Faker();
        String name = faker.name().name();

        String afterLowerCasing = name.toLowerCase();
        String afterRemovingSpecials = StringUtils.removeNonAlphanumeric(afterLowerCasing);
        String nameForEmail = StringUtils.replaceSpacesWithUnderscores(afterRemovingSpecials);

        contactPerson.setName(name);
        contactPerson.setPosition(faker.job().position());
        contactPerson.setMobileNumber(faker.phoneNumber().phoneNumber());
        contactPerson.setEmail(nameForEmail + "@example.com");

        merchant.addContactPerson(contactPerson);

        return contactPerson;
    }

    public ContactPerson make(Merchant merchant) {
        String merchantName = merchant.getName();
        String afterLowerCasing = merchantName.toLowerCase();
        String afterRemovingSpecials = StringUtils.removeNonAlphanumeric(afterLowerCasing);
        String emailDomain = StringUtils.replaceSpacesWithUnderscores(afterRemovingSpecials);

        ContactPerson contactPerson = make();

        String currentEmail = contactPerson.getEmail();
        String newEmail = currentEmail.replace("example", emailDomain);

        contactPerson.setEmail(newEmail);
        contactPerson.setMerchant(merchant);

        return contactPerson;
    }

    public ContactPerson create() {
        ContactPerson contactPerson = make();

        merchantService.save(contactPerson.getMerchant());

        return contactPerson;
    }
}
