package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Admin;
import com.hfhdeals.service.baseservice.AdminService;
import com.hfhdeals.utils.CustomFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class AdminFaker {
    private final AdminService adminService;
    private final CustomFaker customFaker = new CustomFaker();
    private final Faker faker = new Faker();

    @Autowired
    public AdminFaker(AdminService adminService) {
        this.adminService = adminService;
    }

    public Admin make() {
        CustomFaker.Person person = customFaker.makePerson();

        Admin admin = new Admin();
        admin.setFirstName(person.getFirstName());
        admin.setLastName(person.getLastName());
        admin.setEmail(person.getEmail());
        admin.setPassword(DigestUtils.md5DigestAsHex(faker.internet().password(6, 10, true, true).getBytes()));
        admin.setGender(faker.random().nextBoolean() ? "Male" : "Female");
        return admin;
    }

    public Admin create() {
        Admin admin = make();
        adminService.save(admin);
        return admin;
    }
}