package com.hfhdeals.faker;

import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.utils.CustomFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomerFaker {
    private final CustomerService customerService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CustomFaker customFaker = new CustomFaker();
    private final Faker faker = new Faker();

    @Autowired
    public CustomerFaker(CustomerService customerService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.customerService = customerService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public Customer make() {
        CustomFaker.Person person = customFaker.makePerson();

        Address address = faker.address();

        Customer customer = new Customer();
        customer.setBillingFirstName(person.getFirstName());
        customer.setBillingLastName(person.getLastName());
        customer.setBillingAddLineOne(address.streetAddress());
        customer.setBillingAddLineTwo(address.city() + ", " + address.country());
        customer.setBillingAddPostalCode(address.zipCode());
        customer.setBillingCellphone(faker.phoneNumber().cellPhone());
        customer.setBillingTelephone(faker.phoneNumber().phoneNumber());
        customer.setFirstName(person.getFirstName());
        customer.setLastName(person.getLastName());
        customer.setEmail(person.getEmail());
        customer.setPassword(bCryptPasswordEncoder.encode("secret"));
        customer.setGender(faker.random().nextBoolean() ? "Male" : "Female");

        return customer;
    }

    public Customer create() {
        Customer customer = make();

        return customerService.save(customer);
    }
}