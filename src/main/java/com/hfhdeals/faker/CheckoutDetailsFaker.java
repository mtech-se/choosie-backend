//package com.hfhdeals.faker;
//
//import com.github.javafaker.Faker;
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.model.jpaentities.Customer;
//import com.hfhdeals.service.PaymentService;
//import com.hfhdeals.service.baseservice.CustomerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//@Component
//public class CheckoutDetailsFaker {
//
//    @Autowired
//    PaymentService paymentService;
//    @Autowired
//    CustomerService customerService;
//
//    public CheckoutDetails make() {
//
//        Faker faker = new Faker();
//
//        CheckoutDetails checkoutDetails = new CheckoutDetails();
//
//        checkoutDetails.setAddress(faker.address().buildingNumber() + faker.address().streetAddress());
//        checkoutDetails.setCity(faker.address().city());
//        checkoutDetails.setCountry(faker.address().country());
//        checkoutDetails.setFirst_name(faker.name().firstName());
//        checkoutDetails.setLast_name(faker.name().lastName());
//        checkoutDetails.setPhone(faker.phoneNumber().phoneNumber());
//        checkoutDetails.setPostal_code((int) faker.number().randomNumber(6, true));
//
//        List<Customer> customerList = customerService.getAll();
//        checkoutDetails.setUser_id(faker.random().nextInt(customerList.get(0).getId(), customerList.get(customerList.size() - 1).getId()));
//
//        return checkoutDetails;
//    }
//}
