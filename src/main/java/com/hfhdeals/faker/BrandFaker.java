package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class BrandFaker {
    private final BrandService brandService;
    private final MerchantFaker merchantFaker;
    private final MerchantService merchantService;

    private Faker faker = new Faker();

    private static List<String> brandNames = Arrays.asList("4 Fingers Crispy Chicken", "Baskin-Robbins", "BreadTalk", "Carl's Jr.", "Chili's", "Crystal Jade",
            "Dairy Queen", "Din Tai Fung", "Dome (coffeehouse)", "Eighteen Chefs", "Food Republic", "Genki Sushi", "Hard Rock Cafe", "Ippudo", "J.CO Donuts",
            "Jumbo Seafood", "Killiney Kopitiam", "Kopi tiam", "Koufu (company)", "Long Beach Seafood Restaurant", "McDonald's", "MOS Burger", "Mr Bean (company)",
            "Murugan Idli Shop", "Old Chang Kee", "Pastamania", "Pizza Hut", "Popeyes", "Sakae Sushi", "Standing Sushi Bar", "Subway (restaurant)", "Thai Express",
            "VeganBurg", "Ya Kun Kaya Toast", "Yogen Fruz", "Yoshinoya", "Hardee's", "Orange Julius", "Sibylla", "Little Caesars", "Papa John's Pizza", "Arby's",
            "llibee", "Jimmy John's", "Jack in the Box", "Chick-fil-A", "Applebee's", "Panera Bread", "Chipotle Mexican Grill", "Shihao", "Panda Express", "Denny's",
            "Church's Chicken", "WingStreet", "Auntie Anne's", "Five Guys", "Quiznos", "Papa Murphy's", "Long John Silver's", "Buffalo Wild Wings", "A&W Restaurants",
            "Firehouse Subs", "Krispy Kreme", "Jersey Mike's Subs", "Nando's", "Kennedy Fried Chicken", "Haagen-Dazs", "Jamba Juice", "Bakery Cafe", "Sbarro",
            "Zaxby's", "Blimpie", "Checkers and Rally's", "Cinnabon", "Whataburger", "Southern Fried Chicken", "Wingstop", "Einstein Bros. Bagels",
            "Bojangles' Famous Chicken 'n Biscuits", "Moe's Southwest Grill", "Qdoba Mexican Eats", "Rita's Italian Ice", "Smoothie King", "Chuck E. Cheese's",
            "Del Taco", "Steak 'n Shake", "Culver's", "Steers", "Captain D's", "Doree", "Charley's Grilled Subs", "Round Table Pizza", "Shakey's", "CiCi's Pizza",
            "Country Style");

    @Autowired
    public BrandFaker(BrandService brandService, MerchantFaker merchantFaker, MerchantService merchantService) {
        this.brandService = brandService;
        this.merchantFaker = merchantFaker;
        this.merchantService = merchantService;
    }

    public Brand make() {
        String brandName = brandNames.get(faker.random().nextInt(0, brandNames.size() - 1));

        Merchant merchant = merchantFaker.make();

        Brand brand = new Brand();
        brand.setName(brandName);

        merchant.addBrand(brand);

        return brand;
    }

    public Brand create() {
        Brand brand = make();
        merchantService.save(brand.getMerchant());

        return brand;
    }

    public void create(List<Merchant> allMerchant) {
        // Brands will be randomly distributed between all merchants
        for (String brandName : brandNames) {
            Brand brand = new Brand();
            brand.setName(brandName);
            brand.setMerchant(allMerchant.get(faker.random().nextInt(0, allMerchant.size() - 1)));
            brandService.save(brand);
        }
    }

}
