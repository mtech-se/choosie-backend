//package com.hfhdeals.faker;
//
//import com.github.javafaker.Faker;
//import com.hfhdeals.model.jpaentities.Deal;
//import com.hfhdeals.model.jpaentities.Purchase;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//import java.util.concurrent.TimeUnit;
//
//@Component
//public class PurchaseFaker {
//    @Autowired
//    DealFaker dealFaker;
//
//    public Purchase make(Deal deal) {
//        Faker faker = new Faker();
//        Date redeemedAt = faker.bool().bool() ? faker.date().past(30, TimeUnit.DAYS) : null;
//        Date paidMerchantAt = null;
//        if (redeemedAt != null) {
//            if (faker.bool().bool())
//                paidMerchantAt = faker.date().between(redeemedAt, new Date());
//        }
//
//        Purchase purchase = new Purchase();
//        purchase.setDeal(deal);
//        purchase.setRedeemedAt(redeemedAt);
//        purchase.setPaidMerchantAt(paidMerchantAt);
//
//        return purchase;
//    }
//}
