package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.service.baseservice.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocationFaker {
    private final CountryService countryService;
    private final CountryFaker countryFaker;

    private Faker faker = new Faker();

    @Autowired
    public LocationFaker(CountryService countryService, CountryFaker countryFaker) {
        this.countryService = countryService;
        this.countryFaker = countryFaker;
    }

    public Location make(Country country) {
        Location location = new Location();
        location.setName(faker.address().cityName());
        location.setCountry(country);
        return location;
    }

    public Location make() {
        Country country = countryFaker.make();

        Location location = new Location();
        location.setName(faker.address().cityName());

        country.addLocation(location);

        return location;
    }

    public Location create() {
        Location location = make();

        countryService.save(location.getCountry());

        return location;
    }
}
