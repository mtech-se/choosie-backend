package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.service.baseservice.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderFaker {
    private final CustomerService customerService;
    private final OrderService orderService;
    private final CustomerFaker customerFaker;

    @Autowired
    public OrderFaker(CustomerService customerService, OrderService orderService, CustomerFaker customerFaker) {
        this.customerService = customerService;
        this.orderService = orderService;
        this.customerFaker = customerFaker;
    }

    public Order make() {
        Customer customer = customerFaker.make();

        Order order = new Order();

        customer.addOrder(order);

        return order;
    }

    public Order create() {
        Order order = make();

        customerService.save(order.getCustomer());

        return orderService.save(order);
    }

}
