package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.service.baseservice.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryFaker {
    private final CategoryService categoryService;

    @Autowired
    public CategoryFaker(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Category make() {
        Faker faker = new Faker();

        Category category = new Category();
        category.setName(String.join(" ", faker.lorem().words(3)));
        category.setDescription(faker.lebowski().quote());

        return category;
    }

    public Category create() {
        Category category = make();

        categoryService.save(category);

        return category;
    }
}
