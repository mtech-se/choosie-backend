package com.hfhdeals.faker;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MerchantFaker {

    private final MerchantService merchantService;

    @Autowired
    public MerchantFaker(final MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    public Merchant make() {
        Faker faker = new Faker();

        Merchant merchant = new Merchant();
        merchant.setName(faker.company().name());
        merchant.setBusinessRegistrationNumber(faker.bothify("???########?"));
        merchant.setMailingAddress(faker.address().fullAddress());

        return merchant;
    }

    public Merchant create() {
        Merchant merchant = make();
        return merchantService.save(merchant);
    }
}
