package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.service.baseservice.OutletService;
import com.hfhdeals.utils.CustomFaker;
import com.hfhdeals.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class DealFaker {
    private static final List<String> dealTitles = Arrays.asList(
            "5% Cash Back",
            "One (1) Main Course for 1 Person",
            "(Sat - Sun) Hi-Tea Buffet for 1 Person",
            "(Fri - Sun) International Buffet Dinner for 2 People",
            "(Weekend) BBQ Buffet for 1 Person",
            "All-You-Can-Eat Food Buffet", "20% Off",
            "Kids Dine Free",
            "50% Off Dinner Menu",
            "35% Cash Back for Ala Carte",
            "Weekend Bonanza Free Flow of Drinks"
    );
    private final MerchantService merchantService;
    private final BrandService brandService;
    private final OutletService outletService;
    private final OutletFaker outletFaker;
    private final DealService dealService;
    private CustomFaker customFaker = new CustomFaker();

    @Autowired
    public DealFaker(MerchantService merchantService, BrandService brandService, OutletService outletService, DealService dealService, OutletFaker outletFaker) {
        this.merchantService = merchantService;
        this.brandService = brandService;
        this.outletService = outletService;
        this.dealService = dealService;
        this.outletFaker = outletFaker;
    }

    public Deal make() {
        Outlet outlet = outletFaker.make();

        double discountedPriceMultiplier = (double) customFaker.number().numberBetween(50, 90) / (double) 100;
        int originalPrice = customFaker.random().nextInt(500, 10000);  // between $5 to $100

        Deal deal = new Deal();
        deal.setTitle("xxx");
        deal.setOriginalPrice(originalPrice);
        deal.setDiscountedPrice((int) (originalPrice * discountedPriceMultiplier));

        outlet.addDeal(deal);

        return deal;
    }

    public Deal create() {
        Deal deal = make();

        merchantService.save(deal.getOutlet().getBrand().getMerchant());
        brandService.save(deal.getOutlet().getBrand());
        outletService.save(deal.getOutlet());
        dealService.save(deal);

        return deal;
    }

    public Deal createForSeeder() {
        // created_at to be randomized any time between 24 months ago and 3 months ago
        // auto_publish_date can be 50% chance of null, or 1-30 days after created_at
        // if auto_publish_date is not null, then set published_at to be same as auto_publish_date
        // else, set published_at to be same as created_at
        // redemption_expiry_date to be 1-60 days after published_at
        // deleted_at has 90% chance of null, or must be before now and after redemption_expiry_date
        // archived_at is 80% chance of null,
        // or to be anywhere after redemption_expiry_date but before now,
        // or between redemption_expiry_date and deleted_at

        Date threeMonthsAgo = DateUtils.asDate(LocalDate.now().minusMonths(3));
        Date twelveMonthsAgo = DateUtils.asDate(LocalDate.now().minusMonths(24));
        Date createdAt = customFaker.date().between(twelveMonthsAgo, threeMonthsAgo);

        boolean isAutoPublishDateToBeSet = customFaker.bool(50);
        Date autoPublishDate = null;
        if (isAutoPublishDateToBeSet) autoPublishDate = customFaker.date().future(30, TimeUnit.DAYS, createdAt);

        Date publishedAt = autoPublishDate == null ? null : (Date) createdAt.clone();

        Date redemptionExpiryDate = publishedAt == null ? null : customFaker.date().future(60, TimeUnit.DAYS, publishedAt);

        Date deletedAt = null;
        boolean isDeletedAtSet = customFaker.bool(10);
        if (isDeletedAtSet) {
            if (redemptionExpiryDate != null) deletedAt = customFaker.date().between(redemptionExpiryDate, new Date());
        }

        boolean isArchivedAtSet = customFaker.bool(20);
        Date archivedAt = null;
        if (isArchivedAtSet && redemptionExpiryDate != null) {
            if (deletedAt == null) {
                archivedAt = customFaker.date().between(redemptionExpiryDate, new Date());
            } else {
                archivedAt = customFaker.date().between(redemptionExpiryDate, deletedAt);
            }
        }

        double discountedPriceMultiplier = (double) customFaker.number().numberBetween(50, 90) / (double) 100;
        int originalPrice = customFaker.random().nextInt(500, 10000);  // between $5 to $100
        boolean isDealOfTheMonth = customFaker.bool(10);

        List<Outlet> outlets = outletService.getAll();
        Outlet outlet = outlets.get(customFaker.random().nextInt(0, outlets.size() - 1));

        Deal deal = new Deal();
        deal.setTitle(dealTitles.get(customFaker.random().nextInt(0, dealTitles.size() - 1)));
        deal.setDescription(customFaker.shakespeare().hamletQuote());
        deal.setOriginalPrice(originalPrice);
        deal.setDiscountedPrice((int) (originalPrice * discountedPriceMultiplier));
        deal.setTermsOfUse(customFaker.harryPotter().quote());
        deal.setRedemptionInstructions(customFaker.shakespeare().romeoAndJulietQuote());
        deal.setMaxQuantity(customFaker.number().numberBetween(5, 200));
        deal.setDealOfTheMonth(isDealOfTheMonth);
        deal.setOutlet(outlet);
        deal.setAutoPublishDate(autoPublishDate);
        deal.setPublishedAt(publishedAt);
        deal.setRedemptionExpiryDate(redemptionExpiryDate);
        deal.setArchivedAt(archivedAt);
        deal.setDeletedAt(deletedAt);

        dealService.save(deal);
        deal.setCreatedAt(createdAt);

        return deal;
    }
}
