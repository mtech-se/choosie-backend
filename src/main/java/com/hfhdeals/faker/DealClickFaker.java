package com.hfhdeals.faker;

import com.hfhdeals.model.jpaentities.DealCustomerClick;
import org.springframework.stereotype.Component;

@Component
public class DealClickFaker {
    public DealCustomerClick make(int dealId, int customerId) {
        DealCustomerClick dealCustomerClick = new DealCustomerClick();
        dealCustomerClick.setCustomerId(customerId);
        dealCustomerClick.setDealId(dealId);
        return dealCustomerClick;
    }
}
