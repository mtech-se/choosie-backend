package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.requestbody.CustomerRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.service.restservice.SRERESTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final CustomerService customerService;
    private final SRERESTService sreRESTService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public CustomerController(CustomerService customerService, SRERESTService sreRESTService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.customerService = customerService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.sreRESTService = sreRESTService;
    }

    @GetMapping
    public Page<Customer> index(
            @RequestParam(value = "name", required = false) String name,
            Pageable pageable
    ) {
        if (name != null) return customerService.getAll(name, pageable);
        return customerService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Customer show(@PathVariable("id") Integer id) {
        return customerService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@Validated(Create.class) @RequestBody CustomerRequestBody requestBody) {
        System.out.println(requestBody);
        System.out.println(bCryptPasswordEncoder == null);

        Customer customer = new Customer();
        customer.setEmail(requestBody.getEmail());
        customer.setFirstName(requestBody.getFirstName());
        customer.setLastName(requestBody.getLastName());
        customer.setGender(requestBody.getGender());
        customer.setPassword(bCryptPasswordEncoder.encode(requestBody.getPassword()));
        customer.setLastName(requestBody.getLastName());
        customer.setBillingFirstName(requestBody.getBillingFirstName());
        customer.setBillingLastName(requestBody.getBillingLastName());
        customer.setBillingCellphone(requestBody.getBillingCellphone());
        customer.setBillingTelephone(requestBody.getBillingTelephone());
        customer.setBillingAddLineOne(requestBody.getBillingAddLineOne());
        customer.setBillingAddLineTwo(requestBody.getBillingAddLineTwo());
        customer.setBillingAddPostalCode(requestBody.getBillingAddPostalCode());

        sreRESTService.postUsers();

        return customerService.save(customer);
    }

    @PutMapping("/{id}")
    public Customer update(@Validated(Update.class) @RequestBody CustomerRequestBody requestBody, @PathVariable("id") Integer id) {
        Customer customer = customerService.getById(id);

        if (requestBody.getEmail() != null) customer.setEmail(requestBody.getEmail());
        if (requestBody.getFirstName() != null) customer.setFirstName(requestBody.getFirstName());
        if (requestBody.getLastName() != null) customer.setLastName(requestBody.getLastName());
        if (requestBody.getGender() != null) customer.setGender(requestBody.getGender());
        if (requestBody.getPassword() != null) customer.setPassword(requestBody.getPassword());
        if (requestBody.getBillingFirstName() != null) customer.setBillingFirstName(requestBody.getBillingFirstName());
        if (requestBody.getBillingLastName() != null) customer.setBillingLastName(requestBody.getBillingLastName());
        if (requestBody.getBillingCellphone() != null) customer.setBillingCellphone(requestBody.getBillingCellphone());
        if (requestBody.getBillingTelephone() != null) customer.setBillingTelephone(requestBody.getBillingTelephone());
        if (requestBody.getBillingAddLineOne() != null) customer.setBillingAddLineOne(requestBody.getBillingAddLineOne());
        if (requestBody.getBillingAddLineTwo() != null) customer.setBillingAddLineTwo(requestBody.getBillingAddLineTwo());
        if (requestBody.getBillingAddPostalCode() != null) customer.setBillingAddPostalCode(requestBody.getBillingAddPostalCode());

        return customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        customerService.softDelete(id);
    }
}
