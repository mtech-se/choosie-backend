package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.BrandPhoto;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.requestbody.BrandRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.BrandPhotoService;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.MerchantService;
import com.hfhdeals.service.baseservice.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/brands")
public class BrandController {
    private final BrandService brandService;
    private final MerchantService merchantService;
    private final BrandPhotoService brandPhotoService;
    private final PhotoService photoService;

    @Autowired
    public BrandController(BrandService brandService, MerchantService merchantService, BrandPhotoService brandPhotoService, PhotoService photoService) {
        this.brandService = brandService;
        this.merchantService = merchantService;
        this.brandPhotoService = brandPhotoService;
        this.photoService = photoService;
    }

    @GetMapping
    public Page<Brand> index(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "merchant_id", required = false) Integer merchantId,
            Pageable pageable
    ) {
        if (name != null && merchantId != null) {
            return brandService.getAll(name, merchantId, pageable);
        } else if (name != null) {
            return brandService.getAll(name, pageable);
        } else if (merchantId != null) {
            return brandService.getAll(merchantId, pageable);
        }

        return brandService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Brand show(@PathVariable("id") Integer id) {
        return brandService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Brand create(@Validated(Create.class) @RequestBody BrandRequestBody requestBody) {
        Merchant merchant = merchantService.getById(requestBody.getMerchant().getId());

        Brand brand = new Brand();
        brand.setName(requestBody.getName());
        merchant.addBrand(brand);

        return brandService.save(brand);
    }

    @PostMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.CREATED)
    public Page<BrandPhoto> createBrandPhoto(@RequestParam(value = "photos") MultipartFile[] photos, @PathVariable("id") Integer brandId, Pageable pageable) throws Exception {
        Brand brand = brandService.getById(brandId);
        Page<BrandPhoto> currentPhotos = brandPhotoService.getBrandPhotosByBrandAndDeletedAtIsNull(brand, pageable);
        if (photos.length + currentPhotos.getTotalElements() <= 1) {
            Photo photo = new Photo();
            photo.setPath("/brands/" + brand.getId() + "/img.png");
            photoService.savePhotoAndFile(photo, photos[0]);
            brand.addPhoto(photo);
            brandService.update(brand);
        } else {
            throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Too Many Photos. Current Count = " + currentPhotos.getTotalElements());
        }
        return brandPhotoService.getBrandPhotosByBrandAndDeletedAtIsNull(brand, pageable);
    }

    @PutMapping("/{id}")
    public Brand update(@Validated(Update.class) @RequestBody BrandRequestBody requestBody, @PathVariable("id") Integer id) {
        Brand brand = brandService.getById(id);

        if (requestBody.getName() != null) brand.setName(requestBody.getName());

        return brandService.update(brand);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        brandService.softDelete(id);
    }

}
