package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.service.baseservice.CuisineService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cuisines")
public class CuisineController {
    private final CuisineService cuisineService;

    @Autowired
    public CuisineController(CuisineService cuisineService) {
        this.cuisineService = cuisineService;
    }

    @GetMapping
    public Page<Cuisine> index(
            @RequestParam(value = "name", required = false) String name,
            Pageable pageable
    ) throws JSONException {
        if (name != null) return cuisineService.getAll(name, pageable);
        return cuisineService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Cuisine show(@PathVariable("id") Integer id) {
        return cuisineService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cuisine create(@RequestBody Cuisine cuisine) {
        return cuisineService.save(cuisine);
    }

    @PutMapping("/{id}")
    public Cuisine update(@RequestBody Cuisine cuisine, @PathVariable("id") Integer id) {
        cuisine.setId(id);
        return cuisineService.update(cuisine);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        cuisineService.softDelete(id);
    }
}
