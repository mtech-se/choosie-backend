package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.service.baseservice.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/locations")
public class LocationController {
    private final LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping
    public Page<Location> index(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "country_id", required = false) Integer countryId,
            Pageable pageable
    ) {
        if (name != null && countryId != null) return locationService.getAll(name, countryId, pageable);
        if (name != null) return locationService.getAll(name, pageable);
        return locationService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Location show(@PathVariable("id") Integer id) {
        return locationService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Location create(@RequestBody Location location) {
        return locationService.save(location);
    }

    @PutMapping("/{id}")
    public Location update(@RequestBody Location location, @PathVariable("id") Integer id) {
        location.setId(id);
        return locationService.update(location);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        locationService.softDelete(id);
    }
}
