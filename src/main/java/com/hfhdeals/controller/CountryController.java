package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.service.baseservice.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/countries")
public class CountryController {
    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    public Page<Country> index(@RequestParam(value = "name", required = false) String name, Pageable pageable) {
        if (name != null) return countryService.getAll(name, pageable);
        return countryService.getAll(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Country create(@RequestBody Country country) {
        return countryService.save(country);
    }

    @GetMapping("/{id}")
    public Country show(@PathVariable("id") Integer id) {
        return countryService.getById(id);
    }

    @PutMapping("/{id}")
    public Country update(@RequestBody Country country, @PathVariable("id") Integer id) {
        country.setId(id);
        return countryService.update(country);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        countryService.softDelete(id);
    }

}
