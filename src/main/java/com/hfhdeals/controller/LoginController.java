/*package com.hfhdeals.controller;

import com.hfhdeals.model.pojos.Login;
import com.hfhdeals.service.restservice.LoginRESTService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    LoginRESTService loginRESTService;

    @RequestMapping(name = "/", method = RequestMethod.POST, produces = "json/application")
    @ResponseBody
    public ResponseEntity verify(@RequestBody String customer) throws JSONException {
        Login u = Login.fromJson(new JSONObject(customer));
        int result = loginRESTService.verifyCredentials(u);
        JSONObject json = new JSONObject();
        if (result == 1) {
            json.put("status_message", "Success");
            return new ResponseEntity<>(json.toString(), HttpStatus.OK);
        }
        if (result == 0) {
            json.put("status_message", "Failed");
            return new ResponseEntity<>(json.toString(), HttpStatus.FORBIDDEN);
        } else
            json.put("status_message", "Customer not found");
        return new ResponseEntity<>(json.toString(), HttpStatus.BAD_REQUEST);
    }
}
*/