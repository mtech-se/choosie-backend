//package com.hfhdeals.controller;
//
//import com.hfhdeals.model.jpaentities.Deal;
//import com.hfhdeals.service.DealOfTheMonthService;
//import org.json.JSONException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/deals-of-the-month")
//public class DealOfTheMonthController {
//    private final DealOfTheMonthService dealOfTheMonthService;
//
//    @Autowired
//    public DealOfTheMonthController(DealOfTheMonthService dealOfTheMonthService) {
//        this.dealOfTheMonthService = dealOfTheMonthService;
//    }
//
//    @GetMapping
//    public Page<Deal> index(Pageable pageable) {
//        return dealOfTheMonthService.getAll(pageable);
//    }
//
//    @PutMapping(value = "/{id}")
//    public Deal set(@PathVariable("id") Integer id) throws JSONException {
//        return dealOfTheMonthService.set(id);
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public Deal unset(@PathVariable("id") Integer id) {
//        return dealOfTheMonthService.unset(id);
//    }
//}
