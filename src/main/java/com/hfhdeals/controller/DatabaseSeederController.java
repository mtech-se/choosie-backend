package com.hfhdeals.controller;

import com.hfhdeals.database.seeder.DatabaseSeeder;
import com.hfhdeals.service.TruncateDatabaseService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/database")
public class DatabaseSeederController {
    private final DatabaseSeeder databaseSeeder;
    private final TruncateDatabaseService truncateDatabaseService;

    @Autowired
    public DatabaseSeederController(DatabaseSeeder databaseSeeder, TruncateDatabaseService truncateDatabaseService) {
        this.databaseSeeder = databaseSeeder;
        this.truncateDatabaseService = truncateDatabaseService;
    }

    @GetMapping(value = "/seed", produces = "application/json")
    public String seed() {
        databaseSeeder.run();
        String seeded = databaseSeeder.getSeeded();
        String notSeeded = databaseSeeder.getNotSeeded();

        System.out.println("Not Seeded =" + notSeeded);
        System.out.println("Seeded = " + seeded);

        JSONObject json = new JSONObject();
        json.put("seeded", seeded);
        json.put("not_seeded", notSeeded);

        return json.toString() + "";
    }

    @GetMapping(value = "/truncate")
    public List<String> truncate() {
        return truncateDatabaseService.truncate();
    }
}