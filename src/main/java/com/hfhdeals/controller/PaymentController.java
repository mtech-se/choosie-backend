//package com.hfhdeals.controller;
//
//import com.hfhdeals.model.jpaentities.Payment;
//import com.hfhdeals.service.restservice.PaymentRESTService;
//import com.stripe.model.Charge;
//import com.stripe.model.Token;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//@Controller
//@RequestMapping("/{id}/payment")
//public class PaymentController {
//
//    @Autowired
//    PaymentRESTService service;
//
//    @RequestMapping(name = "/", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//    public ResponseEntity pay(@RequestBody String details, @PathVariable("id") Integer id) throws JSONException {
//
//        JSONObject json = new JSONObject(details);
//        Payment payment = Payment.fromJson(json);
//        Token token = service.getToken(payment.getCreditcard_details());
//        Charge pay = service.Pay(token, payment.getChargerequest_details());
//        int checkoutid = service.getLastCheckoutId(payment.getOrder_details().getUser_id());
//        payment.getOrder_details().setTransaction_id(pay.getId());
//        payment.getOrder_details().setTotal(pay.getAmount());
//        payment.getOrder_details().setDeal_id(id);
//        payment.getOrder_details().setCheckout_id(checkoutid);
//        service.save(payment.getOrder_details());
//        return new ResponseEntity<>(pay.toJson(), HttpStatus.OK);
//    }
//
//}
