package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryPhoto;
import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.CategoryPhotoService;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.service.baseservice.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryService categoryService;
    private final CategoryPhotoService categoryPhotoService;
    private final PhotoService photoService;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryPhotoService categoryPhotoService, PhotoService photoService) {
        this.categoryService = categoryService;
        this.categoryPhotoService = categoryPhotoService;
        this.photoService = photoService;
    }

    @GetMapping
    public Page<Category> index(@RequestParam(value = "name", required = false) String name, Pageable pageable) {
        if (name != null) return categoryService.getAll(name, pageable);
        return categoryService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Category show(@PathVariable("id") Integer id) {
        return categoryService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Category create(@RequestBody Category category) {
        return categoryService.save(category);
    }

    @PostMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.CREATED)
    public Page<CategoryPhoto> createCategoryPhoto(@RequestParam(value = "photos") MultipartFile[] photos, @PathVariable("id") Integer categoryId, Pageable pageable) throws Exception {
        Category category = categoryService.getById(categoryId);
        Page<CategoryPhoto> currentPhotos = categoryPhotoService.getCategoryPhotosByCategoryAndDeletedAtIsNull(category, pageable);
        if (photos.length + currentPhotos.getTotalElements() <= 1) {
            Photo photo = new Photo();
            photo.setPath("/categorys/" + category.getId() + "/img.png");
            photoService.savePhotoAndFile(photo, photos[0]);
            category.addPhoto(photo);
            categoryService.update(category);
        } else {
            throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Too Many Photos. Current Count = " + currentPhotos.getTotalElements());
        }
        return categoryPhotoService.getCategoryPhotosByCategoryAndDeletedAtIsNull(category, pageable);
    }

    @PutMapping("/{id}")
    public Category update(@RequestBody Category category, @PathVariable("id") Integer id) {
        category.setId(id);
        return categoryService.update(category);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        categoryService.softDelete(id);
    }
}
