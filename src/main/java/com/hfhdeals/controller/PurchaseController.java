//package com.hfhdeals.controller;
//
//import com.hfhdeals.model.jpaentities.Purchase;
//import com.hfhdeals.service.baseservice.PurchaseService;
//import org.json.JSONException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/purchases")
//public class PurchaseController {
//    private final PurchaseService purchaseService;
//
//    @Autowired
//    public PurchaseController(PurchaseService purchaseService) {
//        this.purchaseService = purchaseService;
//    }
//
//    @GetMapping
//    public Page<Purchase> index(Pageable pageable) throws JSONException {
//        return purchaseService.getAll(pageable);
//    }
//
//    @GetMapping("/{id}")
//    public Purchase show(@PathVariable("id") Integer id) {
//        return purchaseService.getById(id);
//    }
//
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public Purchase create(@RequestBody Purchase purchase) {
//        return purchaseService.save(purchase);
//    }
//
//    @PutMapping("/{id}")
//    public Purchase update(@RequestBody Purchase purchase, @PathVariable("id") Integer id) {
//        purchase.setId(id);
//        return purchaseService.update(purchase);
//    }
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void delete(@PathVariable("id") Integer id) {
//        purchaseService.delete(id);
//    }
//}
