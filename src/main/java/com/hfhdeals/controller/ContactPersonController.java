package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.service.baseservice.ContactPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contact-persons")
public class ContactPersonController {
    private final ContactPersonService contactPersonService;

    @Autowired
    public ContactPersonController(ContactPersonService contactPersonService) {
        this.contactPersonService = contactPersonService;
    }

    @GetMapping
    public Page<ContactPerson> index(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "merchant_id", required = false) Integer merchantId,
            Pageable pageable
    ) {
        if (name != null && merchantId != null) return contactPersonService.getAll(name, merchantId, pageable);
        else if (name != null) return contactPersonService.getAll(name, pageable);
        else if (merchantId != null) return contactPersonService.getAll(merchantId, pageable);
        return contactPersonService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public ContactPerson show(@PathVariable("id") Integer id) {
        return contactPersonService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContactPerson create(@RequestBody ContactPerson contactPerson) {
        return contactPersonService.save(contactPerson);
    }

    @PutMapping("/{id}")
    public ContactPerson update(@RequestBody ContactPerson contactPerson, @PathVariable("id") Integer id) {
        contactPerson.setId(id);
        return contactPersonService.update(contactPerson);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        contactPersonService.softDelete(id);
    }
}
