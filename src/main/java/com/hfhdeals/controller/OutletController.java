package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.requestbody.OutletRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.OutletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/outlets")
public class OutletController {
    private final OutletService outletService;
    private final BrandService brandService;

    @Autowired
    public OutletController(OutletService outletService, BrandService brandService) {
        this.outletService = outletService;
        this.brandService = brandService;
    }

    @GetMapping
    public Page<Outlet> index(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "brand_id", required = false) Integer brandId,
            Pageable pageable
    ) {
        if (name != null && brandId != null) return outletService.getAll(name, brandId, pageable);
        if (name != null) return outletService.getAll(name, pageable);
        if (brandId != null) return outletService.getAll(brandId, pageable);
        return outletService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Outlet show(@PathVariable("id") Integer id) {
        return outletService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Outlet create(@Validated(Create.class) @RequestBody OutletRequestBody requestBody) {
        Brand brand = brandService.getById(requestBody.getBrand().getId());

        Outlet outlet = new Outlet();
        outlet.setName(requestBody.getName());
        outlet.setAddress(requestBody.getAddress());

        brand.addOutlet(outlet);

        return outletService.save(outlet);
    }

    @PutMapping("/{id}")
    public Outlet update(@Validated(Update.class) @RequestBody OutletRequestBody requestBody, @PathVariable("id") Integer id) {
        Outlet outlet = outletService.getById(id);

        if (requestBody.getName() != null) outlet.setName(requestBody.getName());
        if (requestBody.getAddress() != null) outlet.setAddress(requestBody.getAddress());

        return outletService.update(outlet);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        outletService.softDelete(id);
    }
}
