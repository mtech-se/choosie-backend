package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.*;
import com.hfhdeals.model.pojos.JsonReturnObject;
import com.hfhdeals.requestbody.DealRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.*;
import com.hfhdeals.service.restservice.SRERESTService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/deals")
public class DealController {
    private final DealService dealService;
    private final DealPhotoService dealPhotoService;
    private final PhotoService photoService;
    private final SRERESTService sreRESTService;
    private final CategoryService categoryService;
    private final CuisineService cuisineService;
    private CategoryDealService categoryDealService;
    private CuisineDealService cuisineDealService;

    @Autowired
    public DealController(DealService dealService, DealPhotoService dealPhotoService, PhotoService photoService, SRERESTService sreRESTService, CategoryService categoryService, CuisineService cuisineService, CategoryDealService categoryDealService, CuisineDealService cuisineDealService) {
        this.dealService = dealService;
        this.dealPhotoService = dealPhotoService;
        this.photoService = photoService;
        this.sreRESTService = sreRESTService;
        this.categoryService = categoryService;
        this.cuisineService = cuisineService;
        this.categoryDealService = categoryDealService;
        this.cuisineDealService = cuisineDealService;
    }

    @GetMapping
    public Page<Deal> index(
            @RequestParam(value = "dotm", required = false) Boolean onlyDealsOfTheMonth,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "category_id", required = false) Integer categoryId,
            @RequestParam(value = "price_min", required = false) Integer priceMin,
            @RequestParam(value = "price_max", required = false) Integer priceMax,
            @RequestParam(value = "start_date", required = false) Date startDate,
            @RequestParam(value = "end_date", required = false) Date endDate,
            Pageable pageable
    ) {
        // TODO: when start_date is given but end_date is not, throw error; vice-versa
        // TODO: when page is given but size is not given, throw error; vice-versa
        // TODO: when price_min is given but price_max is not given, throw error; vice-versa

        if (onlyDealsOfTheMonth != null && onlyDealsOfTheMonth) {
            return dealService.getDotmDeals(pageable);
        }
        if (startDate != null && endDate != null && title != null && priceMin != null && priceMax != null) {
            return dealService.getByDateRangeTitleAndPriceRange(startDate, endDate, title, priceMin, priceMax, pageable);
        }
        if (startDate != null && endDate != null && priceMin != null && priceMax != null) {
            return dealService.getByDateRangeAndPriceRange(startDate, endDate, priceMin, priceMax, pageable);
        }
        if (startDate != null && endDate != null && title != null) {
            return dealService.getByDateRangeAndTitle(startDate, endDate, title, pageable);
        }
        if (startDate != null && endDate != null) {
            return dealService.getByDateRange(startDate, endDate, pageable);
        }

        if (title != null && categoryId != null && priceMin != null && priceMax != null) {
            return dealService.getDeals(title, categoryId, priceMin, priceMax, pageable);
        }
        if (title != null && categoryId != null) {
            return dealService.getDeals(title, categoryId, pageable);
        }
        if (title != null && priceMin != null && priceMax != null) {
            return dealService.getByTitleAndPriceRange(title, priceMin, priceMax, pageable);
        }
        if (priceMin != null && priceMax != null) {
            return dealService.getByPriceRange(priceMin, priceMax, pageable);
        }
        if (title != null) {
            return dealService.getByTitle(title, pageable);
        }
        if (categoryId != null) {
            return dealService.getByCategoryId(categoryId, pageable);
        }

        return dealService.getAll(pageable);
    }

    @GetMapping(value = "/{id}")
    public Deal show(@PathVariable("id") Integer id) {
        return dealService.getById(id);
    }

    @PutMapping(value = "/{id}")
    @Transactional
    public Deal update(@Validated(Update.class) @RequestBody DealRequestBody requestBody, @PathVariable("id") Integer id) {
        Deal deal = dealService.getById(id);

        if (requestBody.getTitle() != null) deal.setTitle(requestBody.getTitle());
        if (requestBody.getDescription() != null) deal.setDescription(requestBody.getDescription());
        if (requestBody.getOriginalPrice() != null) deal.setOriginalPrice(requestBody.getOriginalPrice());
        if (requestBody.getDiscountedPrice() != null) deal.setDiscountedPrice(requestBody.getDiscountedPrice());
        if (requestBody.getTermsOfUse() != null) deal.setTermsOfUse(requestBody.getTermsOfUse());
        if (requestBody.getRedemptionInstructions() != null) deal.setRedemptionInstructions(requestBody.getRedemptionInstructions());
        if (requestBody.getMaxQuantity() != null) deal.setMaxQuantity(requestBody.getMaxQuantity());
        if (requestBody.getDealOfTheMonth() != null) deal.setDealOfTheMonth(requestBody.getDealOfTheMonth());
        if (requestBody.getAutoPublishDate() != null) deal.setAutoPublishDate(requestBody.getAutoPublishDate());
        if (requestBody.getPublishedAt() != null) deal.setPublishedAt(requestBody.getPublishedAt());
        if (requestBody.getRedemptionExpiryDate() != null) deal.setRedemptionExpiryDate(requestBody.getRedemptionExpiryDate());
        if (requestBody.getArchivedAt() != null) deal.setArchivedAt(requestBody.getArchivedAt());

        // if requestBody supplies non-null for categoryDeals,
        // we delete all existing categoryDeals to this deal's name
        // and add the requestBody's categoryDeals
        // i.e. total replacement
        if (requestBody.getCategoryDeals() != null) {
            ArrayList<CategoryDeal> categoryDeals = new ArrayList<>(deal.getCategoryDeals());
            categoryDeals.forEach(categoryDeal -> categoryDealService.delete(categoryDeal));

            for (CategoryDeal categoryDeal : requestBody.getCategoryDeals()) {
                Category category = categoryService.getById(categoryDeal.getCategory().getId());
                category.addDeal(deal);

                categoryService.update(category);
            }
        }

        // if requestBody supplies non-null for cuisineDeals,
        // we delete all existing cuisineDeals to this deal's name
        // and add the requestBody's cuisineDeals
        // i.e. total replacement
        if (requestBody.getCuisineDeals() != null) {
            ArrayList<CuisineDeal> cuisineDeals = new ArrayList<>(deal.getCuisineDeals());
            cuisineDeals.forEach(cuisineDeal -> cuisineDealService.delete(cuisineDeal));

            for (CuisineDeal cuisineDeal : requestBody.getCuisineDeals()) {
                Cuisine cuisine = cuisineService.getById(cuisineDeal.getCuisine().getId());
                cuisine.addDeal(deal);

                cuisineService.update(cuisine);
            }
        }

        return dealService.update(deal);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Deal create(@Validated(Create.class) @RequestBody DealRequestBody requestBody) {
        Outlet outlet = new Outlet();
        outlet.setId(requestBody.getOutlet().getId());

        Deal deal = new Deal();
        deal.setTitle(requestBody.getTitle());
        deal.setDescription(requestBody.getDescription());
        deal.setOriginalPrice(requestBody.getOriginalPrice());
        deal.setDiscountedPrice(requestBody.getDiscountedPrice());
        deal.setTermsOfUse(requestBody.getTermsOfUse());
        deal.setRedemptionInstructions(requestBody.getRedemptionInstructions());
        deal.setMaxQuantity(requestBody.getMaxQuantity());
        deal.setOutlet(outlet);
        deal.setDealOfTheMonth(requestBody.getDealOfTheMonth());
        deal.setAutoPublishDate(requestBody.getAutoPublishDate());
        deal.setRedemptionExpiryDate(requestBody.getRedemptionExpiryDate());
        sreRESTService.postDeals();
        return dealService.save(deal);
    }

    @PostMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.CREATED)
    public Page<DealPhoto> createDealPhoto(@RequestParam(value = "photos") MultipartFile[] photos, @PathVariable("id") Integer dealId, Pageable pageable) throws Exception {
        Deal deal = dealService.getById(dealId);
        Page<DealPhoto> currentPhotos = dealPhotoService.getDealPhotosByDealAndDeletedAtIsNull(deal, pageable);
        if (photos.length + currentPhotos.getTotalElements() <= 5) {
            for (int i = (int) currentPhotos.getTotalElements() + 1; i <= currentPhotos.getTotalElements() + photos.length; i++) {
                Photo photo = new Photo();
                photo.setPath("/deals/" + deal.getId() + "/" + i + ".png");
                photoService.savePhotoAndFile(photo, photos[0]);
                deal.addPhoto(photo);
                dealService.update(deal);
            }
        } else {
            throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Too Many Photos. Current Count = " + currentPhotos.getTotalElements());
        }
        return dealPhotoService.getDealPhotosByDealAndDeletedAtIsNull(deal, pageable);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        dealService.softDelete(id);
    }

    @GetMapping(value = "/{id}/recommended", produces = "application/json")
    public ResponseEntity<String> getRecommendedDeals(@PathVariable("id") Integer id) {
        ResponseEntity<String> responseEntity;
        JsonReturnObject jsonReturnObject = new JsonReturnObject();

        if (id == null) {
            responseEntity = new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            return responseEntity;
        }
        Deal deal = dealService.getById(id);
        if (deal == null) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
        try {
            JSONArray deals = sreRESTService.getRecommendedDeals(id);
            if (deals == null) {
                responseEntity = new ResponseEntity<>(jsonReturnObject.toJson().toString(), HttpStatus.NOT_FOUND);
            } else {
                jsonReturnObject.setContent(deals);
                responseEntity = new ResponseEntity<>(jsonReturnObject.toJson().toString(), HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject responseContent = new JSONObject();
            JSONArray allErrors = new JSONArray();
            allErrors.put(e.getMessage());
            responseContent.put("errors", allErrors);
            responseEntity = new ResponseEntity<>(responseContent.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;

    }
}
