package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.DealCustomerClick;
import com.hfhdeals.service.baseservice.DealCustomerClickService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deal-customer-clicks")
public class DealCustomerClickController {
    private final DealCustomerClickService dealCustomerClickService;

    @Autowired
    public DealCustomerClickController(DealCustomerClickService dealCustomerClickService) {
        this.dealCustomerClickService = dealCustomerClickService;
    }

    @PostMapping
    public DealCustomerClick clickOnce(@RequestBody DealCustomerClick dealCustomerClick) {
        return dealCustomerClickService.save(dealCustomerClick);
    }
}
