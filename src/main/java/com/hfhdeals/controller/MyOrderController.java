package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.model.jpaentities.OrderItem;
import com.hfhdeals.model.jpaentities.Payment;
import com.hfhdeals.requestbody.PaymentRequestBody;
import com.hfhdeals.service.baseservice.*;
import com.hfhdeals.service.restservice.SRERESTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/my-orders")
public class MyOrderController {
    private final OrderService orderService;
    private final OrderItemService orderItemService;
    private final CustomerService customerService;
    private final DealService dealService;
    private final SRERESTService sreRESTService;
    private final PaymentService paymentService;

    @Autowired
    public MyOrderController(
            OrderService orderService, CustomerService customerService, OrderItemService orderItemService, DealService dealService, PaymentService paymentService, SRERESTService sreRESTService
    ) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.orderItemService = orderItemService;
        this.dealService = dealService;
        this.sreRESTService = sreRESTService;
        this.paymentService = paymentService;
    }

    @GetMapping
    public Page<Order> index(
            @RequestParam(value = "current", required = false) Boolean current,
            HttpServletRequest req,
            Pageable pageable
    ) {
        if (current != null) return orderService.getCurrentCart(customerService.getByEmail(req.getAttribute("email") + ""), pageable);
        return orderService.getAllByCustomer(customerService.getByEmail(req.getAttribute("email") + ""), pageable);
    }

    @GetMapping("/{id}")
    public Order show(@PathVariable("id") Integer id) {
        return orderService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order create(HttpServletRequest req, Pageable pageable) throws HttpClientErrorException {
        Order order = new Order();
        Customer customer = customerService.getByEmail(req.getAttribute("email") + "");
        if (orderService.getCurrentCart(customer, pageable).hasContent())
            throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Unpaid order exists");
        order.setCustomer(customer);
        sreRESTService.postRatings();
        return orderService.save(order);
    }

    @PutMapping("/{id}")
    public Order update(@RequestBody Order orderIncoming, @PathVariable("id") Integer id, HttpServletRequest req) {
        Order order = orderService.getById(id);
        if (order.getPayments().size() > 0) {
            throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "Order Already Paid up.");
        }
        for (OrderItem orderItem : new ArrayList<OrderItem>(order.getOrderItems())) {
            order.removeOrderItem(orderItem);
        }
        for (OrderItem orderItem : orderIncoming.getOrderItems()) {
            orderItem.setSpotPrice(dealService.getById(orderItem.getDeal().getId()).getDiscountedPrice());
            order.addOrderItem(orderItem);
        }
        return orderService.update(order);
    }

    @PutMapping("/redeem-item/{id}")
    public OrderItem redeem(@PathVariable("id") Integer orderItemId, HttpServletRequest req) {
        OrderItem orderItem = orderItemService.getById(orderItemId);
        orderItem.setRedeemedAt(new Date(System.currentTimeMillis()));
        return orderItemService.update(orderItem);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        orderService.softDelete(id);
    }

    @PostMapping("/{id}/payments")
    @ResponseStatus(HttpStatus.CREATED)
    public Payment pay(HttpServletRequest req, @PathVariable("id") Integer id, @RequestBody PaymentRequestBody paymentRequestBody) {
        Order order = orderService.getById(id);
        return paymentService.payWithStripe(order, paymentRequestBody);
    }
}
