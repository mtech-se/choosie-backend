package com.hfhdeals.controller;

import com.hfhdeals.constant.Constants;
import com.hfhdeals.exception.InvalidDateRange;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.pojos.CategoryDTO;
import com.hfhdeals.model.pojos.JsonReturnObject;
import com.hfhdeals.model.pojos.MerchantDTO;
import com.hfhdeals.service.ReportService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController {
    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @RequestMapping(path = "/overview-stats", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> getOverviewStats(
            @RequestParam("startDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date endDate
    ) throws InvalidDateRange {
        if (startDate.after(endDate)) throw new InvalidDateRange("start_date cannot be after end_date");

//        System.err.println(startDate);
//        System.err.println(endDate);

        ResponseEntity<String> responseEntity;
        JsonReturnObject jsonReturnObject = new JsonReturnObject();

        int totalPublishedDeals = reportService.getTotalPublishedDeals(startDate, endDate);
        int totalPurchasedDeals = reportService.getTotalPurchasedDeals(startDate, endDate);
        int totalNewCustomers = reportService.getTotalNewCustomers(startDate, endDate);

        int totalSales;
        try {
            totalSales = reportService.getTotalSales(startDate, endDate);   // for some weird reason, this line throws
        } catch (NullPointerException e) {
            totalSales = 0;
        }

        int totalPayouts = (int) (0.7 * totalSales);
        int totalCommissions = (int) (0.3 * totalSales);

        JSONObject data = new JSONObject();
        data.put("total_published_deals", totalPublishedDeals);
        data.put("total_purchased_deals", totalPurchasedDeals);
        data.put("total_new_customers", totalNewCustomers);
        data.put("total_sales", totalSales);
        data.put("total_payouts", totalPayouts);
        data.put("total_commissions", totalCommissions);
        jsonReturnObject.setContent(data);

        responseEntity = new ResponseEntity<>(jsonReturnObject.toJson().toString(), HttpStatus.OK);

        return responseEntity;
    }

    @RequestMapping(path = "/top-deals-by-purchase-count")
    public Page<Deal> getTopDealsByPurchaseCount(
            @RequestParam("startDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date endDate
    ) throws InvalidDateRange {
        if (startDate.after(endDate)) throw new InvalidDateRange("start_date cannot be after end_date");

        System.err.println(startDate);
        System.err.println(endDate);

        return reportService.getTopDealsByPurchaseCount(startDate, endDate);
    }

    @RequestMapping(path = "/top-categories-by-published-deal-count")
    public List<CategoryDTO> getTopCategoriesByPublishedDealCount(
            @RequestParam("startDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date endDate
    ) throws InvalidDateRange {
        if (startDate.after(endDate)) throw new InvalidDateRange("start_date cannot be after end_date");

        return reportService.getTopCategoriesByPublishedDealCount(startDate, endDate);
    }

    @GetMapping(value = "/top-merchants-by-purchase-redemption-count", produces = "application/json")
    public List<MerchantDTO> getTopMerchantsByPurchaseRedemptionCount(
            @RequestParam("startDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = Constants.ISO_FORMAT) Date endDate
    ) throws InvalidDateRange {
        if (startDate.after(endDate)) throw new InvalidDateRange("start_date cannot be after end_date");

        return reportService.getTopMerchantsByPurchaseRedemptionCount(startDate, endDate);
    }
}
