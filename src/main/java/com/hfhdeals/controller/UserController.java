package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.User;
import com.hfhdeals.service.baseservice.AdminService;
import com.hfhdeals.service.baseservice.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/users")
public class UserController {
    private final AdminService adminService;
    private CustomerService customerService;

    @Autowired
    public UserController(AdminService adminService, CustomerService customerService) {
        this.adminService = adminService;
        this.customerService = customerService;
    }

    @GetMapping
    public User show(HttpServletRequest req) {
        String email = req.getAttribute("email") + "";
        User user = req.getAttribute("roles").equals("ADMIN") ? adminService.getByEmail(email) : customerService.getByEmail(email);
        if (user == null) throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY, "No such user found");
        return user;
    }

}
