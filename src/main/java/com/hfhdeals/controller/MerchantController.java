package com.hfhdeals.controller;

import com.hfhdeals.exception.MoreThanOneContactPersonFound;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.requestbody.MerchantRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.MerchantService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/merchants")
public class MerchantController {
    private final MerchantService merchantService;

    @Autowired
    public MerchantController(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @GetMapping
    public Page<Merchant> index(@RequestParam(value = "name", required = false) String name, Pageable pageable) throws JSONException {
        if (name != null) return merchantService.getAll(name, pageable);
        return merchantService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Merchant show(@PathVariable("id") Integer id, HttpServletRequest req) {
        req.getAttribute("roles");
        req.getAttribute("email");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getPrincipal();
        return merchantService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Merchant create(@RequestBody @Validated(Create.class) MerchantRequestBody requestBody) {
        Merchant merchant = new Merchant();
        merchant.setName(requestBody.getName());
        merchant.setBusinessRegistrationNumber(requestBody.getBusinessRegistrationNumber());
        merchant.setMailingAddress(requestBody.getMailingAddress());

        if (requestBody.getContactPersons().size() > 0) {
            ContactPerson contactPerson = new ContactPerson();
            contactPerson.setName(requestBody.getContactPersons().get(0).getName());
            contactPerson.setPosition(requestBody.getContactPersons().get(0).getPosition());
            contactPerson.setMobileNumber(requestBody.getContactPersons().get(0).getMobileNumber());
            contactPerson.setEmail(requestBody.getContactPersons().get(0).getEmail());
            merchant.addContactPerson(contactPerson);
        }

        return merchantService.createMerchantWithContactPersonsIfPresent(merchant);
    }

    @PutMapping("/{id}")
    public Merchant update(@RequestBody @Validated(Update.class) MerchantRequestBody requestBody, @PathVariable("id") Integer id) {
        Merchant merchant = merchantService.getById(id);
        if (requestBody.getName() != null) merchant.setName(requestBody.getName());
        if (requestBody.getBusinessRegistrationNumber() != null) merchant.setBusinessRegistrationNumber(requestBody.getBusinessRegistrationNumber());
        if (requestBody.getMailingAddress() != null) merchant.setMailingAddress(requestBody.getMailingAddress());

        if (requestBody.getContactPersons().size() > 0) {
            if (requestBody.getContactPersons().size() != 1) throw new MoreThanOneContactPersonFound();
            MerchantRequestBody.ContactPerson incomingContactPerson = requestBody.getContactPersons().get(0);
            ContactPerson existingContactPerson = merchant.getContactPersons().get(0);

            if (incomingContactPerson.getName() != null) existingContactPerson.setName(incomingContactPerson.getName());
            if (incomingContactPerson.getPosition() != null) existingContactPerson.setPosition(incomingContactPerson.getPosition());
            if (incomingContactPerson.getMobileNumber() != null) existingContactPerson.setMobileNumber(incomingContactPerson.getMobileNumber());
            if (incomingContactPerson.getEmail() != null) existingContactPerson.setEmail(incomingContactPerson.getEmail());
        }

        return merchantService.update(merchant);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        merchantService.softDelete(id);
    }
}
