package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.PhotoService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/photos")
public class PhotoController {
    private final PhotoService photoService;

    @Autowired
    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping("/{id}")
    public ResponseEntity show(@PathVariable("id") Integer id) throws Exception {
        Photo photo = photoService.getById(id);
        HttpHeaders httpHeaders = new HttpHeaders();
        if (photo != null) {
            httpHeaders.add("content-type", "image/png");
            return new ResponseEntity<>(photoService.getPhotoFile(photo), httpHeaders, HttpStatus.OK);
        }
        throw new NotFoundException("Photo Not Found");
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        photoService.softDelete(id);
    }
}
