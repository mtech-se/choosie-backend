//package com.hfhdeals.controller;
//
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.service.restservice.PaymentRESTService;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
//@Controller
//@RequestMapping("/checkout")
//public class CheckoutController {
//
//    @Autowired
//    PaymentRESTService service;
//
//    @RequestMapping(name = "/", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//    public ResponseEntity savecheckoutdetails(@RequestBody String details, HttpServletRequest req) throws IOException, JSONException {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        CheckoutDetails readValue = mapper.readValue(details, CheckoutDetails.class);
//        service.saveCheckout(readValue);
//        service.getLastCheckoutId();
//        // req.setAttribute("checkout_id", service.getLastOrderId(););
//        JSONObject json = new JSONObject();
//        json.put("status_message", "success");
//        return new ResponseEntity<>(json.toString(), HttpStatus.OK);
//    }
//
//}
