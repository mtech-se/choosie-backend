package com.hfhdeals.controller;

import com.hfhdeals.model.jpaentities.Admin;
import com.hfhdeals.requestbody.AdminRequestBody;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import com.hfhdeals.service.baseservice.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.hfhdeals.constant.SecurityConstants.ADMIN_ROLE;

@RestController
@RequestMapping("/admins")
public class AdminController {
    private final AdminService adminService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public AdminController(AdminService adminService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.adminService = adminService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping
    @PreAuthorize(ADMIN_ROLE)
    public Page<Admin> index(
            @RequestParam(value = "name", required = false) String name,
            Pageable pageable
    ) {
        if (name != null) return adminService.getAll(name, pageable);
        return adminService.getAll(pageable);
    }

    @GetMapping("/{id}")
    @PreAuthorize(ADMIN_ROLE)
    public Admin show(@PathVariable("id") Integer id) {
        return adminService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize(ADMIN_ROLE)
    public Admin create(@Validated(Create.class) @RequestBody AdminRequestBody requestBody) {
        Admin admin = new Admin();

        admin.setEmail(requestBody.getEmail());
        admin.setFirstName(requestBody.getFirstName());
        admin.setLastName(requestBody.getLastName());
        admin.setGender(requestBody.getGender());
        admin.setPassword(bCryptPasswordEncoder.encode(requestBody.getPassword()));

        return adminService.save(admin);
    }

    @PutMapping("/{id}")
    @PreAuthorize(ADMIN_ROLE)
    public Admin update(@Validated(Update.class) @RequestBody AdminRequestBody requestBody, @PathVariable("id") Integer id) {
        Admin admin = adminService.getById(id);

        if (requestBody.getEmail() != null) admin.setEmail(requestBody.getEmail());
        if (requestBody.getFirstName() != null) admin.setFirstName(requestBody.getFirstName());
        if (requestBody.getLastName() != null) admin.setLastName(requestBody.getLastName());
        if (requestBody.getGender() != null) admin.setGender(requestBody.getGender());

        return adminService.update(admin);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize(ADMIN_ROLE)
    public void delete(@PathVariable("id") Integer id) {
        adminService.softDelete(id);
    }
}
