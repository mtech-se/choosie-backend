package com.hfhdeals.exception;

public class InvalidDateRange extends Exception {
    public InvalidDateRange(String message) {
        super(message);
    }
}
