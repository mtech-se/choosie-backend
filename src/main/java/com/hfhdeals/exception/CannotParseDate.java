package com.hfhdeals.exception;

public class CannotParseDate extends RuntimeException {
    public CannotParseDate(String s) {
        super(s);
    }
}
