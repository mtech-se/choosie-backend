package com.hfhdeals.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class InvalidRequestParams extends Exception {
    private List<String> invalidRequestParams;
}
