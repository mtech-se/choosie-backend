package com.hfhdeals;

import com.hfhdeals.exception.InvalidDateRange;
import com.hfhdeals.exception.InvalidRequestParams;
import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.model.pojos.ApiError;
import javassist.tools.web.BadHttpRequest;
import org.json.JSONException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ApiError> handleException(Exception exception, WebRequest request) {
        exception.printStackTrace();

        HttpHeaders httpHeaders = new HttpHeaders();

        if (exception instanceof HttpRequestMethodNotSupportedException)
            return handle(
                    (HttpRequestMethodNotSupportedException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof InvalidDataAccessApiUsageException)
            return handle(
                    (InvalidDataAccessApiUsageException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof DataIntegrityViolationException)
            return handle(
                    (DataIntegrityViolationException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof InvalidRequestParams)
            return handle(
                    (InvalidRequestParams) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof ConstraintViolationException)
            return handle(
                    (ConstraintViolationException) exception,
                    httpHeaders,
                    HttpStatus.NOT_FOUND,
                    request
            );

        if (exception instanceof BindException)
            return handle(
                    (BindException) exception,
                    httpHeaders,
                    HttpStatus.NOT_FOUND,
                    request
            );

        if (exception instanceof MissingServletRequestParameterException)
            return handle(
                    (MissingServletRequestParameterException) exception,
                    httpHeaders,
                    HttpStatus.NOT_FOUND,
                    request
            );

        if (exception instanceof RecordNotFound)
            return handle(
                    (RecordNotFound) exception,
                    httpHeaders,
                    HttpStatus.NOT_FOUND,
                    request
            );

        if (exception instanceof MethodArgumentTypeMismatchException)
            return handle(
                    (MethodArgumentTypeMismatchException) exception,
                    httpHeaders,
                    HttpStatus.NOT_FOUND,
                    request
            );

        if (exception instanceof HttpMessageNotReadableException)
            return handle(
                    (HttpMessageNotReadableException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof MethodArgumentNotValidException)
            return handle(
                    (MethodArgumentNotValidException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof JSONException)
            return handle(
                    (JSONException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof BadHttpRequest)
            return handle(
                    (BadHttpRequest) exception,
                    httpHeaders,
                    HttpStatus.BAD_REQUEST,
                    request
            );

        if (exception instanceof AccessDeniedException)
            return handle(
                    (AccessDeniedException) exception,
                    httpHeaders,
                    HttpStatus.FORBIDDEN,
                    request
            );

        if (exception instanceof InvalidDateRange)
            return handle(
                    (InvalidDateRange) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        if (exception instanceof HttpClientErrorException)
            return handle(
                    (HttpClientErrorException) exception,
                    httpHeaders,
                    HttpStatus.UNPROCESSABLE_ENTITY,
                    request
            );

        exception.printStackTrace();

        return finalHandler(
                exception,
                new ApiError("Something went wrong at the server"),
                httpHeaders,
                HttpStatus.INTERNAL_SERVER_ERROR,
                request
        );
    }

    private ResponseEntity<ApiError> handle(HttpRequestMethodNotSupportedException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        String message = exception.getMethod() + " is not supported. The following are supported: " + String.join(", ", exception.getSupportedMethods());
        return finalHandler(exception, new ApiError(message), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(InvalidDateRange exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.getMessage()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(InvalidDataAccessApiUsageException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.getMessage()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(InvalidRequestParams exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        String message = "Invalid query params: " + exception.getInvalidRequestParams();
        return finalHandler(exception, new ApiError(message), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(DataIntegrityViolationException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        String rootCauseMessage = exception.getRootCause().getMessage();
        return finalHandler(exception, new ApiError(rootCauseMessage), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(ConstraintViolationException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        List<String> messages = new ArrayList<>();

        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            messages.add(constraintViolation.getPropertyPath().toString() + " " + constraintViolation.getMessage());
        }

        return finalHandler(exception, new ApiError(messages), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(BindException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        List<ObjectError> errors = exception.getAllErrors();
        List<String> messages = errors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
        return finalHandler(exception, new ApiError(messages), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(MethodArgumentNotValidException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        List<String> messages = makeMessagesFromFieldErrors(exception.getBindingResult().getFieldErrors());
        return finalHandler(exception, new ApiError(messages), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(MissingServletRequestParameterException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.getMessage()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(RecordNotFound exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.getMessage()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(MethodArgumentTypeMismatchException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        String message = "Invalid value for " + exception.getName() + " - have you checked that this route exists?";
        return finalHandler(exception, new ApiError(message), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(HttpMessageNotReadableException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError("Unable to parse request JSON body"), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(JSONException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.getMessage()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(BadHttpRequest exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.toString()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(AccessDeniedException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.toString()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> handle(HttpClientErrorException exception, HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest request) {
        return finalHandler(exception, new ApiError(exception.toString()), httpHeaders, httpStatus, request);
    }

    private ResponseEntity<ApiError> finalHandler(
            Exception exception,
            ApiError apiError,
            HttpHeaders httpHeaders,
            HttpStatus httpStatus,
            WebRequest webRequest
    ) {
//    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(httpStatus)) {
//      webRequest.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, exception, WebRequest.SCOPE_REQUEST);
//    }

        return new ResponseEntity<>(apiError, httpHeaders, httpStatus);
    }

    private List<String> makeMessagesFromFieldErrors(List<FieldError> fieldErrors) {
        return fieldErrors.stream()
                .map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage())
                .collect(Collectors.toList());
    }
}
