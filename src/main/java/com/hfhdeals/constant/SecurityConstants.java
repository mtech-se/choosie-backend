package com.hfhdeals.constant;

public class SecurityConstants {
    public static final String SECRET = "HFHDeals_45672";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String ADMIN_TOKEN_PREFIX = "AdminBearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String CUSTOMER_SIGN_UP_URL = "/customers";
    public static final String ADMIN_ROLE = "hasAuthority('ADMIN')";
    public static final String[] ALLOWED_URL_PATTERNS = new String[]{""};

} 
