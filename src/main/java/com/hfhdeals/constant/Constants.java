package com.hfhdeals.constant;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

import java.util.Date;

public class Constants {
    public static final String StripeAPIKey = "sk_test_5aJbKC61rBtKz02YA2cEtklB"; // Change Your Stripe Key HERE

    public static final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSX"; // This format is what's agreed with the FE; don't change

    public static final DateTransformer DATE_TRANSFORMER = new DateTransformer(Constants.ISO_FORMAT);

    public static final String REGEX_NUMBER_NOT_0_OR_STARTING_WITH_0 = "^([1-9][0-9]*)$";

    public static JSONSerializer getJSONSerializer() {
        return new JSONSerializer()
                .transform(Constants.DATE_TRANSFORMER, Date.class)
                .exclude("*.class")   // exclude must come before include so that class key is excluded
                .include("*");
    }
}
