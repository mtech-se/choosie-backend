package com.hfhdeals.interceptor;

import com.hfhdeals.exception.InvalidRequestParams;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class QueryParamsInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws InvalidRequestParams {
        // get a list of valid request params e.g. ["name", "merchant_id"]
        // get a list of the incoming params e.g. ["name", "merchant_id", "some_illegal_param"]
        // if incomingParams has any item that is not in validParams, throw an exception

        List<String> validRequestParams = getValidRequestParams((HandlerMethod) handler);
        List<String> incomingRequestParams = getIncomingRequestParams(request);

        if (request.getMethod().equalsIgnoreCase("get")) {
            validRequestParams.add("page");
            validRequestParams.add("size");
        }

        List<String> invalidRequestParams = new ArrayList<>(incomingRequestParams);
        invalidRequestParams.removeAll(validRequestParams);

        if (invalidRequestParams.size() > 0) throw new InvalidRequestParams(invalidRequestParams);

        return true;
    }

    private List<String> getValidRequestParams(HandlerMethod handlerMethod) {
        List<String> validRequestParams = new ArrayList<>();

        for (MethodParameter methodParameter : handlerMethod.getMethodParameters()) {
            for (Annotation parameterAnnotation : methodParameter.getParameterAnnotations()) {
                if (parameterAnnotation.annotationType().getSimpleName().equals("RequestParam")) {
                    String garbledMess = parameterAnnotation.toString();
                    validRequestParams.add(extractValidParamsFromGarbledMess(garbledMess));
                }
            }
        }

        return validRequestParams;
    }

    private String extractValidParamsFromGarbledMess(String mess) {
        String regex = "\\b[\\w\\d]*=[\\w\\d]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(mess);

        while (matcher.find()) {
            String foundToken = matcher.group(0);

            String[] tokens = foundToken.split("=");
            if (tokens[0].equalsIgnoreCase("name")) return tokens[1];
        }

        return null;
    }

    private List<String> getIncomingRequestParams(HttpServletRequest request) {
        List<String> incomingRequestParams = new ArrayList<>();

        Map<String, String[]> parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            incomingRequestParams.add(entry.getKey());
        }

        return incomingRequestParams;
    }
}
