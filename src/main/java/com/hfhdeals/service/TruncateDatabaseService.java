package com.hfhdeals.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class TruncateDatabaseService {
    private EntityManager entityManager;

    @Autowired
    public TruncateDatabaseService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public List<String> truncate() {
        String getTableNamesQuery = "SELECT table_name FROM information_schema.tables WHERE table_schema ='app_database';";
        List<String> tableNames = entityManager.createNativeQuery(getTableNamesQuery).getResultList();
        tableNames.remove("flyway_schema_history");

        entityManager.flush();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        tableNames.forEach(tableName -> entityManager.createNativeQuery("TRUNCATE TABLE " + tableName).executeUpdate());
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();

        return tableNames;
    }
}