//package com.hfhdeals.service;
//
//import com.hfhdeals.database.dao.baserepository.DealOfTheMonthDao;
//import com.hfhdeals.model.jpaentities.Deal;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//
//@Service("dealOfTheMonthService")
//@Transactional
//public class DealOfTheMonthService {
//    @Qualifier("dealOfTheMonthDao")
//    private final DealOfTheMonthDao dealOfTheMonthDao;
//
//    @Autowired
//    public DealOfTheMonthService(DealOfTheMonthDao dealOfTheMonthDao) {
//        this.dealOfTheMonthDao = dealOfTheMonthDao;
//    }
//
//    public Page<Deal> getAll(Pageable pageable) {
//        return dealOfTheMonthDao.findAllByDealOfTheMonthTrueAndDeletedAtIsNull(pageable);
//    }
//
//    public Deal set(Integer id) {
//        Deal deal = dealOfTheMonthDao.getOne(id);
//        deal.setDealOfTheMonth(true);
//        return dealOfTheMonthDao.save(deal);
//    }
//
//    public Deal unset(Integer id) {
//        Deal deal = dealOfTheMonthDao.getOne(id);
//        deal.setDealOfTheMonth(false);
//        return dealOfTheMonthDao.save(deal);
//    }
//}
