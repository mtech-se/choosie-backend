//package com.hfhdeals.service.restservice;
//
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.model.jpaentities.Order;
//import com.hfhdeals.model.pojos.ChargeRequest;
//import com.hfhdeals.model.pojos.CreditCard;
//import com.hfhdeals.service.PaymentService;
//import com.stripe.model.Charge;
//import com.stripe.model.Token;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service("paymentRESTService")
//public class PaymentRESTService {
//
//    @Autowired
//    PaymentService service;
//
//    public Token getToken(CreditCard cc) {
//        return service.getToken(cc);
//    }
//
//    public Charge Pay(Token token, ChargeRequest cr) {
//        return service.Pay(token, cr);
//    }
//
//    public Order save(Order order) {
//        service.save(order);
//        return order;
//    }
//
//    public void saveCheckout(CheckoutDetails cc) {
//        service.saveCheckout(cc);
//    }
//
//    public int getLastCheckoutId() {
//        return service.getLastCheckoutId();
//
//    }
//
//    public int getLastCheckoutId(int id) {
//        return service.getLastCheckoutId(id);
//
//    }
//
//}
