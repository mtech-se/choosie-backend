package com.hfhdeals.service.restservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfhdeals.model.jpaentities.*;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.service.baseservice.OrderService;
import flexjson.JSONDeserializer;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("sreRESTService")
public class SRERESTService {

    @Value("${smartrecommendationengine.base_url}")
    private String SRE_BASE_URL;

    private DealService dealService;
    private CustomerService customerService;
    private CategoryService categoryService;
    private OrderService orderService;

    public SRERESTService(@Lazy DealService dealService, @Lazy CategoryService categoryService, @Lazy CustomerService customerService, @Lazy OrderService orderService) {
        this.dealService = dealService;
        this.categoryService = categoryService;
        this.customerService = customerService;
        this.orderService = orderService;
    }

    public void postDeals() {
        List<Deal> allDeals = dealService.getAll();
        JSONArray allDealsArray = transformDeals(allDeals);

        new Thread(new Runnable() {

            @Override
            public void run() {
                HttpPost httpPost = new HttpPost(SRE_BASE_URL + "/submitDeals");
                CloseableHttpClient httpclient = HttpClients.createDefault();
                try {
                    StringEntity entity = new StringEntity(allDealsArray.toString());
                    httpPost.setEntity(entity);
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    CloseableHttpResponse response = httpclient.execute(httpPost);
                    System.out.println(response.getStatusLine());
                    HttpEntity responseEntity = response.getEntity();
                    String jsonString = EntityUtils.toString(responseEntity);
                    System.out.println(jsonString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void postUsers() {
        List<Customer> allCustomers = customerService.getAll();
        JSONArray allCustomersArray = transformCustomers(allCustomers);

        new Thread(new Runnable() {

            @Override
            public void run() {
                HttpPost httpPost = new HttpPost(SRE_BASE_URL + "/submitUsers");
                CloseableHttpClient httpclient = HttpClients.createDefault();
                try {
                    StringEntity entity = new StringEntity(allCustomersArray.toString());
                    httpPost.setEntity(entity);
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    CloseableHttpResponse response = httpclient.execute(httpPost);
                    System.out.println(response.getStatusLine());
                    HttpEntity responseEntity = response.getEntity();
                    String jsonString = EntityUtils.toString(responseEntity);
                    System.out.println(jsonString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void postRatings() {
        List<Order> allOrders = orderService.getAll();
        JSONArray allOrderItemsArray = transformReviews(allOrders);
        new Thread(new Runnable() {

            @Override
            public void run() {
                HttpPost httpPost = new HttpPost(SRE_BASE_URL + "/submitRatings");
                CloseableHttpClient httpclient = HttpClients.createDefault();
                try {
                    StringEntity entity = new StringEntity(allOrderItemsArray.toString());
                    httpPost.setEntity(entity);
                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    CloseableHttpResponse response = httpclient.execute(httpPost);
                    System.out.println(response.getStatusLine());
                    HttpEntity responseEntity = response.getEntity();
                    String jsonString = EntityUtils.toString(responseEntity);
                    System.out.println(jsonString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private JSONArray transformDeals(List<Deal> deals) {
        JSONArray output = new JSONArray();
        List<Category> categories = categoryService.getAll();
        for (Deal deal : deals) {
            JSONObject json = new JSONObject();
            json.put("deal_id", deal.getId());
            for (CategoryDeal cd : deal.getCategoryDeals()) {
                json.put(cd.getCategory().getName(), 1);
            }
            for (Category c : categories) {
                if (false == json.has(c.getName())) {
                    json.put(c.getName(), 0);
                }
            }
            int[] discountSlabs = new int[]{5, 10, 15, 25, 50, 75};
            int discountPercent = (deal.getOriginalPrice() - deal.getDiscountedPrice()) * 100 / deal.getDiscountedPrice();
            int discountSlab = 0 < discountPercent && discountPercent <= 5 ? 5
                    : 5 < discountPercent && discountPercent <= 10 ? 10
                    : 10 < discountPercent && discountPercent <= 15 ? 15
                    : 15 < discountPercent && discountPercent <= 25 ? 25 : 25 < discountPercent && discountPercent <= 50 ? 50 : 75;
            json.put("discount_" + discountSlab, 1);
            for (int i = 0; i < discountSlabs.length; i++) {
                if (false == json.has("discount_" + discountSlabs[i]))
                    json.put("discount_" + discountSlabs[i], 0);
            }
            output.put(json);
        }
        //System.out.println(output);
        return output;
    }

    private JSONArray transformCustomers(List<Customer> all) {
        JSONArray output = new JSONArray();
        for (Customer c : all) {
            JSONObject user = new JSONObject();
            user.put("user_id", c.getId());
            user.put("gender", c.getGender() == null ? 0 : c.getGender().equalsIgnoreCase("male") ? 1 : 2);
            user.put("zip_code", c.getBillingAddPostalCode() == null ? 0 : c.getBillingAddPostalCode());
            output.put(user);
        }
        //System.out.println(output);
        return output;
    }

    private JSONArray transformReviews(List<Order> all) {
        JSONArray output = new JSONArray();
        for (Order o : all) {
            for (OrderItem oi : o.getOrderItems()) {
                JSONObject orderItem = new JSONObject();
                orderItem.put("user_id", o.getCustomer().getId());
                orderItem.put("deal_id", oi.getDeal().getId());
                orderItem.put("rates", 1);
                orderItem.put("time", o.getCreatedAt().getTime());
                output.put(orderItem);
            }
        }
        //System.out.println(output);

        return output;
    }

    public JSONArray getRecommendedDeals(Integer dealId) throws Exception {
        JSONArray recommendedDeals = new JSONArray();
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(SRE_BASE_URL + "/getRmdDeals/" + dealId);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        System.out.println(response.getStatusLine());
        HttpEntity entity = response.getEntity();
        String jsonString = EntityUtils.toString(entity);
        if (!jsonString.startsWith("\"[")) {
            throw new Exception(jsonString);
        }
        JSONArray jsonArray = new JSONArray(new JSONDeserializer<String>().deserialize(jsonString));
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                Deal deal = dealService.getById(jsonArray.getJSONObject(i).optInt("Deal id"));
                recommendedDeals.put(new JSONObject(new ObjectMapper().writeValueAsString(deal)));
            } catch (Exception e) {
                e.printStackTrace();
                //Deal doesn't exist.
            }
        }
        EntityUtils.consume(entity);
        return recommendedDeals;
    }

}
