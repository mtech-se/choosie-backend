package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.DealDao;
import com.hfhdeals.database.dao.baserepository.DealPhotoDao;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.DealPhoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("dealPhotoService")
@Transactional
public class DealPhotoService extends BaseService<Deal, Integer> {
    private final DealDao dealDao;
    private final DealPhotoDao dealPhotoDao;

    @Autowired
    public DealPhotoService(DealDao dealDao, DealPhotoDao dealPhotoDao) {
        this.dealDao = dealDao;
        this.dealPhotoDao = dealPhotoDao;
    }

    public Page<DealPhoto> getDealPhotosByDealAndDeletedAtIsNull(Deal deal, Pageable pageable) {
        return dealPhotoDao.getDealPhotosByDealAndDeletedAtIsNull(deal, pageable);
    }
}
