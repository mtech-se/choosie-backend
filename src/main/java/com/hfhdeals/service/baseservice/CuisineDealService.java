package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CuisineDealDao;
import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.model.jpaentities.CuisineDeal;
import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("cuisineDealService")
@Transactional
public class CuisineDealService extends BaseService<CuisineDeal, Integer> {
    private final CuisineDealDao cuisineDealDao;

    @Autowired
    public CuisineDealService(CuisineDealDao cuisineDealDao) {
        this.cuisineDealDao = cuisineDealDao;
    }

    public CuisineDeal get(Cuisine cuisine, Deal deal) {
        return cuisineDealDao.findByCuisineAndDeal(cuisine, deal);
    }

    @Override
    public CuisineDeal save(CuisineDeal cuisineDeal) {
        super.save(cuisineDeal);

        Cuisine cuisine = cuisineDeal.getCuisine();
        Deal deal = cuisineDeal.getDeal();

        cuisine.getCuisineDeals().add(cuisineDeal);
        deal.getCuisineDeals().add(cuisineDeal);

        return cuisineDeal;
    }

    public void delete(CuisineDeal cuisineDeal) {
        Cuisine cuisine = cuisineDeal.getCuisine();
        Deal deal = cuisineDeal.getDeal();

        cuisine.getCuisineDeals().remove(cuisineDeal);
        deal.getCuisineDeals().remove(cuisineDeal);

        cuisineDealDao.deleteCuisineDealByCuisineAndDeal(cuisineDeal.getCuisine(), cuisineDeal.getDeal());
    }
}
