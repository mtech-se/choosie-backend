package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.PhotoDao;
import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.model.jpaentities.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service("photoService")
@Transactional
public class PhotoService extends BaseService<Photo, Integer> {
    private final PhotoDao photoDao;

    @Value("${upload.path}")
    private String uploadDirectory;

    @Autowired
    public PhotoService(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    public void savePhotoAndFile(Photo photo, MultipartFile photoFile) throws IOException {
        super.save(photo);
        Path photoPath = Paths.get(uploadDirectory + photo.getPath());
        Files.createDirectories(photoPath.getParent());
        Files.createFile(photoPath);
        Files.copy(photoFile.getInputStream(), photoPath, StandardCopyOption.REPLACE_EXISTING);
    }

    public byte[] getPhotoFile(Photo photo) throws IOException {
        Path photoPath = Paths.get(uploadDirectory + photo.getPath());
        return Files.readAllBytes(photoPath);
    }

    @Override
    public void softDelete(Integer id) {
        Photo photo = photoDao.getOne(id);
        if (photo == null) throw new RecordNotFound("No record found with ID " + id);
        Path photoPath = Paths.get(uploadDirectory + photo.getPath());
        try {
            Files.delete(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.softDelete(id);
    }
}
