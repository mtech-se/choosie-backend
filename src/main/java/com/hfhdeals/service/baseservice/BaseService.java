package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.BaseRepository;
import com.hfhdeals.exception.RecordNotFound;
import com.hfhdeals.model.jpaentities.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

abstract class BaseService<T extends BaseEntity, ID extends Serializable> {
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    protected BaseRepository<T, ID> baseRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public Page<T> getAll(Pageable pageable) {
        return baseRepository.findAll(pageable);
    }

    public List<T> getAll() {
        return baseRepository.findAll();
    }

    public T getById(ID id) {
        T t = baseRepository.getOne(id);
        if (t == null) throw new RecordNotFound("No record found with ID " + id);
        return t;
    }

    @Transactional
    public T save(T t) {
        baseRepository.save(t);
        entityManager.refresh(t);
        return t;
    }

    @Transactional
    public T update(T t) {
        ID id = (ID) t.getId();

        T foundT = baseRepository.getOne(id);
        if (foundT == null) throw new RecordNotFound("No record found with ID " + id);

        // we need to do this to copy over the existing createdAt value
        t.setCreatedAt(foundT.getCreatedAt());
        t.setUpdatedAt(new Date());

        return baseRepository.save(t);
    }

    @Transactional
    public void softDelete(ID id) {
        T t = baseRepository.getOne(id);
        if (t == null) throw new RecordNotFound("No record found with ID " + id);
        baseRepository.softDelete(id);
        entityManager.refresh(t);
    }

    public long count() {
        return baseRepository.count();
    }
}
