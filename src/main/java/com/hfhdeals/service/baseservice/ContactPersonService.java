package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.ContactPersonDao;
import com.hfhdeals.model.jpaentities.ContactPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("contactPersonService")
@Transactional
public class ContactPersonService extends BaseService<ContactPerson, Integer> {
    private final ContactPersonDao contactPersonDao;

    @Autowired
    public ContactPersonService(ContactPersonDao contactPersonDao) {
        this.contactPersonDao = contactPersonDao;
    }

    public Page<ContactPerson> getAll(String name, Pageable pageable) {
        return contactPersonDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }

    public Page<ContactPerson> getAll(Integer merchantId, Pageable pageable) {
        return contactPersonDao.findAllByMerchant_IdEqualsAndDeletedAtIsNull(merchantId, pageable);
    }

    public Page<ContactPerson> getAll(String name, Integer merchantId, Pageable pageable) {
        return contactPersonDao.findAllByNameContainingAndMerchant_IdAndDeletedAtIsNull(name, merchantId, pageable);
    }
}
