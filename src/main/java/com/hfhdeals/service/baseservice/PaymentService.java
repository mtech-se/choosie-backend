package com.hfhdeals.service.baseservice;

import com.hfhdeals.constant.Constants;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.model.jpaentities.Payment;
import com.hfhdeals.requestbody.PaymentRequestBody;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Token;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("paymentService")
@Transactional
public class PaymentService extends BaseService<Payment, Integer> {

    public Payment payWithStripe(Order order, PaymentRequestBody paymentRequestBody) {

        Payment payment = new Payment();
        payment.setOrder(order);
        payment.setReceiptAmount(paymentRequestBody.getAmount());
        payment.setSpotBillingAddress(paymentRequestBody.getBillingAddress());
        save(payment);

        Stripe.apiKey = Constants.StripeAPIKey;
        Token token = null;

        Map<String, Object> tokenParams = new HashMap<>();
        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("number", paymentRequestBody.getCreditCard().getNumber());
        cardParams.put("exp_month", paymentRequestBody.getCreditCard().getExpMonth());
        cardParams.put("exp_year", paymentRequestBody.getCreditCard().getExpYear());
        cardParams.put("cvc", paymentRequestBody.getCreditCard().getCvv());
        tokenParams.put("card", cardParams);

        try {
            token = Token.create(tokenParams);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Stripe.apiKey = Constants.StripeAPIKey;
        Charge charge = null;
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", payment.getReceiptAmount());
        chargeParams.put("currency", paymentRequestBody.getCurrency());
        chargeParams.put("source", token.getId());
        try {
            charge = Charge.create(chargeParams);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //charge.get
        payment.setCompletedAt(new Date());
        return payment;
    }

}

//package com.hfhdeals.service;
//
//import com.hfhdeals.database.dao.PaymentDao;
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.model.jpaentities.Order;
//import com.hfhdeals.model.pojos.ChargeRequest;
//import com.hfhdeals.model.pojos.CreditCard;
//import com.stripe.model.Charge;
//import com.stripe.model.Token;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//
//@Service("paymentService")
//@Transactional
//public class PaymentService {
//
//    @Autowired
//    PaymentDao paymentDao;
//
//    public Token getToken(CreditCard cc) {
//        return paymentDao.createToken(cc);
//    }
//
//    public Charge Pay(Token token, ChargeRequest cr) {
//        return paymentDao.createCharge(token, cr);
//
//    }
//
//    public Order save(Order order) {
//        paymentDao.save(order);
//        return order;
//    }
//
//    public void saveCheckout(CheckoutDetails cc) {
//        paymentDao.saveCheckout(cc);
//    }
//
//    public int getLastCheckoutId() {
//        return paymentDao.getLastCheckoutId();
//
//    }
//
//    public int getLastCheckoutId(int id) {
//        return paymentDao.getLastCheckoutId(id);
//
//    }
//
//}
