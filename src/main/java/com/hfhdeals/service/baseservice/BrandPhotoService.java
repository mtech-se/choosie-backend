package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.BrandDao;
import com.hfhdeals.database.dao.baserepository.BrandPhotoDao;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.BrandPhoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("brandPhotoService")
@Transactional
public class BrandPhotoService extends BaseService<Brand, Integer> {
    private final BrandDao brandDao;
    private final BrandPhotoDao brandPhotoDao;

    @Autowired
    public BrandPhotoService(BrandDao brandDao, BrandPhotoDao brandPhotoDao) {
        this.brandDao = brandDao;
        this.brandPhotoDao = brandPhotoDao;
    }

    public Page<BrandPhoto> getBrandPhotosByBrandAndDeletedAtIsNull(Brand brand, Pageable pageable) {
        return brandPhotoDao.getBrandPhotosByBrandAndDeletedAtIsNull(brand, pageable);
    }
}
