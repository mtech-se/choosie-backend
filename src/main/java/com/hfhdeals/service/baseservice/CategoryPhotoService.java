package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CategoryDao;
import com.hfhdeals.database.dao.baserepository.CategoryPhotoDao;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryPhoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("categoryPhotoService")
@Transactional
public class CategoryPhotoService extends BaseService<Category, Integer> {
    private final CategoryDao categoryDao;
    private final CategoryPhotoDao categoryPhotoDao;

    @Autowired
    public CategoryPhotoService(CategoryDao categoryDao, CategoryPhotoDao categoryPhotoDao) {
        this.categoryDao = categoryDao;
        this.categoryPhotoDao = categoryPhotoDao;
    }

    public Page<CategoryPhoto> getCategoryPhotosByCategoryAndDeletedAtIsNull(Category category, Pageable pageable) {
        return categoryPhotoDao.getCategoryPhotosByCategoryAndDeletedAtIsNull(category, pageable);
    }
}
