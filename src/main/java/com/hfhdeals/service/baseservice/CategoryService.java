package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CategoryDao;
import com.hfhdeals.model.jpaentities.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("categoryService")
@Transactional
public class CategoryService extends BaseService<Category, Integer> {
    private final CategoryDao categoryDao;

    @Autowired
    public CategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    public Page<Category> getAll(String name, Pageable pageable) {
        return categoryDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }

    @Override
    public Category update(Category category) {
        Category foundCategory = getById(category.getId());
        if (foundCategory == null) throw new EmptyResultDataAccessException(1);

        category.setCategoryPhotos(foundCategory.getCategoryPhotos());

        return super.update(category);
    }
}
