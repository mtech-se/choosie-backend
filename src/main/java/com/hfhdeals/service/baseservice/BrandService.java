package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.BrandDao;
import com.hfhdeals.model.jpaentities.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("brandService")
@Transactional
public class BrandService extends BaseService<Brand, Integer> {
    private final BrandDao brandDao;

    @Autowired
    public BrandService(BrandDao brandDao) {
        this.brandDao = brandDao;
    }

    public Page<Brand> getAll(String name, Pageable pageable) {
        return brandDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }

    public Page<Brand> getAll(Integer merchantId, Pageable pageable) {
        return brandDao.findAllByMerchant_IdAndDeletedAtIsNull(merchantId, pageable);
    }

    public Page<Brand> getAll(String name, Integer merchantId, Pageable pageable) {
        return brandDao.findAllByNameContainingAndMerchant_IdAndDeletedAtIsNull(name, merchantId, pageable);
    }
}
