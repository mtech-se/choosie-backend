package com.hfhdeals.service.baseservice;

import com.hfhdeals.model.jpaentities.OrderItem;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("orderItemService")
@Transactional
public class OrderItemService extends BaseService<OrderItem, Integer> {
}
