package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.MerchantDao;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("merchantService")
@Transactional
public class MerchantService extends BaseService<Merchant, Integer> {
    private final MerchantDao merchantDao;

    @Autowired
    public MerchantService(MerchantDao merchantDao) {
        this.merchantDao = merchantDao;
    }

    public Page<Merchant> getAll(String name, Pageable pageable) {
        return merchantDao.findAllByNameIgnoreCaseContainingAndDeletedAtIsNull(name, pageable);
    }

    public Merchant createMerchantWithContactPersonsIfPresent(Merchant merchant) {
        Merchant newMerchant = new Merchant();
        newMerchant.setName(merchant.getName());
        newMerchant.setBusinessRegistrationNumber(merchant.getBusinessRegistrationNumber());
        newMerchant.setMailingAddress(merchant.getMailingAddress());

        save(newMerchant);

        if (merchant.getContactPersons().size() != 0) {
            ContactPerson contactPerson = merchant.getContactPersons().get(0);
            newMerchant.addContactPerson(contactPerson);
        }

        return newMerchant;
    }

//    @Override
//    public Merchant update(Merchant merchant) {
//        Merchant foundMerchant = getById(merchant.getId());
//        if (foundMerchant == null) throw new EmptyResultDataAccessException(1);
//
//        merchant.setContactPersons(foundMerchant.getContactPersons());
//
//        return super.update(merchant);
//    }
}
