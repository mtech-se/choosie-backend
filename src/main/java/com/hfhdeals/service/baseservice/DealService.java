package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.DealCustomerClickDao;
import com.hfhdeals.database.dao.baserepository.DealDao;
import com.hfhdeals.database.dao.baserepository.OrderItemDao;
import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service("dealService")
@Transactional
public class DealService extends BaseService<Deal, Integer> {
    private final DealDao dealDao;
    private final DealCustomerClickDao dealCustomerClickDao;
    private final OrderItemDao orderItemDao;

    @Autowired
    public DealService(DealDao dealDao, DealCustomerClickDao dealCustomerClickDao, OrderItemDao orderItemDao) {
        this.dealDao = dealDao;
        this.dealCustomerClickDao = dealCustomerClickDao;
        this.orderItemDao = orderItemDao;
    }

    @Override
    public Page<Deal> getAll(Pageable pageable) {
        Page<Deal> deals = super.getAll(pageable);
        loadAggregates(deals);
        return deals;
    }

    @Override
    public List<Deal> getAll() {
        List<Deal> deals = super.getAll();
        loadAggregates(deals);
        return deals;
    }

    @Override
    public Deal getById(Integer integer) {
        Deal deal = super.getById(integer);
        loadAggregates(deal);
        return deal;
    }

    @Override
    public Deal save(Deal deal) {
        Deal savedDeal = super.save(deal);
        loadAggregates(savedDeal);
        return savedDeal;
    }

    @Override
    public Deal update(Deal deal) {
        Deal updatedDeal = super.update(deal);
        loadAggregates(updatedDeal);
        return updatedDeal;
    }

    public Page<Deal> getDotmDeals(Pageable pageable) {
        Page<Deal> deals = dealDao.getDotmDealsOrderedByPurchaseCount(pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getDeals(String title, Integer categoryId, Integer priceMin, Integer priceMax, Pageable pageable) {
        Page<Deal> deals = dealDao.getDeals(title, categoryId, priceMin, priceMax, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getDeals(String title, Integer categoryId, Pageable pageable) {
        Page<Deal> deals = dealDao.getDeals(title, categoryId, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByDateRangeTitleAndPriceRange(Date startDate, Date endDate, String title, Integer priceMin, Integer priceMax, Pageable pageable) {
        return dealDao.findAllByCreatedAtBetweenAndTitleContainingAndDiscountedPriceBetweenAndDeletedAtIsNull(startDate, endDate, title, priceMin, priceMax, pageable);
    }

    public Page<Deal> getByDateRangeAndPriceRange(Date startDate, Date endDate, Integer priceMin, Integer priceMax, Pageable pageable) {
        return dealDao.findAllByCreatedAtBetweenAndDiscountedPriceBetweenAndDeletedAtIsNull(startDate, endDate, priceMin, priceMax, pageable);
    }

    public Page<Deal> getByDateRangeAndTitle(Date startDate, Date endDate, String title, Pageable pageable) {
        Page<Deal> deals = dealDao.findAllByCreatedAtBetweenAndTitleContainingAndDeletedAtIsNull(startDate, endDate, title, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByDateRange(Date startDate, Date endDate, Pageable pageable) {
        Page<Deal> deals = dealDao.findAllByCreatedAtBetweenAndDeletedAtIsNull(startDate, endDate, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByTitleAndPriceRange(String title, Integer priceMin, Integer priceMax, Pageable pageable) {
        Page<Deal> deals = dealDao.findAllByTitleContainingAndDiscountedPriceBetweenAndDeletedAtIsNull(title, priceMin, priceMax, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByPriceRange(Integer priceMin, Integer priceMax, Pageable pageable) {
        Page<Deal> deals = dealDao.findAllByDiscountedPriceBetweenAndDeletedAtIsNull(priceMin, priceMax, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByTitle(String title, Pageable pageable) {
        Page<Deal> deals = dealDao.findAllByTitleContainingAndDeletedAtIsNull(title, pageable);
        loadAggregates(deals);
        return deals;
    }

    public Page<Deal> getByCategoryId(Integer categoryId, Pageable pageable) {
        Page<Deal> deals = dealDao.getDeals(categoryId, pageable);
        loadAggregates(deals);
        return deals;
    }

    public List<Deal> getAllPublishedDeals() {
        return dealDao.findAllByPublishedAtIsNotNull();
    }

    public void loadAggregates(Iterable<Deal> deals) {
        for (Deal deal : deals) {
            loadAggregates(deal);
        }
    }

    private void loadAggregates(Deal deal) {
        loadDealCustomerClickAggregate(deal);
        loadNumBoughtAggregate(deal);
    }

    private void loadDealCustomerClickAggregate(Deal deal) {
        Integer hitCount = Math.toIntExact(dealCustomerClickDao.countAllByDealId(deal.getId()));
        deal.setHitCount(hitCount);
    }

    private void loadNumBoughtAggregate(Deal deal) {
        Integer numBought = Math.toIntExact(orderItemDao.countAllByDealId(deal.getId()));
        deal.setNumBought(numBought);
    }
}
