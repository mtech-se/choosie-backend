package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CategoryDealDao;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryDeal;
import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("categoryDealService")
@Transactional
public class CategoryDealService extends BaseService<CategoryDeal, Integer> {
    private final CategoryDealDao categoryDealDao;

    @Autowired
    public CategoryDealService(CategoryDealDao categoryDealDao) {
        this.categoryDealDao = categoryDealDao;
    }

    public CategoryDeal get(Category category, Deal deal) {
        return categoryDealDao.findByCategoryAndDeal(category, deal);
    }

    @Override
    public CategoryDeal save(CategoryDeal categoryDeal) {
        super.save(categoryDeal);

        Category category = categoryDeal.getCategory();
        Deal deal = categoryDeal.getDeal();

        category.getCategoryDeals().add(categoryDeal);
        deal.getCategoryDeals().add(categoryDeal);

        return categoryDeal;
    }

    public void delete(CategoryDeal categoryDeal) {
        Category category = categoryDeal.getCategory();
        Deal deal = categoryDeal.getDeal();

        category.getCategoryDeals().remove(categoryDeal);
        deal.getCategoryDeals().remove(categoryDeal);

        categoryDealDao.deleteCategoryDealByCategoryAndDeal(categoryDeal.getCategory(), categoryDeal.getDeal());
    }
}
