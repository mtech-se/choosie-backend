package com.hfhdeals.service.baseservice;

import com.hfhdeals.model.jpaentities.DealCustomerClick;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("dealCustomerClickService")
@Transactional
public class DealCustomerClickService extends BaseService<DealCustomerClick, Integer> {
}
