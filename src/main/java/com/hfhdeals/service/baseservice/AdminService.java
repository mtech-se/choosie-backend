package com.hfhdeals.service.baseservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfhdeals.database.dao.baserepository.AdminDao;
import com.hfhdeals.model.jpaentities.Admin;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("adminService")
@Transactional
public class AdminService extends BaseService<Admin, Integer> {
    private final AdminDao adminDao;

    @Autowired
    public AdminService(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    public Page<Admin> getAll(String name, Pageable pageable) {
        return adminDao.findAllByFirstNameContainingOrLastNameContainingAndDeletedAtIsNull(name, name, pageable);
    }

    public Admin getByEmail(String email) {
        return adminDao.findByEmailAndDeletedAtIsNull(email);
    }

    public JSONObject getJSONByEmail(String email) throws JSONException, JsonProcessingException {
        return new JSONObject(new ObjectMapper().writeValueAsString(adminDao.findByEmailAndDeletedAtIsNull(email)));
    }
}
