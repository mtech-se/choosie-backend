package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.LocationDao;
import com.hfhdeals.model.jpaentities.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("locationService")
@Transactional
public class LocationService extends BaseService<Location, Integer> {
    private final LocationDao locationDao;

    @Autowired
    public LocationService(LocationDao locationDao) {
        this.locationDao = locationDao;
    }

    public Page<Location> getAll(String name, Pageable pageable) {
        return locationDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }

    public Page<Location> getAll(Integer countryId, Pageable pageable) {
        return locationDao.findAllByCountry_IdAndDeletedAtIsNull(countryId, pageable);
    }

    public Page<Location> getAll(String name, Integer countryId, Pageable pageable) {
        return locationDao.findAllByNameContainingAndCountry_IdAndDeletedAtIsNull(name, countryId, pageable);
    }
}
