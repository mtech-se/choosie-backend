package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.OrderDao;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("orderService")
@Transactional
public class OrderService extends BaseService<Order, Integer> {

    private final OrderDao orderDao;

    @Autowired
    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public Page<Order> getCurrentCart(Customer customer, Pageable pageable) {
        Page<Order> all = orderDao.findAllByCustomer(customer, pageable);
        List<Order> allList = all.getContent();
        List<Order> matching = new ArrayList<Order>();
        for (Order order : allList) {
            if (order.getPayments() == null || order.getPayments().size() == 0) {
                matching.add(order);
            }
        }
        Page<Order> newPage = new PageImpl<>(matching);
        return newPage;
    }

    public Page<Order> getAllByCustomer(Customer customer, Pageable pageable) {
        Page<Order> all = orderDao.findAllByCustomer(customer, pageable);
        return all;
    }
}
