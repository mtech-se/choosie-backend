package com.hfhdeals.service.baseservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfhdeals.database.dao.baserepository.CustomerDao;
import com.hfhdeals.model.jpaentities.Customer;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("customerService")
@Transactional
public class CustomerService extends BaseService<Customer, Integer> {
    private final CustomerDao customerDao;

    @Autowired
    public CustomerService(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public Page<Customer> getAll(String name, Pageable pageable) {
        return customerDao.getAll(name, pageable);
    }

    public Customer getByEmail(String email) {
        return customerDao.findByEmailAndDeletedAtIsNull(email);
    }

    public JSONObject getJSONByEmail(String email) throws JSONException, JsonProcessingException {
        return new JSONObject(new ObjectMapper().writeValueAsString(customerDao.findByEmailAndDeletedAtIsNull(email)));
    }
}
