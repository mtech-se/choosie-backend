package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CountryDao;
import com.hfhdeals.model.jpaentities.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("countryService")
@Transactional
public class CountryService extends BaseService<Country, Integer> {
    private final CountryDao countryDao;

    @Autowired
    public CountryService(CountryDao countryDao) {
        this.countryDao = countryDao;
    }

    public Page<Country> getAll(String name, Pageable pageable) {
        return countryDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }
}
