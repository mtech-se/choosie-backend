package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.CuisineDao;
import com.hfhdeals.model.jpaentities.Cuisine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("cuisineService")
@Transactional
public class CuisineService extends BaseService<Cuisine, Integer> {
    private final CuisineDao cuisineDao;

    @Autowired
    public CuisineService(CuisineDao cuisineDao) {
        this.cuisineDao = cuisineDao;
    }

    public Page<Cuisine> getAll(String name, Pageable pageable) {
        return cuisineDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }
}
