package com.hfhdeals.service.baseservice;

import com.hfhdeals.database.dao.baserepository.OutletDao;
import com.hfhdeals.model.jpaentities.Outlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("outletService")
@Transactional
public class OutletService extends BaseService<Outlet, Integer> {
    private final OutletDao outletDao;

    @Autowired
    public OutletService(OutletDao outletDao) {
        this.outletDao = outletDao;
    }

    public Page<Outlet> getAll(String name, Pageable pageable) {
        return outletDao.findAllByNameContainingAndDeletedAtIsNull(name, pageable);
    }

    public Page<Outlet> getAll(Integer brandId, Pageable pageable) {
        return outletDao.findAllByBrand_IdAndDeletedAtIsNull(brandId, pageable);
    }

    public Page<Outlet> getAll(String name, Integer brandId, Pageable pageable) {
        return outletDao.findAllByNameContainingAndBrand_IdAndDeletedAtIsNull(name, brandId, pageable);
    }
}
