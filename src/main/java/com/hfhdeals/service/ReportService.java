package com.hfhdeals.service;

import com.hfhdeals.database.dao.baserepository.*;
import com.hfhdeals.database.dao.customdao.CustomizedMerchantDao;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.model.pojos.CategoryDTO;
import com.hfhdeals.model.pojos.MerchantDTO;
import com.hfhdeals.service.baseservice.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("reportService")
@Transactional
public class ReportService {
    private final CustomizedMerchantDao customizedMerchantDao;
    private final DealDao dealDao;
    private final CustomerDao customerDao;
    private final OrderItemDao orderItemDao;
    private final CategoryDao categoryDao;
    private final MerchantDao merchantDao;
    private final DealService dealService;

    @Autowired
    public ReportService(@Qualifier("customizedMerchantDaoImpl") CustomizedMerchantDao customizedMerchantDao, DealDao dealDao, CustomerDao customerDao, OrderItemDao orderItemDao, CategoryDao categoryDao, MerchantDao merchantDao, DealService dealService) {
        this.customizedMerchantDao = customizedMerchantDao;
        this.dealDao = dealDao;
        this.customerDao = customerDao;
        this.orderItemDao = orderItemDao;
        this.categoryDao = categoryDao;
        this.merchantDao = merchantDao;
        this.dealService = dealService;
    }

    public int getTotalPublishedDeals(Date startDate, Date endDate) {
        return dealDao.countAllByDeletedAtIsNullAndPublishedAtIsNotNullAndPublishedAtBetween(startDate, endDate);
    }

    public int getTotalPurchasedDeals(Date startDate, Date endDate) {
        return orderItemDao.countAllByDeletedAtIsNullAndCreatedAtBetween(startDate, endDate);
    }

    public int getTotalNewCustomers(Date startDate, Date endDate) {
        return customerDao.countAllByDeletedAtIsNullAndCreatedAtBetween(startDate, endDate);
    }

    public int getTotalSales(Date startDate, Date endDate) {
        return orderItemDao.getSumSpotPrice(startDate, endDate);
    }

    public Page<Deal> getTopDealsByPurchaseCount(Date startDate, Date endDate) {
        Page<Deal> deals = dealDao.getDealsOrderedByPurchaseCount(startDate, endDate, new PageRequest(0, 5));

        dealService.loadAggregates(deals);

        return deals;
    }

    public List<CategoryDTO> getTopCategoriesByPublishedDealCount(Date startDate, Date endDate) {
        List<Object[]> records = categoryDao.getCategoriesWithPublishedDealCount(startDate, endDate, new PageRequest(0, 5));
        List<CategoryDTO> categoryDTOS = new ArrayList<>();

        for (Object[] record : records) {
            Category category = (Category) record[0];
            int numPublishedDeals = Math.toIntExact((long) record[1]);

            CategoryDTO categoryDTO = new CategoryDTO(category, numPublishedDeals);

            categoryDTOS.add(categoryDTO);
        }

        return categoryDTOS;
    }

    public List<MerchantDTO> getTopMerchantsByPurchaseRedemptionCount(Date startDate, Date endDate) {
        List<Object[]> records = merchantDao.getMerchantsWithRedeemedDealCount(startDate, endDate, new PageRequest(0, 5));
        List<MerchantDTO> merchantDTOS = new ArrayList<>();

        for (Object[] record : records) {
            Merchant merchant = (Merchant) record[0];
            int numDealsRedeemed = Math.toIntExact((long) record[1]);

            MerchantDTO merchantDTO = new MerchantDTO(merchant, numDealsRedeemed);

            merchantDTOS.add(merchantDTO);
        }

        return merchantDTOS;
    }
}
