package com.hfhdeals.utils;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.concurrent.ThreadLocalRandom;

public class CustomFaker extends Faker {
    public boolean bool(int percentageChanceForTrue) {
        assert percentageChanceForTrue >= 0;
        assert percentageChanceForTrue <= 100;

        int rolledNumber = super.number().numberBetween(0, 100);
        return rolledNumber <= percentageChanceForTrue;
    }

    public List<Integer> getUniqueNumbers(int howManyNumbers, int lowerLimit, int upperLimit) {
        PrimitiveIterator.OfInt iterator = ThreadLocalRandom.current().ints(lowerLimit, upperLimit).distinct().iterator();

        List<Integer> uniqueNumbers = new ArrayList<>();
        for (int i = 0; i < howManyNumbers; i++) {
            uniqueNumbers.add(iterator.next());
        }

        return uniqueNumbers;
    }

    public Person makePerson() {
        Faker faker = new Faker();

        Name name = faker.name();
        String firstName = name.firstName();
        String lastName = name.lastName();

        String fullNameForEmail = firstName + " " + lastName;
        String afterLowerCasing = fullNameForEmail.toLowerCase();
        String afterRemovingSpecials = StringUtils.removeNonAlphanumeric(afterLowerCasing);
        String nameForEmail = StringUtils.replaceSpacesWithUnderscores(afterRemovingSpecials);

        String email = nameForEmail + "@example.com";

        return new Person(firstName, lastName, email);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public class Person {
        private String firstName;
        private String lastName;
        private String email;
    }
}