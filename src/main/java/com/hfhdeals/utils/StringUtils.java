package com.hfhdeals.utils;

public class StringUtils {
    public static String removeNonAlphanumeric(String inputString) {
        return inputString.replaceAll("[^A-Za-z0-9 ]", "");
    }

    public static String replaceSpacesWithUnderscores(String inputString) {
        return inputString.replaceAll("\\s", "_");
    }
}
