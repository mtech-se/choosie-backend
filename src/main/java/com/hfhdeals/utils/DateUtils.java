package com.hfhdeals.utils;

import com.hfhdeals.constant.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date fromString(String string, DateFormat dateFormat) throws ParseException {
        return new SimpleDateFormat(dateFormat.pattern).parse(string);
    }

    public static Boolean isDateBetweenInclusive(Date dateUnderTest, Date lowerBoundary, Date upperBoundary) {
        return isDateLaterInclusive(dateUnderTest, lowerBoundary) && isDateEarlierInclusive(dateUnderTest, upperBoundary);
    }

    public static Boolean isDateLaterInclusive(Date dateUnderTest, Date lowerBoundary) {
        return dateUnderTest.compareTo(lowerBoundary) >= 0;
    }

    public static Boolean isDateEarlierInclusive(Date dateUnderTest, Date lowerBoundary) {
        return dateUnderTest.compareTo(lowerBoundary) <= 0;
    }

    public static Date addDays(Date date, int numDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, numDays);

        return calendar.getTime();
    }

    public static String toIsoFormatString(Date date) {
        if (date == null) return null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_FORMAT);
        return simpleDateFormat.format(date);
    }

    public enum DateFormat {
        MYSQL_FORMAT("yyyy-MM-dd HH:mm:ss");

        private String pattern;

        DateFormat(String pattern) {
            this.pattern = pattern;
        }
    }
}