package com.hfhdeals.security;

import com.hfhdeals.model.jpaentities.Admin;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.service.baseservice.AdminService;
import com.hfhdeals.service.baseservice.CustomerService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private CustomerService customerService;
    private AdminService adminService;

    public UserDetailsServiceImpl(CustomerService customerService, AdminService adminService) {
        this.customerService = customerService;
        this.adminService = adminService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (request.getAttribute("login_type") != null && request.getAttribute("login_type").equals("admin")) {
            Admin admin = adminService.getByEmail(email);
            if (admin == null) {
                throw new UsernameNotFoundException(email);
            }
            return new User(admin.getEmail(), admin.getPassword(), emptyList());
        } else {
            Customer customer = customerService.getByEmail(email);
            if (customer == null) {
                throw new UsernameNotFoundException(email);
            }
            return new User(customer.getEmail(), customer.getPassword(), emptyList());
        }
    }
}