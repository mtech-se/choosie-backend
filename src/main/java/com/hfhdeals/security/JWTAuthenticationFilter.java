package com.hfhdeals.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfhdeals.service.baseservice.AdminService;
import com.hfhdeals.service.baseservice.CustomerService;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.hfhdeals.constant.SecurityConstants.*;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private AdminService adminService;
    private CustomerService customerService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, AdminService adminService, CustomerService customerService) {
        this.authenticationManager = authenticationManager;
        this.adminService = adminService;
        this.customerService = customerService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            Map data = new ObjectMapper()
                    .readValue(req.getInputStream(), Map.class);

            if (data != null && (data.get("type") + "").equalsIgnoreCase("admin")) {
                req.setAttribute("login_type", "admin");
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                data.get("email"),
                                data.get("password"),
                                new ArrayList<>())
                );

            } else {
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                data.get("email"),
                                data.get("password"),
                                new ArrayList<>())
                );
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        String email = ((User) auth.getPrincipal()).getUsername();
        String token = JWT.create()
                .withSubject(email)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));

        boolean isAdmin = (req.getAttribute("login_type") + "").equalsIgnoreCase("admin");
        String accessTokenPrefix = isAdmin ? ADMIN_TOKEN_PREFIX : TOKEN_PREFIX;
        JSONObject json = new JSONObject();
        json.put("access_token_prefix", accessTokenPrefix);
        json.put("access_token", token);
        json.put("user", isAdmin ? adminService.getJSONByEmail(email) : customerService.getJSONByEmail(email));

        res.addHeader("Content-Type", "application/json");
        res.setStatus(HttpStatus.OK.value());
        res.getWriter().write(json + "");
        res.flushBuffer();
    }
}