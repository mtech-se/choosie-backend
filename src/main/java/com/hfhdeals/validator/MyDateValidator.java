package com.hfhdeals.validator;

import com.hfhdeals.annotation.ValidDateRange;
import com.hfhdeals.model.pojos.DateRange;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

public class MyDateValidator implements ConstraintValidator<ValidDateRange, DateRange> {

    @Override
    public void initialize(ValidDateRange constraintAnnotation) {
    }

    @Override
    public boolean isValid(DateRange dateRange, ConstraintValidatorContext context) {
        Date startDate = dateRange.getStartDate();
        Date endDate = dateRange.getEndDate();
        return startDate.before(endDate);
    }

}
