package com.hfhdeals.model.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public interface DealDTO {
    Integer getId();

    String getTitle();

    String getDescription();

    @JsonProperty("original_price")
    Integer getOriginalPrice();

    @JsonProperty("discounted_price")
    Integer getDiscountedPrice();

    @JsonProperty("terms_of_use")
    String getTermsOfUse();

    @JsonProperty("redemption_instructions")
    String getRedemptionInstructions();

    @JsonProperty("max_quantity")
    Integer getMaxQuantity();

//    Outlet getOutlet();

    @JsonProperty("auto_publish_date")
    Date getAutoPublishDate();

    @JsonProperty("published_at")
    Date getPublishedAt();

    @JsonProperty("redemption_expiry_date")
    Date getRedemptionExpiryDate();

    @JsonProperty("archived_at")
    Date getArchivedAt();

    @JsonProperty("deal_of_the_month")
    boolean getDealOfTheMonth();

    @JsonProperty("num_bought")
    Integer getNumBought();

    @JsonProperty("hit_count")
    Integer getHitCount();
}
