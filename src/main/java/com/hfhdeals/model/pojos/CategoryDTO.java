package com.hfhdeals.model.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.model.jpaentities.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CategoryDTO {

    @JsonProperty("category")
    private Category category;

    @JsonProperty("num_published_deals")
    private Integer numPublishedDeals;
}
