package com.hfhdeals.model.pojos;

import com.hfhdeals.constant.Constants;
import flexjson.JSON;
import flexjson.JSONDeserializer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Login implements Serializable {

    @JSON(name = "email")
    private String email;
    @JSON(name = "password")
    private String password;

    public Login() {
    }

    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject(Constants.getJSONSerializer().serialize(this));
    }

    public static Login fromJson(JSONObject json) {
        return new JSONDeserializer<Login>().deserialize(json + "", Login.class);
    }

    @Override
    public String toString() {
        return "Login{" + "email=" + email + ", password=" + password + '}';
    }

}
