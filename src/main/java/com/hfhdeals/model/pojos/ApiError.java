package com.hfhdeals.model.pojos;

import java.util.ArrayList;
import java.util.List;

public class ApiError {
    private List<String> errors = new ArrayList<>();

    public ApiError(String error) {
        this.errors.add(error);
    }

    public ApiError(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "errors=" + errors +
                '}';
    }
}
