package com.hfhdeals.model.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.annotation.ValidDateRange;
import com.hfhdeals.constant.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@ToString
@ValidDateRange
public class DateRange {
    @DateTimeFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("startDate")
    @NotNull
    private Date startDate;

    @DateTimeFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("endDate")
    @NotNull
    private Date endDate;
}
