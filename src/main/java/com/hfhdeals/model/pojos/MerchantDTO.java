package com.hfhdeals.model.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.model.jpaentities.Merchant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MerchantDTO {

    @JsonProperty("merchant")
    private Merchant merchant;

    @JsonProperty("num_deals_redeemed")
    private Integer numDealsRedeemed;
}
