package com.hfhdeals.model.pojos;

import com.hfhdeals.constant.Constants;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonReturnObject {

    private Object content;

    public JsonReturnObject() {
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject(Constants.getJSONSerializer().serialize(this));

        if (content instanceof JSONObject || content instanceof JSONArray) jsonObject.put("content", content);

        return jsonObject;
    }

}
