package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "merchants")
@NoArgsConstructor
@Getter
@Setter
public class Merchant extends BaseEntity {
    @NotNull
    private String name;

    @Column(name = "business_registration_number")
    @JsonProperty("business_registration_number")
    @NotNull
    private String businessRegistrationNumber;

    @Column(name = "mailing_address")
    @JsonProperty("mailing_address")
    @NotNull
    private String mailingAddress;

    @OneToMany(
            mappedBy = "merchant",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("contact_persons")
    @JsonIgnoreProperties("merchant")
    private List<ContactPerson> contactPersons = new ArrayList<>();

    @OneToMany(
            mappedBy = "merchant",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties({"merchant", "brand_photos"})
    private List<Brand> brands = new ArrayList<>();

    public void addContactPerson(ContactPerson contactPerson) {
        contactPersons.add(contactPerson);
        contactPerson.setMerchant(this);
    }

    public void removeContactPerson(ContactPerson contactPerson) {
        contactPersons.remove(contactPerson);
        contactPerson.setMerchant(null);
    }

    public void addBrand(Brand brand) {
        brands.add(brand);
        brand.setMerchant(this);
    }

    public void removeBrand(Brand brand) {
        brands.remove(brand);
        brand.setMerchant(null);
    }
}
