package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.constant.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "order_items")
@NoArgsConstructor
@Getter
@Setter
public class OrderItem extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deal_id")
    @NotNull
    @JsonIgnoreProperties({"order_items", "hibernateLazyInitializer"})
    private Deal deal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    @NotNull
    @JsonIgnoreProperties({"order_items", "hibernateLazyInitializer"})
    private Order order;

    @Column(name = "spot_price")
    @JsonProperty("spot_price")
    private Integer spotPrice;

    @Column(name = "redeemed_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("redeemed_at")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date redeemedAt;

    public OrderItem(@NotNull Deal deal, @NotNull Order order) {
        this.deal = deal;
        this.order = order;
        this.spotPrice = deal.getDiscountedPrice();
    }
}
