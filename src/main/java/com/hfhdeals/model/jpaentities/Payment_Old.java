/*package com.hfhdeals.model.jpaentities;

import com.hfhdeals.constant.Constants;
import com.hfhdeals.model.pojos.ChargeRequest;
import com.hfhdeals.model.pojos.CreditCard;
import flexjson.JSON;
import flexjson.JSONDeserializer;
import org.json.JSONException;
import org.json.JSONObject;

public class Payment_Old {

    CreditCard creditcard_details;
    ChargeRequest chargerequest_details;
    Order order_details;

    public Payment_Old() {
    }

    public Payment_Old(CreditCard creditCardDetails, ChargeRequest chargeRequestDetails, Order orderDetails) {
        this.creditcard_details = creditCardDetails;
        this.chargerequest_details = chargeRequestDetails;
        this.order_details = orderDetails;
    }

    @JSON(name = "creditcard_details")
    public CreditCard getCreditcard_details() {
        return creditcard_details;
    }

    @JSON(name = "chargerequest_details")
    public ChargeRequest getChargerequest_details() {
        return chargerequest_details;
    }

    @JSON(name = "order_details")
    public Order getOrder_details() {
        return order_details;
    }

    public void setCreditcard_details(CreditCard creditcard_details) {
        this.creditcard_details = creditcard_details;
    }

    public void setChargerequest_details(ChargeRequest chargerequest_details) {
        this.chargerequest_details = chargerequest_details;
    }

    public void setOrder_details(Order order_details) {
        this.order_details = order_details;
    }

    @Override
    public String toString() {
        return "Payment{" + "creditcard_details=" + creditcard_details + ", chargerequest_details=" + chargerequest_details + ", order_details=" + order_details
                + '}';
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject(Constants.getJSONSerializer().serialize(this));
    }

    public static Payment_Old fromJson(JSONObject json) {
        return new JSONDeserializer<Payment_Old>().deserialize(json + "", Payment_Old.class);
    }
}
*/