package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.constant.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "deals")
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties({"order_items"})
public class Deal extends BaseEntity {
    private String title;

    private String description;

    @Column(name = "original_price")
    @JsonProperty("original_price")
    private Integer originalPrice;

    @Column(name = "discounted_price")
    @JsonProperty("discounted_price")
    private Integer discountedPrice;

    @Column(name = "terms_of_use")
    @JsonProperty("terms_of_use")
    private String termsOfUse;

    @Column(name = "redemption_instructions")
    @JsonProperty("redemption_instructions")
    private String redemptionInstructions;

    @Column(name = "max_quantity")
    @JsonProperty("max_quantity")
    private Integer maxQuantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "outlet_id")
    @JsonIgnoreProperties({"deals", "hibernateLazyInitializer"})
    private Outlet outlet;

    @Column(name = "deal_of_the_month")
    @JsonProperty("deal_of_the_month")
    private Boolean dealOfTheMonth;

    @OneToMany(
            mappedBy = "deal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("category_deals")
    @JsonIgnoreProperties({"deal", "hibernateLazyInitializer"})
    private List<CategoryDeal> categoryDeals = new ArrayList<>();

    @OneToMany(
            mappedBy = "deal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("cuisine_deals")
    @JsonIgnoreProperties({"deal"})
    private List<CuisineDeal> cuisineDeals = new ArrayList<>();

    @OneToMany(
            mappedBy = "deal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("deal_photos")
    @JsonIgnoreProperties({"deal"})
    private List<DealPhoto> dealPhotos = new ArrayList<>();

    @OneToMany(
            mappedBy = "deal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("order_items")
    @JsonIgnoreProperties({"deal"})
    private List<OrderItem> orderItems = new ArrayList<>();

    @Column(name = "auto_publish_date")
    @JsonProperty("auto_publish_date")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    private Date autoPublishDate;

    @Column(name = "published_at")
    @JsonProperty("published_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    private Date publishedAt;

    @Column(name = "redemption_expiry_date")
    @JsonProperty("redemption_expiry_date")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    private Date redemptionExpiryDate;

    @Column(name = "archived_at")
    @JsonProperty("archived_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    private Date archivedAt;

    @JsonProperty("hit_count")
    @Transient
    private Integer hitCount;

    @JsonProperty("num_bought")
    @Transient
    private Integer numBought;

    public void addPhoto(Photo photo) {
        DealPhoto dealPhoto = new DealPhoto();
        dealPhoto.setDeal(this);
        dealPhoto.setPhoto(photo);

        dealPhotos.add(dealPhoto);
        photo.getDealPhotos().add(dealPhoto);
    }

    public void removePhoto(Photo photo) {
        for (DealPhoto dealPhoto : dealPhotos) {
            Integer huntedDealId = getId();
            Integer huntedPhotoId = photo.getId();

            if (dealPhoto.getDeal().getId().equals(huntedDealId) && dealPhoto.getPhoto().getId().equals(huntedPhotoId)) {
                dealPhotos.remove(dealPhoto);
                dealPhoto.setDeal(null);
                dealPhoto.setPhoto(null);
                break;
            }
        }
    }
}
