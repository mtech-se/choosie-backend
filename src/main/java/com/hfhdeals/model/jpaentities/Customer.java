package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customers")
@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends User {
    @Column(name = "billing_first_name")
    @JsonProperty("billing_first_name")
    private String billingFirstName;

    @Column(name = "billing_last_name")
    @JsonProperty("billing_last_name")
    private String billingLastName;

    @Column(name = "billing_cellphone")
    @JsonProperty("billing_cellphone")
    private String billingCellphone;

    @Column(name = "billing_telephone")
    @JsonProperty("billing_telephone")
    private String billingTelephone;

    @Column(name = "billing_add_line_one")
    @JsonProperty("billing_add_line_one")
    private String billingAddLineOne;

    @Column(name = "billing_add_line_two")
    @JsonProperty("billing_add_line_two")
    private String billingAddLineTwo;

    @Column(name = "billing_add_postal_code")
    @JsonProperty("billing_add_postal_code")
    private String billingAddPostalCode;

    @OneToMany(
            mappedBy = "customer",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Order> orders = new ArrayList<>();

    public void addOrder(Order order) {
        orders.add(order);
        order.setCustomer(this);
    }

    public void removeOrder(Order order) {
        orders.remove(order);
        order.setCustomer(null);
    }
}
