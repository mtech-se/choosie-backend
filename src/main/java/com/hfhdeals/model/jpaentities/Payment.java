package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.constant.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "payments")
@NoArgsConstructor
@Getter
@Setter
public class Payment extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    @NotNull
    @JsonIgnoreProperties({"payments", "order_items", "hibernateLazyInitializer"})
    private Order order;

    @Column(name = "receipt_amount")
    @JsonProperty("receipt_amount")
    @NotNull
    private Integer receiptAmount;

    @Column(name = "spot_billing_address")
    @JsonProperty("spot_billing_address")
    @NotNull
    private String spotBillingAddress;

    @Column(name = "completed_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("completed_at")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date completedAt;
}
