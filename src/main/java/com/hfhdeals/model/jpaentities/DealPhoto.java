package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "deal_photo")
@NoArgsConstructor
@Getter
@Setter
public class DealPhoto extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deal_id")
    @JsonIgnore
    private Deal deal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id")
    @JsonIgnoreProperties({"categoryPhotos", "brandPhotos", "dealPhotos", "hibernateLazyInitializer"})
    private Photo photo;
}
