package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "brands")
@NoArgsConstructor
@Getter
@Setter
public class Brand extends BaseEntity {
    @Column(name = "name")
    @NotNull
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    @NotNull
    @JsonIgnoreProperties({"brands", "contact_persons", "hibernateLazyInitializer"})
    private Merchant merchant;

    @OneToMany(
            mappedBy = "brand",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties({"brand", "deals"})
    private List<Outlet> outlets = new ArrayList<>();

    @OneToMany(
            mappedBy = "brand",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("brand_photos")
    @JsonIgnoreProperties({"brand"})    // ignore "brand" cos not meaningful
    private List<BrandPhoto> brandPhotos = new ArrayList<>();

    public void addOutlet(Outlet outlet) {
        outlets.add(outlet);
        outlet.setBrand(this);
    }

    public void removeOutlet(Outlet outlet) {
        outlets.remove(outlet);
        outlet.setBrand(null);
    }

    public void addPhoto(Photo photo) {
        BrandPhoto brandPhoto = new BrandPhoto();
        brandPhoto.setBrand(this);
        brandPhoto.setPhoto(photo);

        brandPhotos.add(brandPhoto);
        photo.getBrandPhotos().add(brandPhoto);
    }

    public void removePhoto(Photo photo) {
        for (BrandPhoto brandPhoto : brandPhotos) {
            Integer huntedBrandId = getId();
            Integer huntedPhotoId = photo.getId();

            if (brandPhoto.getBrand().getId().equals(huntedBrandId) && brandPhoto.getPhoto().getId().equals(huntedPhotoId)) {
                brandPhotos.remove(brandPhoto);
                brandPhoto.setBrand(null);
                brandPhoto.setPhoto(null);
                break;
            }
        }
    }
}
