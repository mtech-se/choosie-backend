package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Role extends BaseEntity {
    @Column(name = "name")
    @JsonProperty("name")
    private String name;
}
