//package com.hfhdeals.model.jpaentities;
//
//import com.hfhdeals.constant.Constants;
//import flexjson.JSON;
//import org.json.JSONObject;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "checkoutdetails")
//public class CheckoutDetails {
//    private int checkout_id;
//    private String first_name;
//    private String last_name;
//    private String address; // can be line 1 and line 2
//    private String country;
//    private String city;
//    private int postal_code; // can be string
//    private String phone;
//    private int user_id; // Join column
//
//    public CheckoutDetails() {
//    }
//
//    public CheckoutDetails(int checkout_id, String first_name, String last_name, String address, String country, String city, int postal_code,
//                           String phone, int user_id
//    ) {
//        this.checkout_id = checkout_id;
//        this.first_name = first_name;
//        this.last_name = last_name;
//        this.address = address;
//        this.country = country;
//        this.city = city;
//        this.postal_code = postal_code;
//        this.phone = phone;
//        this.user_id = user_id;
//    }
//
//    public CheckoutDetails(String first_name, String last_name, String address, String country, String city, int postal_code, String phone,
//                           int user_id
//    ) {
//
//        this.first_name = first_name;
//        this.last_name = last_name;
//        this.address = address;
//        this.country = country;
//        this.city = city;
//        this.postal_code = postal_code;
//        this.phone = phone;
//        this.user_id = user_id;
//    }
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "checkout_id", unique = true, nullable = false)
//    public int getCheckout_id() {
//        return checkout_id;
//    }
//
//    @Column(name = "first_name")
//    @JSON(name = "first_name")
//    public String getFirst_name() {
//        return first_name;
//    }
//
//    @Column(name = "last_name")
//    @JSON(name = "last_name")
//    public String getLast_name() {
//        return last_name;
//    }
//
//    @Column(name = "address")
//    @JSON(name = "address")
//    public String getAddress() {
//        return address;
//    }
//
//    @Column(name = "country")
//    @JSON(name = "country")
//    public String getCountry() {
//        return country;
//    }
//
//    @Column(name = "city")
//    @JSON(name = "city")
//    public String getCity() {
//        return city;
//    }
//
//    @Column(name = "postal_code")
//    @JSON(name = "postal_code")
//    public int getPostal_code() {
//        return postal_code;
//    }
//
//    @Column(name = "phone")
//    @JSON(name = "phone")
//    public String getPhone() {
//        return phone;
//    }
//
//    @Column(name = "user_id")
//    @JSON(name = "user_id")
//    public int getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(int user_id) {
//        this.user_id = user_id;
//    }
//
//    public void setCheckout_id(int checkout_id) {
//        this.checkout_id = checkout_id;
//    }
//
//    public void setFirst_name(String first_name) {
//        this.first_name = first_name;
//    }
//
//    public void setLast_name(String last_name) {
//        this.last_name = last_name;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public void setPostal_code(int postal_code) {
//        this.postal_code = postal_code;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public JSONObject toJson() {
//        return new JSONObject(Constants.getJSONSerializer().serialize(this));
//    }
//}
