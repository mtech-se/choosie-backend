package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "admins")
@Getter
@Setter
@ToString(callSuper = true)
public class Admin extends User {

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "admin",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("admin_roles")
    @JsonIgnoreProperties({"admin"})
    private List<AdminRole> adminRoles = new ArrayList<>();

}
