package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cuisine_deal")
@NoArgsConstructor
@Getter
@Setter
public class CuisineDeal extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cuisine_id")
    @JsonIgnoreProperties({"cuisine_deals", "hibernateLazyInitializer"})
    private Cuisine cuisine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deal_id")
    @JsonIgnoreProperties({"cuisine_deals", "hibernateLazyInitializer"})
    private Deal deal;
}
