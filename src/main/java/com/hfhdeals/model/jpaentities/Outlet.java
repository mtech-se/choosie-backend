package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "outlets")
@NoArgsConstructor
@Getter
@Setter
public class Outlet extends BaseEntity {
    private String name;
    private String address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brand_id")
    @JsonIgnoreProperties({"outlets", "hibernateLazyInitializer"})
    private Brand brand;

    @OneToMany(
            mappedBy = "outlet",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Deal> deals = new ArrayList<>();

    public void addDeal(Deal deal) {
        deals.add(deal);
        deal.setOutlet(this);
    }

    public void removeDeal(Deal deal) {
        deals.remove(deal);
        deal.setOutlet(null);
    }
}
