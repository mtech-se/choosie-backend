package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "categories")
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties({"category_deals"})
public class Category extends BaseEntity {

    private String name;
    private String description;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("category_deals")
    @JsonIgnoreProperties({"category"})
    private List<CategoryDeal> categoryDeals = new ArrayList<>();

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("category_photos")
    @JsonIgnoreProperties({"category"}) // ignore "category" cos not meaningful
    private List<CategoryPhoto> categoryPhotos = new ArrayList<>();

    public void addPhoto(Photo photo) {
        CategoryPhoto categoryPhoto = new CategoryPhoto();
        categoryPhoto.setCategory(this);
        categoryPhoto.setPhoto(photo);

        categoryPhotos.add(categoryPhoto);
        photo.getCategoryPhotos().add(categoryPhoto);
    }

    public void removePhoto(Photo photo) {
        // loop through all category-photos of this category
        // find for a category-photo which has categoryId equalling this and photoId equalling photo's
        // when found, remove it from this category's category-photos
        // for it, also set its category and photo to null

        for (CategoryPhoto categoryPhoto : categoryPhotos) {
            Integer huntedCategoryId = getId();
            Integer huntedPhotoId = photo.getId();

            if (categoryPhoto.getCategory().getId().equals(huntedCategoryId) && categoryPhoto.getPhoto().getId().equals(huntedPhotoId)) {
                categoryPhotos.remove(categoryPhoto);
                categoryPhoto.setCategory(null);
                categoryPhoto.setPhoto(null);
                break;
            }
        }
    }

    public void addDeal(Deal deal) {
        CategoryDeal categoryDeal = new CategoryDeal();
        categoryDeal.setCategory(this);
        categoryDeal.setDeal(deal);

        categoryDeals.add(categoryDeal);
        deal.getCategoryDeals().add(categoryDeal);
    }

    public void removeDeal(Deal dealToBeRemoved) {
        for (Iterator<CategoryDeal> iterator = categoryDeals.iterator(); iterator.hasNext(); ) {
            CategoryDeal categoryDeal = iterator.next();

            Deal thisCurrentDeal = categoryDeal.getDeal();
            Hibernate.unproxy(thisCurrentDeal);
            Category thisCurrentCategory = categoryDeal.getCategory();

            boolean isTheDeal = thisCurrentDeal.getId().equals(dealToBeRemoved.getId());
            boolean isTheCategory = thisCurrentCategory.getId().equals(this.getId());

            if (isTheDeal && isTheCategory) {
                iterator.remove();
                categoryDeal.getCategory().getCategoryDeals().remove(categoryDeal);
                categoryDeal.getDeal().getCategoryDeals().remove(categoryDeal);
                categoryDeal.setDeal(null);
                categoryDeal.setCategory(null);
            }
        }
    }
}
