package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "category_deal")
@NoArgsConstructor
@Getter
@Setter
public class CategoryDeal extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    @JsonIgnoreProperties({"category_deals", "hibernateLazyInitializer"})
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deal_id")
    @JsonIgnoreProperties({"category_deals", "hibernateLazyInitializer"})
    private Deal deal;

    @Override
    public String toString() {
        return "CategoryDeal{" +
                "id=" + getId() +
                ", categoryId=" + category.getId() +
                '}';
    }
}
