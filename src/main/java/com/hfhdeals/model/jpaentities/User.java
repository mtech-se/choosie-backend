package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@JsonIgnoreProperties(value = {"password"}, allowSetters = true)
abstract public class User extends BaseEntity {
    @Column(name = "email", unique = true, nullable = false)
    @JsonProperty("email")
    @NotNull
    @NotEmpty
    @Email
    private String email;

    @Column(name = "first_name")
    @JsonProperty("first_name")
    @NotNull
    @NotEmpty
    private String firstName;

    @Column(name = "last_name")
    @JsonProperty("last_name")
    @NotNull
    @NotEmpty
    private String lastName;

    @Column(name = "gender")
    @JsonProperty("gender")
    @NotNull
    @NotEmpty
    private String gender;

    @Column(name = "password")
    @NotNull
    @NotEmpty
    private String password;
}
