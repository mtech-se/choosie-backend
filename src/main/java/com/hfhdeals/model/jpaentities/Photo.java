package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "photos")
@NoArgsConstructor
@Getter
@Setter
public class Photo extends BaseEntity {
    @Column(name = "path")
    private String path;

    @OneToMany(
            mappedBy = "photo",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<CategoryPhoto> categoryPhotos = new ArrayList<>();

    @OneToMany(
            mappedBy = "photo",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<BrandPhoto> brandPhotos = new ArrayList<>();

    @OneToMany(
            mappedBy = "photo",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<DealPhoto> dealPhotos = new ArrayList<>();
}