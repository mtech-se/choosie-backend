package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.constant.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.JSONObject;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"id", "created_at", "updated_at", "deleted_at"}, allowGetters = true)
@Getter
@Setter
@ToString
//@JsonPropertyOrder(alphabetic = true)
public abstract class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "created_at", nullable = false)
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    protected Date createdAt;

    @Column(name = "updated_at", nullable = false)
    @JsonProperty("updated_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    protected Date updatedAt;

    @Column(name = "deleted_at")
    @JsonProperty("deleted_at")
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date deletedAt;

    public JSONObject toJson() {
        return new JSONObject(Constants.getJSONSerializer().serialize(this));
    }
}
