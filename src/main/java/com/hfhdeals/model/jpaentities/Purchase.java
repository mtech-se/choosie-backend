//package com.hfhdeals.model.jpaentities;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//import flexjson.JSON;
//import lombok.*;
//
//import javax.persistence.*;
//import java.util.Date;
//
//@Entity
//@Table(name = "purchases")
//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
//@ToString(callSuper = true)
//public class Purchase extends BaseEntity {
//    @Column(name = "redeemed_at")
//    @JSON(name = "redeemed_at")
//    @JsonProperty("redeemed_at")
//    private Date redeemedAt;
//
//    @Column(name = "paid_merchant_at")
//    @JSON(name = "paid_merchant_at")
//    @JsonProperty("paid_merchant_at")
//    private Date paidMerchantAt;
//
//    @ManyToOne
//    @JoinColumn(name = "deal_id")
//    private Deal deal;
//}
