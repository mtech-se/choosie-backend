package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonProperty;
import flexjson.JSON;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "deal_customer_clicks")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class DealCustomerClick extends BaseEntity {
    @Column(name = "deal_id")
    @JSON(name = "deal_id")
    @JsonProperty("deal_id")
    private int dealId;

    @Column(name = "customer_id")
    @JSON(name = "customer_id")
    @JsonProperty("customer_id")
    private int customerId;
}
