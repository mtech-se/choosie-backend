package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "cuisines")
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties({"cuisine_deals"})
public class Cuisine extends BaseEntity {
    @Column(name = "name", nullable = false)
    @NotNull
    @NotBlank
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(
            mappedBy = "cuisine",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonProperty("cuisine_deals")
    @JsonIgnoreProperties({"cuisine", "deal"})
    private List<CuisineDeal> cuisineDeals = new ArrayList<>();

    public void addDeal(Deal deal) {
        CuisineDeal cuisineDeal = new CuisineDeal();
        cuisineDeal.setCuisine(this);
        cuisineDeal.setDeal(deal);

        cuisineDeals.add(cuisineDeal);
        deal.getCuisineDeals().add(cuisineDeal);
    }

    public void removeDeal(Deal dealToBeRemoved) {
        for (Iterator<CuisineDeal> iterator = cuisineDeals.iterator(); iterator.hasNext(); ) {
            CuisineDeal cuisineDeal = iterator.next();

            Deal thisCurrentDeal = cuisineDeal.getDeal();
            Hibernate.unproxy(thisCurrentDeal);
            Cuisine thisCurrentCuisine = cuisineDeal.getCuisine();

            boolean isTheDeal = thisCurrentDeal.getId().equals(dealToBeRemoved.getId());
            boolean isTheCuisine = thisCurrentCuisine.getId().equals(this.getId());

            if (isTheDeal && isTheCuisine) {
                iterator.remove();
                cuisineDeal.getCuisine().getCuisineDeals().remove(cuisineDeal);
                cuisineDeal.setDeal(null);
                cuisineDeal.setCuisine(null);
            }
        }
    }
}
