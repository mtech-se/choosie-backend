package com.hfhdeals.model.jpaentities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contact_persons")
@NoArgsConstructor
@Getter
@Setter
public class ContactPerson extends BaseEntity {
    @Column(name = "name", nullable = false)
    @NotNull
    @NotBlank
    private String name;

    @Column(name = "position", nullable = false)
    @NotNull
    @NotBlank
    private String position;

    @Column(name = "mobile_number", nullable = false)
    @NotNull
    @NotBlank
    @JsonProperty("mobile_number")
    private String mobileNumber;

    @Column(name = "email", nullable = false)
    @NotNull
    @NotBlank
    @Email
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    @JsonIgnoreProperties({"contact_persons", "brands", "hibernateLazyInitializer"})
    private Merchant merchant;
}
