SELECT merchants.id,
       merchants.name,
       merchants.business_registration_number,
       merchants.mailing_address,
       merchants.deleted_at,
       COUNT(*) as num_purchases
FROM purchases
       INNER JOIN deals on purchases.deal_id = deals.id
       INNER JOIN outlets on deals.outlet_id = outlets.id
       INNER JOIN brands on outlets.brand_id = brands.id
       INNER JOIN merchants on brands.merchant_id = merchants.id
WHERE purchases.created_at BETWEEN '2017-01-01 00:00:00' AND '2018-12-04 00:00:00'
  AND merchants.deleted_at IS NULL
GROUP BY merchants.id
ORDER BY num_purchases DESC
LIMIT 5;
