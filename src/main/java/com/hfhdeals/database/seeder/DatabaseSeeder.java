package com.hfhdeals.database.seeder;

import com.hfhdeals.database.dao.DatabasePopulateDao;
import com.hfhdeals.database.seeder.tableseeder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class DatabaseSeeder {
    private final DatabasePopulateDao databasePopulateDao;

    // tables with no foreign keys
    private final CuisinesTableSeeder cuisinesTableSeeder;
    private final CountriesTableSeeder countriesTableSeeder;
    private final CategoriesTableSeeder categoriesTableSeeder;
    private final MerchantsTableSeeder merchantsTableSeeder;
    private final CustomersTableSeeder customersTableSeeder;
    private final AdminsTableSeeder adminsTableSeeder;

    // tables with 1 foreign key
    private final LocationsTableSeeder locationsTableSeeder;
    private final ContactPersonsTableSeeder contactPersonsTableSeeder;
    private final BrandsTableSeeder brandsTableSeeder;
    private final OutletsTableSeeder outletsTableSeeder;
    private final OrdersTableSeeder ordersTableSeeder;

    // tables with multiple foreign keys
    private final DealsTableSeeder dealsTableSeeder;
    private final DealClickByUserTableSeeder dealCustomerClickTableSeeder;
    private final PhotosTableSeeder photosTableSeeder;
    private final CategoryDealTableSeeder categoryDealTableSeeder;
    private final CuisineDealTableSeeder cuisineDealTableSeeder;
    private final OrderItemsTableSeeder orderItemsTableSeeder;

    private List<String> seeded = new ArrayList<>();
    private List<String> notSeeded = new ArrayList<>();
    private Map<String, Long> tableRows;

    @Autowired
    public DatabaseSeeder(DatabasePopulateDao databasePopulateDao, CuisinesTableSeeder cuisinesTableSeeder, CountriesTableSeeder countriesTableSeeder, CategoriesTableSeeder categoriesTableSeeder, MerchantsTableSeeder merchantsTableSeeder, CustomersTableSeeder customersTableSeeder, AdminsTableSeeder adminsTableSeeder, LocationsTableSeeder locationsTableSeeder, ContactPersonsTableSeeder contactPersonsTableSeeder, BrandsTableSeeder brandsTableSeeder, OutletsTableSeeder outletsTableSeeder, OrdersTableSeeder ordersTableSeeder, DealsTableSeeder dealsTableSeeder, DealClickByUserTableSeeder dealCustomerClickTableSeeder, PhotosTableSeeder photosTableSeeder, CategoryDealTableSeeder categoryDealTableSeeder, CuisineDealTableSeeder cuisineDealTableSeeder, OrderItemsTableSeeder orderItemsTableSeeder) {
        this.databasePopulateDao = databasePopulateDao;

        // tables with no foreign keys
        this.cuisinesTableSeeder = cuisinesTableSeeder;
        this.countriesTableSeeder = countriesTableSeeder;
        this.categoriesTableSeeder = categoriesTableSeeder;
        this.merchantsTableSeeder = merchantsTableSeeder;
        this.customersTableSeeder = customersTableSeeder;
        this.adminsTableSeeder = adminsTableSeeder;

        // tables with 1 foreign key
        this.locationsTableSeeder = locationsTableSeeder;
        this.contactPersonsTableSeeder = contactPersonsTableSeeder;
        this.brandsTableSeeder = brandsTableSeeder;
        this.outletsTableSeeder = outletsTableSeeder;
        this.ordersTableSeeder = ordersTableSeeder;

        // tables with multiple foreign keys
        this.dealsTableSeeder = dealsTableSeeder;
        this.dealCustomerClickTableSeeder = dealCustomerClickTableSeeder;
        this.photosTableSeeder = photosTableSeeder;
        this.categoryDealTableSeeder = categoryDealTableSeeder;
        this.cuisineDealTableSeeder = cuisineDealTableSeeder;
        this.orderItemsTableSeeder = orderItemsTableSeeder;
    }

    @Transactional
    public void run() {
        tableRows = databasePopulateDao.getCounts();

        // tables with no foreign keys
        runForModel("Cuisine", cuisinesTableSeeder);
        runForModel("Country", countriesTableSeeder);
        runForModel("Category", categoriesTableSeeder);
        runForModel("Merchant", merchantsTableSeeder);
        runForModel("Customer", customersTableSeeder);
        runForModel("Admin", adminsTableSeeder);

        // tables with 1 foreign key
        runForModel("Location", locationsTableSeeder);
        runForModel("ContactPerson", contactPersonsTableSeeder);
        runForModel("Brand", brandsTableSeeder);
        runForModel("Outlet", outletsTableSeeder);
        runForModel("Order", ordersTableSeeder);

        // tables with multiple foreign keys
        runForModel("Deal", dealsTableSeeder);
        runForModel("DealCustomerClick", dealCustomerClickTableSeeder);
        runForModel("Photo", photosTableSeeder);
        runForModel("CategoryDeal", categoryDealTableSeeder);
        runForModel("CuisineDeal", cuisineDealTableSeeder);
        runForModel("OrderItem", orderItemsTableSeeder);
    }

    private void runForModel(String modelName, TableSeeder tableSeeder) {
        if (tableRows.get(modelName) < 1 || modelName.equals("Admin")) {
            System.out.println("Running for: " + modelName);

            Date before = new Date();
            tableSeeder.run();
            seeded.add(modelName);
            Date after = new Date();

            long timeInMilliseconds = after.getTime() - before.getTime();
            String message = timeInMilliseconds + " ms";

            if (timeInMilliseconds > 1000) {
                System.out.println(message);
            }
        } else {
            notSeeded.add(modelName);
        }
    }

    public String getSeeded() {
        String collect = String.join(",", seeded);
        System.out.println(collect);
        return collect;
    }

    public String getNotSeeded() {
        String collect = String.join(",", notSeeded);
        System.out.println(collect);
        return collect;
    }
}
