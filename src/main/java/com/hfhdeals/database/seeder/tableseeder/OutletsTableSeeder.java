package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.OutletFaker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.model.jpaentities.Outlet;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.LocationService;
import com.hfhdeals.service.baseservice.OutletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OutletsTableSeeder implements TableSeeder {
    @Autowired
    BrandService brandService;

    @Autowired
    OutletService outletService;

    @Autowired
    OutletFaker outletFaker;

    @Autowired
    LocationService locationService;

    Faker faker = new Faker();

    @Override
    public void run() {
        List<Brand> brandList = brandService.getAll();
        List<Location> locationList = locationService.getAll();
        //Total number of outlets is twice of the brands, but not twice each.
        for (int i = 0; i < brandList.size() * 2; i++) {
            Outlet outlet = outletFaker.make(locationList.get(faker.random().nextInt(0, locationList.size() - 1)),
                    brandList.get(faker.random().nextInt(0, brandList.size() - 1)));
            outletService.save(outlet);
        }
    }
}
