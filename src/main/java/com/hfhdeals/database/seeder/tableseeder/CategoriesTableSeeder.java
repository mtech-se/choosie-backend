package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.faker.CategoryFaker;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.service.baseservice.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoriesTableSeeder implements TableSeeder {

    @Autowired
    CategoryFaker categoryFaker;

    @Autowired
    CategoryService categoryService;

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Category category = categoryFaker.make();
            categoryService.save(category);
        }
    }
}
