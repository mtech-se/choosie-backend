package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.DealClickFaker;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.DealCustomerClick;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.service.baseservice.DealCustomerClickService;
import com.hfhdeals.service.baseservice.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DealClickByUserTableSeeder implements TableSeeder {
    private final DealClickFaker dealClickFaker;
    private final DealService dealService;
    private final DealCustomerClickService dealCustomerClickService;
    private final CustomerService userService;
    private Faker faker = new Faker();

    @Autowired
    public DealClickByUserTableSeeder(DealClickFaker dealClickFaker, DealService dealService, DealCustomerClickService dealCustomerClickService, CustomerService userService) {
        this.dealClickFaker = dealClickFaker;
        this.dealService = dealService;
        this.dealCustomerClickService = dealCustomerClickService;
        this.userService = userService;
    }

    @Override
    public void run() {
        List<Customer> allCustomers = userService.getAll();
        List<Deal> allDeals = dealService.getAll();

        for (Deal deal : allDeals) {
            // 75% chance that this deal will be clicked by some users.
            if (faker.random().nextBoolean() || faker.random().nextBoolean()) {

                // Get some unique users who would click this particular deal.
                Set<Integer> uniqueUserIds = new HashSet<>();
                for (int i = 0; i < faker.random().nextInt(0, allCustomers.size() * 2); i++) {
                    uniqueUserIds.add(allCustomers.get(faker.random().nextInt(0, allCustomers.size() - 1)).getId());
                }

                for (int userId : uniqueUserIds) {
                    DealCustomerClick dealCustomerClick = dealClickFaker.make(deal.getId(), userId);
                    dealCustomerClickService.save(dealCustomerClick);
                }
            }
        }
    }

}
