package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.LocationFaker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.model.jpaentities.Location;
import com.hfhdeals.service.baseservice.CountryService;
import com.hfhdeals.service.baseservice.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LocationsTableSeeder implements TableSeeder {
    private final LocationService locationService;
    private final LocationFaker locationFaker;
    private final CountryService countryService;
    private Faker faker = new Faker();

    @Autowired
    public LocationsTableSeeder(LocationService locationService, LocationFaker locationFaker, CountryService countryService) {
        this.locationService = locationService;
        this.locationFaker = locationFaker;
        this.countryService = countryService;
    }

    @Override
    public void run() {
        // for each country already in database, add 2-5 locations
        List<Country> countries = countryService.getAll();

        for (Country country : countries) {
            int numRecords = faker.number().numberBetween(2, 5);
            for (int i = 0; i < numRecords; i++) {
                Location location = locationFaker.make(country);
                locationService.save(location);
            }
        }
    }
}
