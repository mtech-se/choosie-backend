package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.model.jpaentities.OrderItem;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.service.baseservice.OrderItemService;
import com.hfhdeals.service.baseservice.OrderService;
import com.hfhdeals.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class OrderItemsTableSeeder implements TableSeeder {

    private final OrderService orderService;
    private final DealService dealService;
    private final OrderItemService orderItemService;
    private Faker faker = new Faker();
    private List<Deal> deals;

    @Autowired
    public OrderItemsTableSeeder(OrderService orderService, DealService dealService, OrderItemService orderItemService) {
        this.orderService = orderService;
        this.dealService = dealService;
        this.orderItemService = orderItemService;
    }

    @Override
    public void run() {
        // for each order that's already in the database,
        // skip deals who have null published_at
        // pick a 1-5 deals from deals already in database
        // and create 1-3 order_item with this order and each of those deals
        // and make the order_item's created_at dates be 1-3 days after the deal's published_at date
        // and 50% chance the OI has redeemed_at set (i.e. already redeemed)

        deals = dealService.getAllPublishedDeals();
        List<Order> orders = orderService.getAll();

        for (Order order : orders) {
            List<Deal> pickedDeals = pickDeals();
            for (Deal pickedDeal : pickedDeals) {
                int numOrderItemsToSeed = faker.random().nextInt(1, 3);
                for (int i = 0; i < numOrderItemsToSeed; i++) {
                    OrderItem orderItem = new OrderItem(pickedDeal, order);
                    orderItemService.save(orderItem);

                    Date publishedAt = pickedDeal.getPublishedAt();
                    int numDaysAfter = faker.random().nextInt(1, 3);
                    Date createdAt = DateUtils.addDays(publishedAt, numDaysAfter);
                    orderItem.setCreatedAt(createdAt);

                    if (faker.bool().bool()) orderItem.setRedeemedAt(new Date());
                }
            }
        }
    }

    private List<Deal> pickDeals() {
        List<Deal> pickedDeals = new ArrayList<>();

        int numDealsToPick = faker.random().nextInt(1, 5);
        for (int i = 0; i < numDealsToPick; i++) {
            Deal deal = deals.get(faker.random().nextInt(0, deals.size() - 1));
            pickedDeals.add(deal);
        }

        return pickedDeals;
    }

}
