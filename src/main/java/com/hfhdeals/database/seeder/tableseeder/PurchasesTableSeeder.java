//package com.hfhdeals.database.seeder.tableseeder;
//
//import com.hfhdeals.faker.PurchaseFaker;
//import com.hfhdeals.model.jpaentities.Deal;
//import com.hfhdeals.model.jpaentities.Purchase;
//import com.hfhdeals.service.baseservice.DealService;
//import com.hfhdeals.service.baseservice.PurchaseService;
//import com.hfhdeals.utils.CustomFaker;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//import java.util.List;
//
//@Component
//public class PurchasesTableSeeder implements TableSeeder {
//    @Autowired
//    DealService dealService;
//
//    @Autowired
//    PurchaseService purchaseService;
//
//    @Autowired
//    PurchaseFaker purchaseFaker;
//
//    private final CustomFaker customFaker = new CustomFaker();
//
//    @Override
//    public void run() {
//        // loop through all existing deals
//        // for each deal, it has a 20% chance of having no purchases
//        // when a deal does have purchases, it'll have anywhere between 1-10 purchases
//
//        // purchases need to have sensible dates:
//        // - deal's published_at must be not null (how could he have bought it if the deal wasn't published?)
//        // - purchase's created_at must be before deal's redemption_expiry_date (how could he have bought it if it has already expired?)
//        //   and after deal's published_at
//
//        // redeemed_at: 20% chance - between purchase.created_at and now
//        // redeemed_at: 80% chance - null
//
//        // paid_merchant_at: 20% chance - must be between purchase.createdAt and now
//        // paid_merchant_at: 80% chance - null
//
//        List<Deal> deals = dealService.getAll();
//
//        for (Deal deal : deals) {
//            if (customFaker.bool(80)) {
//                if (deal.getPublishedAt() == null) continue;
//
//                int numPurchasesToCreate = customFaker.number().numberBetween(1, 10);
//
//                for (int i = 0; i < numPurchasesToCreate; i++) {
//                    Date createdAt = customFaker.date().between(deal.getPublishedAt(), deal.getRedemptionExpiryDate());
//                    Date redeemedAt = customFaker.bool(20) ? customFaker.date().between(createdAt, new Date()) : null;
//                    Date paidMerchantAt = customFaker.bool(20) ? customFaker.date().between(createdAt, new Date()) : null;
//
//                    Purchase purchase = new Purchase();
//                    purchase.setDeal(deal);
//                    purchase.setCreatedAt(createdAt);
//                    purchase.setRedeemedAt(redeemedAt);
//                    purchase.setPaidMerchantAt(paidMerchantAt);
//                    purchaseService.save(purchase);
//                }
//            }
//        }
//    }
//}