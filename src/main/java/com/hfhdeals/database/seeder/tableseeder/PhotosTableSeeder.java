package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.Photo;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.service.baseservice.PhotoService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class PhotosTableSeeder implements TableSeeder {
    private final CategoryService categoryService;
    private final BrandService brandService;
    private final DealService dealService;
    private final PhotoService photoService;
    List<String> allSamplePictures = new ArrayList<String>();
    Faker faker = new Faker();

    @Value("${upload.path}")
    private String uploadDirectory;

    @Autowired
    public PhotosTableSeeder(CategoryService categoryService, BrandService brandService, DealService dealService, PhotoService photoService) {
        this.categoryService = categoryService;
        this.brandService = brandService;
        this.dealService = dealService;
        this.photoService = photoService;
    }

    @Override
    public void run() {
        if (new File(uploadDirectory).exists())
            try {
                FileUtils.forceDelete(new File(uploadDirectory));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        new File(uploadDirectory).mkdirs();
        // this photos table seeder should seed photos for all entities needing photos
        // deals, categories, brands need photos right now
        // merchants, outlets, customers, admins may need photos in future
        // for each entity, we need a joining table e.g. deal_photo, customer_photo

        // for this seeder, when seeding a photo for an entity,
        // this seeder needs to create a record inside both the photos table
        // as well as the related joining table for the entity

        File[] files = new File("sample_images").listFiles();
        for (File file : files) {
            if (file.isFile()) {
                allSamplePictures.add(file.getName());
            }
        }
        try {
            runForCategoriesTable();
            runForBrandsTable();
            runForDealsTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void runForCategoriesTable() throws IOException {
        System.out.println("Seeding photos for categories...");
        // each category to have exactly 1 photo

        List<Category> categories = categoryService.getAll();
        for (Category category : categories) {
            Photo photo = new Photo();
            photo.setPath("/categories/" + category.getId() + "/1.png");
            photoService.savePhotoAndFile(photo, getRandomPicture());

            category.addPhoto(photo);

            // we need to call something on the collection
            // to force JPA to save the records in the joining table,
            // not doing so will lead to JPA double-inserting
            // cos it also inserts for the other side (the photo side)
            assert category.getCategoryPhotos().size() == 1;
        }
    }

    private void runForBrandsTable() throws IOException {
        System.out.println("Seeding photos for brands...");
        // each brand to have exactly 1 photo

        List<Brand> brands = brandService.getAll();
        for (Brand brand : brands) {
            Photo photo = new Photo();
            photo.setPath("/brands/" + brand.getId() + "/1.png");
            photoService.savePhotoAndFile(photo, getRandomPicture());

            brand.addPhoto(photo);

            assert brand.getBrandPhotos().size() == 1;
        }
    }

    private void runForDealsTable() throws IOException {
        System.out.println("Seeding photos for deals...");
        // each deal to have:
        // 20% chance of having 1 photos
        // 30% chance of having 1-4 photos
        // 50% chance of having 5 photos

        Faker faker = new Faker();

        List<Deal> deals = dealService.getAll();
        for (Deal deal : deals) {
            int numPhotosToSeed = 1;
            int roll = faker.number().numberBetween(1, 10);

            if (roll <= 2) continue;
            if (roll <= 5) numPhotosToSeed = faker.number().numberBetween(1, 4);
            if (roll > 5 && roll <= 10) numPhotosToSeed = 5;

            for (int i = 0; i < numPhotosToSeed; i++) {

                Photo photo = new Photo();
                photo.setPath("/deals/" + deal.getId() + "/" + i + ".png");
                photoService.savePhotoAndFile(photo, getRandomPicture());
                deal.addPhoto(photo);
                dealService.update(deal);
                assert deal.getDealPhotos().size() > 0;
            }
        }
    }

    private MultipartFile getRandomPicture() throws IOException {
        MultipartFile file = new MockMultipartFile("test.jpg", new FileInputStream(new File("sample_images/" + allSamplePictures.get(faker.random().nextInt(0, allSamplePictures.size() - 1)))));
        return file;
    }
}
