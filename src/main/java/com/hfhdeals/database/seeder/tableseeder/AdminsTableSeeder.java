package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.AdminFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminsTableSeeder implements TableSeeder {
    private final AdminFaker adminFaker;
    private Faker faker = new Faker();

    @Autowired
    public AdminsTableSeeder(AdminFaker adminFaker) {
        this.adminFaker = adminFaker;
    }

    @Override
    public void run() {
        for (int i = 0; i < faker.random().nextInt(15, 30); i++) {
            adminFaker.create();
        }
    }
}
