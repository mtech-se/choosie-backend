package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.service.baseservice.CategoryService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.utils.CustomFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryDealTableSeeder implements TableSeeder {
    private final DealService dealService;
    private final CategoryService categoryService;
    private final CustomFaker customFaker = new CustomFaker();

    @Autowired
    public CategoryDealTableSeeder(DealService dealService, CategoryService categoryService) {
        this.dealService = dealService;
        this.categoryService = categoryService;
    }

    @Override
    public void run() {
        // note that deal and category have a many-to-many relationship
        // we want to make each deal have a 50% chance having 0 categories,
        // 40% chance of having 1 random category from the existing categories
        // 10% chance of having 2 random categories - cannot be repeated categories

        List<Deal> deals = dealService.getAll();
        List<Category> existingCategories = categoryService.getAll();

        for (Deal deal : deals) {
            int roll = customFaker.number().numberBetween(0, 99);

            boolean shouldHaveNoCategories = roll < 50;
            boolean shouldHaveOneCategory = roll >= 50 && roll < 90;
            boolean shouldHaveTwoCategories = roll >= 90;

            if (shouldHaveNoCategories) continue;

            if (shouldHaveOneCategory) {
                Category category = existingCategories.get(customFaker.number().numberBetween(0, existingCategories.size() - 1));
                category.addDeal(deal);

                assert category.getCategoryDeals().size() > 0;
            }

            if (shouldHaveTwoCategories) {
                List<Integer> uniqueCategoryIds = customFaker.getUniqueNumbers(2, 0, existingCategories.size() - 1);
                List<Category> selectedCategories = new ArrayList<>();

                for (Integer uniqueCategoryId : uniqueCategoryIds) {
                    selectedCategories.add(categoryService.getById(uniqueCategoryId));
                }

                for (Category selectedCategory : selectedCategories) {
                    selectedCategory.addDeal(deal);

                    assert selectedCategory.getCategoryDeals().size() > 0;
                }
            }
        }
    }
}
