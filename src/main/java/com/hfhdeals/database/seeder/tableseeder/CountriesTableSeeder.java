package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.faker.CountryFaker;
import com.hfhdeals.model.jpaentities.Country;
import com.hfhdeals.service.baseservice.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CountriesTableSeeder implements TableSeeder {
    private static List<String> countryNames = Arrays.asList(
            "Singapore",
            "Malaysia",
            "Indonesia",
            "Hong Kong",
            "South Korea"
    );
    private final CountryService countryService;
    private final CountryFaker countryFaker;

    @Autowired
    public CountriesTableSeeder(CountryService countryService, CountryFaker countryFaker) {
        this.countryService = countryService;
        this.countryFaker = countryFaker;
    }

    @Override
    public void run() {
        for (String countryName : countryNames) {
            Country country = countryFaker.make(countryName);
            countryService.save(country);
        }
    }
}
