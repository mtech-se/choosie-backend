package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.MerchantFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MerchantsTableSeeder implements TableSeeder {

    @Autowired
    MerchantFaker merchantFaker;

    Faker faker = new Faker();

    @Override
    public void run() {
        for (int i = 0; i < faker.random().nextInt(5, 20); i++) {
            merchantFaker.create();
        }
    }
}
