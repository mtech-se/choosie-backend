//package com.hfhdeals.database.seeder.tableseeder;
//
//import com.hfhdeals.faker.CheckoutDetailsFaker;
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.service.PaymentService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class CheckoutDetailsTableSeeder implements TableSeeder {
//    @Autowired
//    PaymentService paymentService;
//
//    @Autowired
//    CheckoutDetailsFaker checkoutDetailsFaker;
//
//    @Override
//    public void run() {
//        for (int i = 0; i < 10; i++) {
//            CheckoutDetails checkoutDetails = checkoutDetailsFaker.make();
//            paymentService.saveCheckout(checkoutDetails);
//        }
//    }
//}
