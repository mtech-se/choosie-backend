package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.CustomerFaker;
import com.hfhdeals.model.jpaentities.Customer;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CustomersTableSeeder implements TableSeeder {
    private final CustomerFaker customerFaker;
    private Faker faker = new Faker();

    @Autowired
    public CustomersTableSeeder(CustomerFaker customerFaker) {
        this.customerFaker = customerFaker;
    }

    @Override
    public void run() {
        // customer's created_at to be any date between 24 months ago and now
        for (int i = 0; i < faker.random().nextInt(15, 30); i++) {
            Customer customer = customerFaker.create();

            DateTime lowerBoundary = new DateTime().minusMonths(24);
            Date createdAt = faker.date().between(lowerBoundary.toDate(), new Date());
            customer.setCreatedAt(createdAt);
        }
    }
}
