package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.faker.DealFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DealsTableSeeder implements TableSeeder {
    private final DealFaker dealFaker;

    @Autowired
    public DealsTableSeeder(DealFaker dealFaker) {
        this.dealFaker = dealFaker;
    }

    @Override
    public void run() {
        // seed 200 deals
        for (int i = 0; i < 200; i++) {
            dealFaker.createForSeeder();
        }
    }
}
