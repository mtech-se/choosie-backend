package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.service.baseservice.CuisineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CuisinesTableSeeder implements TableSeeder {
    private static final List<String> cuisineNames = Arrays.asList(
            "French",
            "Indian",
            "Mexican",
            "Chinese",
            "Thai",
            "Japanese",
            "American"
    );
    private final CuisineService cuisineService;

    @Autowired
    public CuisinesTableSeeder(CuisineService cuisineService) {
        this.cuisineService = cuisineService;
    }

    @Override
    public void run() {
        Faker faker = new Faker();

        cuisineNames.forEach(cuisineName -> {
            Cuisine cuisine = new Cuisine();
            cuisine.setName(cuisineName);
            cuisine.setDescription(faker.rickAndMorty().quote());
            cuisineService.save(cuisine);
        });
    }

}
