package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.faker.BrandFaker;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.BrandService;
import com.hfhdeals.service.baseservice.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BrandsTableSeeder implements TableSeeder {
    @Autowired
    MerchantService merchantService;

    @Autowired
    BrandService brandService;

    @Autowired
    BrandFaker brandFaker;

    @Override
    public void run() {
        List<Merchant> allMerchant = merchantService.getAll();
        brandFaker.create(allMerchant);
    }
}
