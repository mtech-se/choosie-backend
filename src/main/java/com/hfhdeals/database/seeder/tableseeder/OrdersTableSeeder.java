package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.faker.OrderFaker;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.service.baseservice.CustomerService;
import com.hfhdeals.utils.CustomFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrdersTableSeeder implements TableSeeder {

    private final OrderFaker orderFaker;
    private final CustomFaker customFaker = new CustomFaker();
    private final CustomerService customerService;

    @Autowired
    public OrdersTableSeeder(OrderFaker orderFaker, CustomerService customerService) {
        this.orderFaker = orderFaker;
        this.customerService = customerService;
    }

    @Override
    public void run() {
        // 20% of customers will not have any orders
        // for the remainder of customers, they will have 1-5 orders

        List<Customer> customers = customerService.getAll();

        for (Customer customer : customers) {
            boolean hasNoOrders = customFaker.bool(20);

            if (hasNoOrders) continue;

            int numOrdersToSeed = customFaker.random().nextInt(1, 5);
            for (int i = 0; i < numOrdersToSeed; i++) {

                Order order = orderFaker.make();

                customer.addOrder(order);
            }
        }
    }

}
