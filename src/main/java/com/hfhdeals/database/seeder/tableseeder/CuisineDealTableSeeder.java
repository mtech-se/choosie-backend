package com.hfhdeals.database.seeder.tableseeder;

import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.service.baseservice.CuisineService;
import com.hfhdeals.service.baseservice.DealService;
import com.hfhdeals.utils.CustomFaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CuisineDealTableSeeder implements TableSeeder {
    private final DealService dealService;
    private final CuisineService cuisineService;
    private final CustomFaker customFaker = new CustomFaker();

    @Autowired
    public CuisineDealTableSeeder(DealService dealService, CuisineService cuisineService) {
        this.dealService = dealService;
        this.cuisineService = cuisineService;
    }

    @Override
    public void run() {
        // note that deal and cuisine have a many-to-many relationship
        // we want to make each deal have a 50% chance having 0 cuisines,
        // 40% chance of having 1 random cuisine from the existing cuisines
        // 10% chance of having 2 random cuisines - cannot be repeated cuisines

        List<Deal> deals = dealService.getAll();
        List<Cuisine> existingCuisines = cuisineService.getAll();

        for (Deal deal : deals) {
            int roll = customFaker.number().numberBetween(0, 99);

            boolean shouldHaveNoCuisines = roll < 50;
            boolean shouldHaveOneCuisine = roll >= 50 && roll < 90;
            boolean shouldHaveTwoCuisines = roll >= 90;

            if (shouldHaveNoCuisines) continue;

            if (shouldHaveOneCuisine) {
                Cuisine cuisine = existingCuisines.get(customFaker.number().numberBetween(0, existingCuisines.size() - 1));

                cuisine.addDeal(deal);

                assert cuisine.getCuisineDeals().size() > 0;
            }

            if (shouldHaveTwoCuisines) {
                List<Integer> uniqueCuisineIds = customFaker.getUniqueNumbers(2, 0, existingCuisines.size() - 1);
                List<Cuisine> selectedCuisines = new ArrayList<>();

                for (Integer uniqueCuisineId : uniqueCuisineIds) {
                    selectedCuisines.add(cuisineService.getById(uniqueCuisineId));
                }

                for (Cuisine selectedCuisine : selectedCuisines) {
                    selectedCuisine.addDeal(deal);

                    assert selectedCuisine.getCuisineDeals().size() > 0;
                }
            }
        }
    }
}
