package com.hfhdeals.database.seeder.tableseeder;

import com.github.javafaker.Faker;
import com.hfhdeals.faker.ContactPersonFaker;
import com.hfhdeals.model.jpaentities.ContactPerson;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.service.baseservice.ContactPersonService;
import com.hfhdeals.service.baseservice.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ContactPersonsTableSeeder implements TableSeeder {
    private final MerchantService merchantService;
    private final ContactPersonService contactPersonService;
    private final ContactPersonFaker contactPersonFaker;

    @Autowired
    public ContactPersonsTableSeeder(MerchantService merchantService, ContactPersonService contactPersonService, ContactPersonFaker contactPersonFaker) {
        this.merchantService = merchantService;
        this.contactPersonService = contactPersonService;
        this.contactPersonFaker = contactPersonFaker;
    }

    @Override
    public void run() {
        Faker faker = new Faker();

        List<Merchant> merchants = merchantService.getAll();

        for (Merchant merchant : merchants) {
            Integer numContactPersonsToCreate = faker.random().nextInt(1, 3);

            for (int i = 0; i < numContactPersonsToCreate; i++) {
                ContactPerson contactPerson = contactPersonFaker.make(merchant);

                contactPersonService.save(contactPerson);
            }
        }
    }
}
