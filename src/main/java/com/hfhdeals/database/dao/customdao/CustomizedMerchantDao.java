package com.hfhdeals.database.dao.customdao;

import org.json.JSONArray;

import java.text.ParseException;
import java.util.Date;

public interface CustomizedMerchantDao {
    JSONArray getTopMerchantsByPurchaseRedemptionCount(Date startDate, Date endDate) throws ParseException;
}
