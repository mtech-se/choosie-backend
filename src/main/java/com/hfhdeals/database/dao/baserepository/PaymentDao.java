package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Payment;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentDao extends BaseRepository<Payment, Integer> {

}
