package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.DealCustomerClick;

public interface DealCustomerClickDao extends BaseRepository<DealCustomerClick, Integer> {
    Long countAllByDealId(Integer dealId);
}
