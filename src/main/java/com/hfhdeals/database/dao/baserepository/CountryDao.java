package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryDao extends BaseRepository<Country, Integer> {
    Page<Country> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);
}
