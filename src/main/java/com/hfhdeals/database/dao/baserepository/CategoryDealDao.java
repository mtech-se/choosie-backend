package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryDeal;
import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDealDao extends BaseRepository<CategoryDeal, Integer> {
    CategoryDeal findByCategoryAndDeal(Category category, Deal deal);

    void deleteCategoryDealByCategoryAndDeal(Category category, Deal deal);
}
