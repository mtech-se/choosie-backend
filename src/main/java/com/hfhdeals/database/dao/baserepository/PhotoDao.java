package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Photo;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoDao extends BaseRepository<Photo, Integer> {
}