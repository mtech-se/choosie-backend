package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AdminDao extends BaseRepository<Admin, Integer> {
    Integer countAllByDeletedAtIsNullAndCreatedAtBetween(Date startDate, Date endDate);

    Page<Admin> findAllByFirstNameContainingOrLastNameContainingAndDeletedAtIsNull(String firstName, String lastName, Pageable pageable);

    Admin findByEmailAndDeletedAtIsNull(String email);

}
