package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Cuisine;
import com.hfhdeals.model.jpaentities.CuisineDeal;
import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.stereotype.Repository;

@Repository
public interface CuisineDealDao extends BaseRepository<CuisineDeal, Integer> {
    CuisineDeal findByCuisineAndDeal(Cuisine cuisine, Deal deal);

    void deleteCuisineDealByCuisineAndDeal(Cuisine cuisine, Deal deal);
}
