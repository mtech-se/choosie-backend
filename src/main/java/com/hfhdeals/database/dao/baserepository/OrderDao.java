package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.jpaentities.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDao extends BaseRepository<Order, Integer> {

    Page<Order> findAllByCustomer(Customer customer, Pageable pageable);

}
