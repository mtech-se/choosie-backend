package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Cuisine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface CuisineDao extends BaseRepository<Cuisine, Integer> {
    Page<Cuisine> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);
}
