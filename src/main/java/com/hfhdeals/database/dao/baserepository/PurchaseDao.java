//package com.hfhdeals.database.dao.baserepository;
//
//import com.hfhdeals.model.jpaentities.Purchase;
//import org.springframework.stereotype.Repository;
//
//import java.util.Date;
//
//@Repository
//public interface PurchaseDao extends BaseRepository<Purchase, Integer> {
//    Integer countAllByCreatedAtBetween(Date startDate, Date endDate);
//}
