package com.hfhdeals.database.dao.baserepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.deletedAt IS NULL")
    List<T> findAll();

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.deletedAt IS NULL")
    Page<T> findAll(Pageable pageable);

    @Query("SELECT e from #{#entityName} e WHERE e.deletedAt IS NOT NULL")
    List<T> findAllSoftDeletedOnly();

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.deletedAt IS NULL AND e.id=?1")
    T getOne(ID id);

    @Modifying
    @Query("UPDATE #{#entityName} e SET e.deletedAt=CURRENT_TIMESTAMP WHERE e.id=?1")
    void softDelete(ID id);

}
