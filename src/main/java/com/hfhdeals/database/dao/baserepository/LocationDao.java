package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationDao extends BaseRepository<Location, Integer> {
    Page<Location> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);

    Page<Location> findAllByCountry_IdAndDeletedAtIsNull(Integer countryId, Pageable pageable);

    Page<Location> findAllByNameContainingAndCountry_IdAndDeletedAtIsNull(String name, Integer countryId, Pageable pageable);
}
