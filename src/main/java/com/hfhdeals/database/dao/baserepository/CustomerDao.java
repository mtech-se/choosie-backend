package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface CustomerDao extends BaseRepository<Customer, Integer> {
    Integer countAllByDeletedAtIsNullAndCreatedAtBetween(Date startDate, Date endDate);

    @Query("SELECT c FROM Customer c WHERE c.deletedAt IS NULL AND (c.firstName LIKE %?1% OR c.lastName LIKE %?1%)")
    Page<Customer> getAll(String name, Pageable pageable);

    Customer findByEmailAndDeletedAtIsNull(String email);
}
