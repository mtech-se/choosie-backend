package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.ContactPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactPersonDao extends BaseRepository<ContactPerson, Integer> {
    Page<ContactPerson> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);

    Page<ContactPerson> findAllByMerchant_IdEqualsAndDeletedAtIsNull(Integer merchantId, Pageable pageable);

    Page<ContactPerson> findAllByNameContainingAndMerchant_IdAndDeletedAtIsNull(String name, Integer merchantId, Pageable pageable);
}