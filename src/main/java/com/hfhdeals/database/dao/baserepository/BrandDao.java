package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandDao extends BaseRepository<Brand, Integer> {
    Page<Brand> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);

    Page<Brand> findAllByMerchant_IdAndDeletedAtIsNull(Integer merchantId, Pageable pageable);

    Page<Brand> findAllByNameContainingAndMerchant_IdAndDeletedAtIsNull(String name, Integer merchantId, Pageable pageable);
}
