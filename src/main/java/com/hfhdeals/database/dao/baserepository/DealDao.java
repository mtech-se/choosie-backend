package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Deal;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@Primary
public interface DealDao extends BaseRepository<Deal, Integer> {
    @Query(
            "SELECT d " +
                    "FROM Deal d " +
                    "LEFT JOIN d.orderItems oi " +
                    "WHERE d.dealOfTheMonth = TRUE " +
                    "GROUP BY d.id " +
                    "ORDER BY COUNT(oi.id) DESC"
    )
    Page<Deal> getDotmDealsOrderedByPurchaseCount(Pageable pageable);

    Page<Deal> findAllByCreatedAtBetweenAndTitleContainingAndDiscountedPriceBetweenAndDeletedAtIsNull(Date startDate, Date endDate, String title, Integer priceMin, Integer priceMax, Pageable pageable);

    Page<Deal> findAllByCreatedAtBetweenAndDiscountedPriceBetweenAndDeletedAtIsNull(Date startDate, Date endDate, Integer priceMin, Integer priceMax, Pageable pageable);

    Page<Deal> findAllByCreatedAtBetweenAndTitleContainingAndDeletedAtIsNull(Date startDate, Date endDate, String title, Pageable pageable);

    Page<Deal> findAllByCreatedAtBetweenAndDeletedAtIsNull(Date startDate, Date endDate, Pageable pageable);

    Page<Deal> findAllByTitleContainingAndDiscountedPriceBetweenAndDeletedAtIsNull(String title, Integer priceMin, Integer priceMax, Pageable pageable);

    Page<Deal> findAllByDiscountedPriceBetweenAndDeletedAtIsNull(Integer priceMin, Integer priceMax, Pageable pageable);

    Page<Deal> findAllByTitleContainingAndDeletedAtIsNull(String title, Pageable pageable);

    List<Deal> findAllByPublishedAtIsNotNull();

    @Query(
            "SELECT d " +
                    "FROM Deal d, OrderItem oi " +
                    "WHERE d.id = oi.deal.id " +
                    "AND d.createdAt BETWEEN ?1 AND ?2 " +
                    "GROUP BY d.id " +
                    "ORDER BY COUNT(oi.id) DESC"
    )
    Page<Deal> getDealsOrderedByPurchaseCount(Date startDate, Date endDate, Pageable pageable);

    @Query(
            "SELECT d " +
                    "FROM Deal d " +
                    "WHERE d.id IN (SELECT DISTINCT cd.deal.id FROM CategoryDeal cd WHERE cd.category.id = ?2) " +
                    "AND d.discountedPrice BETWEEN ?3 AND ?4 " +
                    "AND d.title LIKE %?1% " +
                    "AND d.deletedAt IS NULL"
    )
    Page<Deal> getDeals(String title, Integer categoryId, Integer priceMin, Integer priceMax, Pageable pageable);

    @Query(
            "SELECT d " +
                    "FROM Deal d " +
                    "WHERE d.id IN (SELECT DISTINCT cd.deal.id FROM CategoryDeal cd WHERE cd.category.id = ?2) " +
                    "AND d.title LIKE %?1% " +
                    "AND d.deletedAt IS NULL"
    )
    Page<Deal> getDeals(String title, Integer categoryId, Pageable pageable);

    @Query(
            "SELECT d " +
                    "FROM Deal d " +
                    "WHERE d.id IN (SELECT DISTINCT cd.deal.id FROM CategoryDeal cd WHERE cd.category.id = :categoryId) " +
                    "AND d.deletedAt IS NULL"
    )
    Page<Deal> getDeals(Integer categoryId, Pageable pageable);

    //    List<Deal> findAllByDeletedAtIsNullAndCreatedAtBetweenOrderByNumBoughtDesc(Date startDate, Date endDate);

    Integer countAllByDeletedAtIsNullAndPublishedAtIsNotNullAndPublishedAtBetween(Date startDate, Date endDate);
}
