package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Outlet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface OutletDao extends BaseRepository<Outlet, Integer> {
    Page<Outlet> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);

    Page<Outlet> findAllByBrand_IdAndDeletedAtIsNull(Integer brandId, Pageable pageable);

    Page<Outlet> findAllByNameContainingAndBrand_IdAndDeletedAtIsNull(String name, Integer brandId, Pageable pageable);
}
