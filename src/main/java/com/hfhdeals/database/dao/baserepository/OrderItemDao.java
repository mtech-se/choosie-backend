package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface OrderItemDao extends BaseRepository<OrderItem, Integer> {
    Long countAllByDealId(Integer dealId);

    Integer countAllByDeletedAtIsNullAndCreatedAtBetween(Date startDate, Date endDate);

    @Query("SELECT SUM(oi.spotPrice) FROM OrderItem oi WHERE oi.createdAt BETWEEN ?1 AND ?2")
    Integer getSumSpotPrice(Date startDate, Date endDate);
}
