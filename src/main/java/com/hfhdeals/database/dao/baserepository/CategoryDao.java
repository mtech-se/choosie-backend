package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CategoryDao extends BaseRepository<Category, Integer> {
    Page<Category> findAllByNameContainingAndDeletedAtIsNull(String name, Pageable pageable);

    @Query("" +
            "SELECT c, COUNT(d) AS num_deals " +
            "FROM CategoryDeal cd " +
            "INNER JOIN cd.deal d " +
            "INNER JOIN cd.category c " +
            "WHERE d.publishedAt IS NOT NULL " +
            "AND d.createdAt BETWEEN ?1 AND ?2 " +
            "GROUP BY c.id " +
            "ORDER BY num_deals DESC"
    )
    List<Object[]> getCategoriesWithPublishedDealCount(Date startDate, Date endDate, Pageable pageable);
}
