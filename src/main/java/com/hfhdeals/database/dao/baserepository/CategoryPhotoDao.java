package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Category;
import com.hfhdeals.model.jpaentities.CategoryPhoto;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface CategoryPhotoDao extends BaseRepository<CategoryPhoto, Integer> {
    Page<CategoryPhoto> getCategoryPhotosByCategoryAndDeletedAtIsNull(Category category, Pageable pageable);
}
