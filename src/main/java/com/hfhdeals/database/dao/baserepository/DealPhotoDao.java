package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Deal;
import com.hfhdeals.model.jpaentities.DealPhoto;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface DealPhotoDao extends BaseRepository<DealPhoto, Integer> {
    Page<DealPhoto> getDealPhotosByDealAndDeletedAtIsNull(Deal deal, Pageable pageable);
}
