package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.database.dao.customdao.CustomizedMerchantDao;
import com.hfhdeals.model.jpaentities.Merchant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MerchantDao extends BaseRepository<Merchant, Integer>, CustomizedMerchantDao {
    Page<Merchant> findAllByNameIgnoreCaseContainingAndDeletedAtIsNull(String name, Pageable pageable);

    @Query("" +
            "SELECT m, COUNT(oi) AS num_deals_redeemed " +
            "FROM OrderItem oi " +
            "INNER JOIN oi.deal d " +
            "INNER JOIN d.outlet o " +
            "INNER JOIN o.brand b " +
            "INNER JOIN b.merchant m " +
            "WHERE oi.redeemedAt IS NOT NULL " +
            "AND d.createdAt BETWEEN ?1 AND ?2 " +
            "GROUP BY m.id " +
            "ORDER BY num_deals_redeemed DESC"
    )
    List<Object[]> getMerchantsWithRedeemedDealCount(Date startDate, Date endDate, Pageable pageable);
}