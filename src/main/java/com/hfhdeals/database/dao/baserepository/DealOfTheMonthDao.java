//package com.hfhdeals.database.dao.baserepository;
//
//import com.hfhdeals.model.jpaentities.Deal;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//
//public interface DealOfTheMonthDao extends BaseRepository<Deal, Integer> {
//    Page<Deal> findAllByDealOfTheMonthTrueAndDeletedAtIsNull(Pageable pageable);
//}
