package com.hfhdeals.database.dao.baserepository;

import com.hfhdeals.model.jpaentities.Brand;
import com.hfhdeals.model.jpaentities.BrandPhoto;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface BrandPhotoDao extends BaseRepository<BrandPhoto, Integer> {
    Page<BrandPhoto> getBrandPhotosByBrandAndDeletedAtIsNull(Brand brand, Pageable pageable);
}
