/*package com.hfhdeals.database.dao;

import com.hfhdeals.model.jpaentities.Order;
import com.hfhdeals.model.pojos.ChargeRequest;
import com.hfhdeals.model.pojos.CreditCard;
import com.stripe.model.Charge;
import com.stripe.model.Token;

public interface PaymentDao {

    Token createToken(CreditCard cc);

    Charge createCharge(Token token, ChargeRequest cr);

    void save(Order order);

    int getLastCheckoutId();

    int getLastCheckoutId(int id);

}
*/