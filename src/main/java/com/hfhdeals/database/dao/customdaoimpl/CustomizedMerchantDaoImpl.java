package com.hfhdeals.database.dao.customdaoimpl;

import com.hfhdeals.database.dao.customdao.CustomizedMerchantDao;
import com.hfhdeals.model.jpaentities.Merchant;
import com.hfhdeals.utils.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Repository
public class CustomizedMerchantDaoImpl implements CustomizedMerchantDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public JSONArray getTopMerchantsByPurchaseRedemptionCount(Date startDate, Date endDate) throws ParseException {
        String query = "SELECT merchants.id,\n" +
                "       merchants.name,\n" +
                "       merchants.business_registration_number,\n" +
                "       merchants.mailing_address,\n" +
                "       merchants.deleted_at,\n" +
                "       COUNT(*) as num_purchases\n" +
                "FROM purchases\n" +
                "       INNER JOIN deals on purchases.deal_id = deals.id\n" +
                "       INNER JOIN outlets on deals.outlet_id = outlets.id\n" +
                "       INNER JOIN brands on outlets.brand_id = brands.id\n" +
                "       INNER JOIN merchants on brands.merchant_id = merchants.id\n" +
                "WHERE purchases.created_at BETWEEN '2017-01-01 00:00:00' AND '2018-12-04 00:00:00'\n" +
                "  AND merchants.deleted_at IS NULL\n" +
                "GROUP BY merchants.id\n" +
                "ORDER BY num_purchases DESC\n" +
                "LIMIT 5;\n";

        List<Object[]> result = entityManager.createNativeQuery(query).getResultList();

        JSONArray jsonArray = new JSONArray();

        for (Object[] element : result) {
            Integer id = (Integer) element[0];
            String name = (String) element[1];
            String businessRegistrationNumber = (String) element[2];
            String mailingAddress = (String) element[3];
            String deletedAt = (String) element[4];

            Merchant merchant = new Merchant();
            merchant.setId(id);
            merchant.setName(name);
            merchant.setBusinessRegistrationNumber(businessRegistrationNumber);
            merchant.setMailingAddress(mailingAddress);
            merchant.setDeletedAt(DateUtils.fromString(deletedAt, DateUtils.DateFormat.MYSQL_FORMAT));

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("merchant", merchant.toJson());
            jsonObject.put("num_purchases_redeemed", element[5]);

            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }
}
