package com.hfhdeals.database.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class DatabasePopulateDao {
    @PersistenceContext
    private EntityManager entityManager;

    private List<String> modelNames = new ArrayList<>();

    public Map<String, Long> getCounts() {
        Map<String, Long> results = new LinkedHashMap<>();

        Long count;

        modelNames.add("Brand");
        modelNames.add("Country");
        modelNames.add("Category");
        modelNames.add("Deal");
        modelNames.add("Merchant");
        modelNames.add("Outlet");
        modelNames.add("Customer");
        modelNames.add("Order");
        modelNames.add("Admin");
        modelNames.add("Cuisine");
        modelNames.add("Location");
        modelNames.add("DealCustomerClick");
        modelNames.add("ContactPerson");
        modelNames.add("Photo");
        modelNames.add("CategoryDeal");
        modelNames.add("CuisineDeal");
        modelNames.add("OrderItem");

        String baseQuery = "select count(*) from ";
        String fullQuery;

        for (String modelName : modelNames) {
            fullQuery = baseQuery + modelName;
            count = (Long) entityManager.createQuery(fullQuery).getSingleResult();
            results.put(modelName, count);

            System.out.println(fullQuery + ": " + count);
        }

        return results;
    }
}
