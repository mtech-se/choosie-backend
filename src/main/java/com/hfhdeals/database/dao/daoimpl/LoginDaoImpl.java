/*package com.hfhdeals.database.dao.daoimpl;

import com.hfhdeals.database.dao.LoginDao;
import com.hfhdeals.model.jpaentities.Customer;
import com.hfhdeals.model.pojos.Login;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class LoginDaoImpl implements LoginDao {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int verifyCredentials(Login customer) {
        Query q;
        Customer u;
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
            Root<Customer> criteria = query.from(Customer.class);
            query.select(criteria);
            query.where(builder.equal(criteria.get("email"), customer.getEmail()));
            u = entityManager.createQuery(query).getSingleResult();
            if (u == null) {
                System.out.println("No Customer Found");
                return 0;
            } else if (u.getEmail().equals(customer.getEmail()) && u.getPassword().equals(customer.getPassword())) {
                System.out.println("successLogin");
                String otp = generateOTP();
                System.out.println("Otp is :" + otp);
                q = entityManager.createQuery("Update Customer c set otp=:otp Where c.id=:id ");
                q.setParameter("otp", otp);
                q.setParameter("id", u.getId());
                int i = q.executeUpdate();
                if (i == 1)
                    System.out.print("added entry");
                else
                    System.out.print("error");
                return 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public String generateOTP() {
        int count = 10;
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
}
*/