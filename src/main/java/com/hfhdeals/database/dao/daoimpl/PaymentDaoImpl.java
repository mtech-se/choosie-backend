//package com.hfhdeals.database.dao.daoimpl;
//
//import com.hfhdeals.constant.Constants;
//import com.hfhdeals.database.dao.PaymentDao;
//import com.hfhdeals.model.jpaentities.CheckoutDetails;
//import com.hfhdeals.model.jpaentities.Order;
//import com.hfhdeals.model.pojos.ChargeRequest;
//import com.hfhdeals.model.pojos.CreditCard;
//import com.stripe.Stripe;
//import com.stripe.exception.*;
//import com.stripe.model.Charge;
//import com.stripe.model.Token;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//@Repository
//public class PaymentDaoImpl implements PaymentDao {
//
//    @PersistenceContext
//    private EntityManager entityManager;
//
//    @Override
//    public Token createToken(CreditCard cc) {
//        Stripe.apiKey = Constants.StripeAPIKey;
//        Token token = null;
//
//        Map<String, Object> tokenParams = new HashMap<>();
//        Map<String, Object> cardParams = new HashMap<>();
//        cardParams.put("number", cc.getNumber());
//        cardParams.put("exp_month", cc.getExp_month());
//        cardParams.put("exp_year", cc.getExp_year());
//        cardParams.put("cvc", cc.getCvc());
//        tokenParams.put("card", cardParams);
//
//        try {
//            token = Token.create(tokenParams);
//        } catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException ex) {
//            Logger.getLogger(PaymentDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return token;
//
//    }
//
//    @Override
//    public Charge createCharge(Token token, ChargeRequest cr) {
//        Stripe.apiKey = Constants.StripeAPIKey;
//        Charge charge = null;
//        Map<String, Object> chargeParams = new HashMap<>();
//        chargeParams.put("amount", cr.getAmount());
//        chargeParams.put("currency", cr.getCurrency());
//        chargeParams.put("description", cr.getDescription());
//        chargeParams.put("source", token.getId());
//        try {
//            // ^ obtained with Stripe.js
//            charge = Charge.create(chargeParams);
//        } catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException ex) {
//            Logger.getLogger(PaymentDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return charge;
//
//    }
//
//    @Override
//    public void save(Order order) {
//        order.setDate_time_order(new Date(System.currentTimeMillis()));
//        entityManager.persist(order);
//
//    }
//
//    public void save(CheckoutDetails cd) {
//        entityManager.persist(cd);
//
//    }
//
//    @Override
//    public void saveCheckout(CheckoutDetails cc) {
//        entityManager.persist(cc);
//        // return (int) entityManager.createQuery("select max(o.checkout_id) from Order o").getSingleResult();
//        // System.out.println("_____________");
//        // System.out.println(id);
//        // return 1;
//    }
//
//    @Override
//    public int getLastCheckoutId() {
//
//        return (int) entityManager.createQuery("select max(c.checkout_id) from CheckoutDetails c").getSingleResult();
//
//    }
//
//    @Override
//    public int getLastCheckoutId(int id) {
//        Query jpqlQuery = entityManager.createQuery("select max(c.checkout_id) from CheckoutDetails c where c.user_id=:id");
//        jpqlQuery.setParameter("id", id);
//        return (int) jpqlQuery.getSingleResult();
//    }
//}
