package com.hfhdeals.requestbody;

import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BrandRequestBody {
    @NotNull(groups = {Create.class, Update.class})
    @NotEmpty(groups = {Create.class, Update.class})
    private String name;

    private Merchant merchant;

    @Getter
    @Setter
    public class Merchant {
        @NotNull(groups = Create.class)
        private Integer id;
    }
}
