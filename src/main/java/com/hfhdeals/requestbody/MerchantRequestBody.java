package com.hfhdeals.requestbody;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class MerchantRequestBody {
    @NotNull(groups = Create.class)
    @NotEmpty(groups = Create.class)
    private String name;

    @NotNull(groups = Create.class)
    @NotEmpty(groups = Create.class)
    @JsonProperty("business_registration_number")
    private String businessRegistrationNumber;

    @NotNull(groups = Create.class)
    @NotEmpty(groups = Create.class)
    @JsonProperty("mailing_address")
    private String mailingAddress;

    @JsonProperty("contact_persons")
    private List<ContactPerson> contactPersons = new ArrayList<>();

    @Getter
    @Setter
    @ToString
    public static class ContactPerson {
        @Null(groups = Update.class)
        Integer id;

        @NotNull
        @NotEmpty
        String name;

        @NotNull
        @NotEmpty
        String position;

        @NotNull
        @NotEmpty
        @JsonProperty("mobile_number")
        String mobileNumber;

        @NotNull
        @NotEmpty
        @Email
        String email;
    }
}
