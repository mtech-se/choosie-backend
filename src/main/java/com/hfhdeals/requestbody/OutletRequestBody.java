package com.hfhdeals.requestbody;

import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class OutletRequestBody {
    @NotNull(groups = {Create.class, Update.class})
    @NotEmpty(groups = {Create.class, Update.class})
    private String name;

    private String address;

    private Brand brand;

    @Getter
    @Setter
    public class Brand {
        @NotNull(groups = Create.class)
        private Integer id;
    }
}
