package com.hfhdeals.requestbody;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Getter
@Setter
@ToString
public class AdminRequestBody {
    @Null(groups = {Create.class, Update.class})
    private Integer id;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    @Email
    private String email;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    @JsonProperty("first_name")
    private String firstName;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    @JsonProperty("last_name")
    private String lastName;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    private String gender;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    private String password;
}
