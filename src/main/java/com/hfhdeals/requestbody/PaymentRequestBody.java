package com.hfhdeals.requestbody;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class PaymentRequestBody {

    @NotNull
    @NotEmpty
    private Integer amount;

    @NotNull
    @NotEmpty
    @JsonProperty("billing_address")
    private String billingAddress;

    @NotNull
    @NotEmpty
    private Currency currency;

    @JsonProperty("credit_card")
    private CreditCard creditCard;

    public enum Currency {
        SGD, MYR, HKD
    }

    @Getter
    @Setter
    @ToString
    public static class CreditCard {

        @NotNull
        @NotEmpty
        String number;

        @NotNull
        @NotEmpty
        @JsonProperty("exp_month")
        Integer expMonth;

        @NotNull
        @NotEmpty
        @JsonProperty("exp_year")
        Integer expYear;

        @NotNull
        @NotEmpty
        Integer cvv;

    }
}
