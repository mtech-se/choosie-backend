package com.hfhdeals.requestbody;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hfhdeals.constant.Constants;
import com.hfhdeals.model.jpaentities.CategoryDeal;
import com.hfhdeals.model.jpaentities.CuisineDeal;
import com.hfhdeals.requestbody.interfaces.Create;
import com.hfhdeals.requestbody.interfaces.Update;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
public class DealRequestBody {
    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    private String title;

    private String description;

    @NotNull(groups = {Create.class})
    @JsonProperty("original_price")
    private Integer originalPrice;

    @NotNull(groups = {Create.class})
    @JsonProperty("discounted_price")
    private Integer discountedPrice;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    @JsonProperty("terms_of_use")
    private String termsOfUse;

    @NotNull(groups = {Create.class})
    @NotEmpty(groups = {Create.class})
    @JsonProperty("redemption_instructions")
    private String redemptionInstructions;

    @NotNull(groups = {Create.class})
    @JsonProperty("max_quantity")
    private Integer maxQuantity;

    private Outlet outlet;

    @NotNull(groups = {Create.class})
    @JsonProperty("deal_of_the_month")
    private Boolean dealOfTheMonth;

    @JsonProperty("category_deals")
    private List<CategoryDeal> categoryDeals = new ArrayList<>();

    @JsonProperty("cuisine_deals")
    private List<CuisineDeal> cuisineDeals = new ArrayList<>();

    @NotNull(groups = {Create.class})
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("auto_publish_date")
    private Date autoPublishDate;

    @Null(groups = {Create.class, Update.class})
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("published_at")
    private Date publishedAt;

    @NotNull(groups = {Create.class})
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("redemption_expiry_date")
    private Date redemptionExpiryDate;

    @Null(groups = {Create.class, Update.class})
    @JsonFormat(pattern = Constants.ISO_FORMAT)
    @JsonProperty("archived_at")
    private Date archivedAt;

    @Getter
    @Setter
    public class Outlet {
        @NotNull(groups = Create.class)
        private Integer id;
    }
}
