package com.hfhdeals.config.converter;

import com.hfhdeals.constant.Constants;
import com.hfhdeals.exception.CannotParseDate;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.ISO_FORMAT);

        try {
            return simpleDateFormat.parse(s);
        } catch (ParseException e) {
            throw new CannotParseDate("Input string \"" + s + "\" cannot be parsed into " + Constants.ISO_FORMAT);
        }
    }
}
