package com.hfhdeals.config;

import com.hfhdeals.interceptor.QueryParamsInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class InterceptorConfiguration extends WebMvcConfigurerAdapter {
    private final QueryParamsInterceptor queryParamsInterceptor;

    @Autowired
    public InterceptorConfiguration(QueryParamsInterceptor queryParamsInterceptor) {
        this.queryParamsInterceptor = queryParamsInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(queryParamsInterceptor);
    }
}
