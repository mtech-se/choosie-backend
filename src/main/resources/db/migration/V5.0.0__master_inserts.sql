INSERT INTO `admins` (`id`, `email`, `first_name`, `gender`, `last_name`, `password`)
VALUES (1, 'root@halalfoodhunt.com', 'Root', 'Male', 'Admin', '$2a$10$fEqqPztCcr6.eIlif85snus34MFxNeIA9Taz4BX5x/AoIRZWCE6yS');


INSERT INTO `roles` (`id`, `name`)
VALUES ('1', 'Super Admin');
INSERT INTO `roles` (`id`, `name`)
VALUES ('2', 'Admin');

INSERT INTO `admin_roles` (`id`, `admin_id`, `role_id`)
VALUES ('1', '1', '1');
