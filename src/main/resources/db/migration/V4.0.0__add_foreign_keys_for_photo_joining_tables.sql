ALTER TABLE category_photo
  ADD CONSTRAINT fk_categories_category_photo FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE category_photo
  ADD CONSTRAINT fk_category_photo_photos FOREIGN KEY (photo_id) REFERENCES photos (id);

ALTER TABLE brand_photo
  ADD CONSTRAINT fk_brands_brand_photo FOREIGN KEY (brand_id) REFERENCES brands (id);

ALTER TABLE brand_photo
  ADD CONSTRAINT fk_brand_photo_photos FOREIGN KEY (photo_id) REFERENCES photos (id);

ALTER TABLE deal_photo
  ADD CONSTRAINT fk_deal_photo_deals FOREIGN KEY (deal_id) REFERENCES deals (id);

ALTER TABLE deal_photo
  ADD CONSTRAINT fk_deal_photo_photos FOREIGN KEY (photo_id) REFERENCES photos (id);