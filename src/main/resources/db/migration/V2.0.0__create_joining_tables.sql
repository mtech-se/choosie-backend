CREATE TABLE category_photo
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  category_id INT(11) NOT NULL,
  photo_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE category_photo
  ADD CONSTRAINT uk_category_id_photo_id UNIQUE (category_id, photo_id);

CREATE TABLE brand_photo
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  brand_id INT(11) NOT NULL,
  photo_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE brand_photo
  ADD CONSTRAINT uk_brand_id_photo_id UNIQUE (brand_id, photo_id);

CREATE TABLE deal_photo
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  deal_id INT(11) NOT NULL,
  photo_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE deal_photo
  ADD CONSTRAINT uk_deal_id_photo_id UNIQUE (deal_id, photo_id);

CREATE TABLE category_deal
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  category_id INT(11) NOT NULL,
  deal_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE category_deal
  ADD CONSTRAINT uk_category_id_deal_id UNIQUE (category_id, deal_id);

CREATE TABLE cuisine_deal
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  cuisine_id INT(11) NOT NULL,
  deal_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE cuisine_deal
  ADD CONSTRAINT uk_cuisine_id_deal_id UNIQUE (cuisine_id, deal_id);

CREATE TABLE admin_roles
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  admin_id INT(11) NOT NULL,
  role_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE admin_roles
  ADD CONSTRAINT uk_admin_id_role_id UNIQUE (admin_id, role_id);
