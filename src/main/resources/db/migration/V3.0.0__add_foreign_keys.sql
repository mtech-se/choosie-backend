ALTER TABLE deals
  ADD CONSTRAINT fk_deals_outlets FOREIGN KEY (outlet_id) REFERENCES outlets (id);

ALTER TABLE brands
  ADD CONSTRAINT fk_brands_merchants FOREIGN KEY (merchant_id) REFERENCES merchants (id);

ALTER TABLE outlets
  ADD CONSTRAINT fk_brands_outlets FOREIGN KEY (brand_id) REFERENCES brands (id);

ALTER TABLE locations
  ADD CONSTRAINT fk_countries_locations FOREIGN KEY (country_id) REFERENCES countries (id);

ALTER TABLE category_deal
  ADD CONSTRAINT fk_deals_category_deal FOREIGN KEY (deal_id) REFERENCES deals (id);

ALTER TABLE category_deal
  ADD CONSTRAINT fk_categories_category_deal FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE cuisine_deal
  ADD CONSTRAINT fk_deals_cuisine_deal FOREIGN KEY (deal_id) REFERENCES deals (id);

ALTER TABLE cuisine_deal
  ADD CONSTRAINT fk_cuisines_cuisine_deal FOREIGN KEY (cuisine_id) REFERENCES cuisines (id);

ALTER TABLE deal_customer_clicks
  ADD CONSTRAINT fk_customers_deal_customer_clicks FOREIGN KEY (customer_id) REFERENCES customers (id);

ALTER TABLE deal_customer_clicks
  ADD CONSTRAINT fk_deals_deal_customer_clicks FOREIGN KEY (deal_id) REFERENCES deals (id);

ALTER TABLE contact_persons
  ADD CONSTRAINT fk_contact_persons_merchants FOREIGN KEY (merchant_id) REFERENCES merchants (id);

ALTER TABLE admin_roles
  ADD CONSTRAINT fk_admins_admin_roles FOREIGN KEY (admin_id) REFERENCES admins (id);

ALTER TABLE admin_roles
  ADD CONSTRAINT fk_roles_admin_roles FOREIGN KEY (role_id) REFERENCES roles (id);



