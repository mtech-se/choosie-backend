CREATE TABLE deals
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  description TEXT NULL,
  original_price INT(11) NOT NULL,
  discounted_price INT(11) NOT NULL,
  deal_of_the_month BIT(1) NULL,
  terms_of_use VARCHAR(255) NULL,
  redemption_instructions VARCHAR(255) NULL,
  max_quantity INT(11) NULL,
  --   num_bought INT(11) NOT NULL,
  --   hit_count INT(11) NOT NULL,
  outlet_id INT(11) NOT NULL,
  auto_publish_date DATETIME NULL,
  published_at DATETIME NULL,
  redemption_expiry_date DATETIME NULL,
  archived_at DATETIME NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE photos
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  path VARCHAR(255) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE merchants
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  business_registration_number VARCHAR(255) NOT NULL,
  mailing_address VARCHAR(255) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE merchants
  ADD CONSTRAINT uk_merchants_business_registration_number UNIQUE (business_registration_number);

CREATE TABLE brands
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  merchant_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE outlets
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  brand_id INT(11) NOT NULL,
  address VARCHAR(255) NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE customers
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  gender VARCHAR(255) NOT NULL,
  password VARCHAR(255) NULL,

  billing_add_line_one VARCHAR(255) NULL,
  billing_add_line_two VARCHAR(255) NULL,
  billing_add_postal_code VARCHAR(255) NULL,
  billing_cellphone VARCHAR(255) NULL,
  billing_first_name VARCHAR(255) NULL,
  billing_last_name VARCHAR(255) NULL,
  billing_telephone VARCHAR(255) NULL,

  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE customers
  ADD CONSTRAINT uk_customers_email UNIQUE (email);

CREATE TABLE admins
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  gender VARCHAR(255) NOT NULL,
  password VARCHAR(255) NULL,

  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE admins
  ADD CONSTRAINT uk_admins_email UNIQUE (email);

CREATE TABLE roles
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE roles
  ADD CONSTRAINT uk_roles_name UNIQUE (name);

CREATE TABLE categories
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description TEXT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE categories
  ADD CONSTRAINT uk_categories_name UNIQUE (name);

CREATE TABLE countries
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE countries
  ADD CONSTRAINT uk_countries_name UNIQUE (name);

CREATE TABLE cuisines
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description TEXT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE cuisines
  ADD CONSTRAINT uk_cuisines_name UNIQUE (name);

CREATE TABLE locations
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  country_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE deal_customer_clicks
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  deal_id INT(11) NOT NULL,
  customer_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE contact_persons
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  position VARCHAR(255) NOT NULL,
  mobile_number VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  merchant_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

ALTER TABLE contact_persons
  ADD CONSTRAINT uk_contact_persons_email UNIQUE (email);

CREATE TABLE orders
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  customer_id INT(11) NOT NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE order_items
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  deal_id INT(11) NOT NULL,
  order_id INT(11) NOT NULL,
  spot_price INT(11) NOT NULL,
  redeemed_at DATETIME NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);

CREATE TABLE payments
(
  id INT(11) NOT NULL AUTO_INCREMENT,
  order_id INT(11) NOT NULL,
  spot_billing_address VARCHAR(255) NOT NULL,
  receipt_amount INT(11) NOT NULL,
  completed_at DATETIME NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  deleted_at DATETIME NULL,
  PRIMARY KEY (id)
);